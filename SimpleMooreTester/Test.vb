Imports Microsoft.VisualBasic
Imports System
Imports System.Threading
Imports isr.Algorithms.Automata
Imports isr.Algorithms.Automata.Moore

Public Class Test

    Public Shared Sub Run()
        Dim test As New Test
        'test.RunClassBasedSigma()
        'test.RunEnumBasedSigma()
        'test.RunStringBasedSigma()
        test.RunQueryMachine()
        Console.WriteLine("Done.  Enter key to exit:")
        Console.ReadKey()
    End Sub

#Region " THREE STATE MACHINES "

    Private WithEvents _State1 As IState
    Private WithEvents _State2 As IState
    Private WithEvents _State3 As IState

    Private WithEvents _StringTransition1 As ITransition(Of String)
    Private WithEvents _StringTransition2 As ITransition(Of String)
    Private WithEvents _StringTransition3 As ITransition(Of String)
    Private WithEvents _StringMachine As IMachine(Of String)

    Private WithEvents _SignalTransition1 As ITransition(Of SigmaSignal)
    Private WithEvents _SignalTransition2 As ITransition(Of SigmaSignal)
    Private WithEvents _SignalTransition3 As ITransition(Of SigmaSignal)
    Private WithEvents _SignalMachine As IMachine(Of SigmaSignal)

    Private WithEvents _SigmaTransition1 As ITransition(Of Sigma)
    Private WithEvents _SigmaTransition2 As ITransition(Of Sigma)
    Private WithEvents _SigmaTransition3 As ITransition(Of Sigma)
    Private WithEvents _SigmaMachine As IMachine(Of Sigma)

    ''' <summary>
    ''' Tests a Moore machine with String signals.
    ''' </summary>
    Public Sub RunStringBasedSigma()

        Me._stringMachine = New Machine(Of String)()
        Me._stringMachine.OnsetDelay = 500
        Me._stringMachine.Interval = 1700
        Me._state1 = Me._stringMachine.AddState("State 1")
        Me._state2 = Me._stringMachine.AddState("State 2")
        Me._state3 = Me._stringMachine.AddState("State 3")

        Dim sigma As String() = New String() {"", "", ""}
        sigma(0) = Me._stringMachine.AddSymbol("1->2")
        sigma(1) = Me._stringMachine.AddSymbol("2->3")
        sigma(2) = Me._stringMachine.AddSymbol("3->1")

        Me._stringTransition1 = Me._stringMachine.AddTransition(Me._state1, sigma(0), Me._state2) ' state1 --1->2--> state2
        Me._stringTransition2 = Me._stringMachine.AddTransition(Me._state2, sigma(1), Me._state3) ' state2 --2->3--> state3
        Me._stringTransition3 = Me._stringMachine.AddTransition(Me._state3, sigma(2), Me._state1) ' state3 --3->1--> state1

        Me._stringMachine.Analyze()
        Console.WriteLine(Me._stringMachine.AnalysisReport)

        Dim machineThread As Thread = New Thread(New ThreadStart(AddressOf Me.stringMachine.Activate))
        machineThread.Start()

        Dim i As Integer
        Do Until Me._stringMachine.IsStopRequested
            Me._stringMachine.EnqueueInput(sigma(i))
            If (DateTime.Now.Second Mod 15 = 0) Then
                Me._stringMachine.RequestStop()
            Else
                Thread.Sleep(600)
            End If
            i = (i + 1) Mod 3
        Loop
        ' wait till thread completes all actions.
        If machineThread.IsAlive Then
            Console.WriteLine("thread still alive")
        Else
            Console.WriteLine("thread not alive")
        End If
        machineThread.Join()
        Console.WriteLine("machine thread joined with main thread")
    End Sub

    ''' <summary>
    ''' Tests a state machine using enumerated signals.
    ''' </summary>
    Public Sub RunEnumBasedSigma()

        Me._signalMachine = New Machine(Of SigmaSignal)()
        Me._signalMachine.OnsetDelay = 500
        Me._signalMachine.Interval = 1700
        Me._state1 = Me._signalMachine.AddState("State 1")
        Me._state2 = Me._signalMachine.AddState("State 2")
        Me._state3 = Me._signalMachine.AddState("State 3")

        Dim sigma As SigmaSignal() = New SigmaSignal(2) {}
        sigma(0) = Me._signalMachine.AddSymbol(SigmaSignal.One)
        sigma(1) = Me._signalMachine.AddSymbol(SigmaSignal.Two)
        sigma(2) = Me._signalMachine.AddSymbol(SigmaSignal.Three)

        Me._signalTransition1 = Me._signalMachine.AddTransition(Me._state1, sigma(0), Me._state2) ' state1 --one--> state2
        Me._signalTransition2 = Me._signalMachine.AddTransition(Me._state2, sigma(1), Me._state3) ' state2 --two--> state3
        Me._signalTransition3 = Me._signalMachine.AddTransition(Me._state3, sigma(2), Me._state1) ' state3 --three--> state1

        Me._signalMachine.Analyze()
        Console.WriteLine(Me._signalMachine.AnalysisReport)

        Dim machineThread As Thread = New Thread(New ThreadStart(AddressOf Me.signalMachine.Activate))
        machineThread.Start()

        Dim i As Integer
        Do Until Me._signalMachine.IsStopRequested
            Me._signalMachine.EnqueueInput(sigma(i))
            If (DateTime.Now.Second Mod 15 = 0) Then
                Me._signalMachine.RequestStop()
            End If
            Thread.Sleep(600)
            i = (i + 1) Mod 3
        Loop
        ' wait till thread completes all actions.
        machineThread.Join()
    End Sub

    ''' <summary>
    ''' Tests a state machine with signals based on a class. 
    ''' </summary>
    Public Sub RunClassBasedSigma()

        Me._sigmaMachine = New Machine(Of Sigma)()
        Me._sigmaMachine.OnsetDelay = 500
        Me._sigmaMachine.Interval = 1700
        Me._state1 = Me._sigmaMachine.AddState("State 1")
        Me._state2 = Me._sigmaMachine.AddState("State 2")
        Me._state3 = Me._sigmaMachine.AddState("State 3")

        Dim sigma As Sigma() = New Sigma(2) {}
        sigma(0) = Me._sigmaMachine.AddSymbol(New Sigma(SigmaSignal.One))
        sigma(1) = Me._sigmaMachine.AddSymbol(New Sigma(SigmaSignal.Two))
        sigma(2) = Me._sigmaMachine.AddSymbol(New Sigma(SigmaSignal.Three))

        Me._sigmaTransition1 = Me._sigmaMachine.AddTransition(Me._state1, sigma(0), Me._state2) ' state1 --one--> state2
        Me._sigmaTransition2 = Me._sigmaMachine.AddTransition(Me._state2, sigma(1), Me._state3) ' state2 --two--> state3
        Me._sigmaTransition3 = Me._sigmaMachine.AddTransition(Me._state3, sigma(2), Me._state1) ' state3 --three--> state1
        ' delta.AddTransition(state3, sigma(2), state2) ' Not deterministic

        Me._sigmaMachine.Analyze()
        Console.WriteLine(Me._sigmaMachine.AnalysisReport)

        Dim machineThread As Thread = New Thread(New ThreadStart(AddressOf Me.sigmaMachine.Activate))
        machineThread.Start()

        Dim i As Integer
        Do Until Me._sigmaMachine.IsStopRequested
            Me._sigmaMachine.EnqueueInput(sigma(i))
            If (DateTime.Now.Second Mod 31 = 0) Then
                Me._sigmaMachine.RequestStop()
            End If
            Thread.Sleep(600)
            i = (i + 1) Mod 3
        Loop
        ' wait till thread completes all actions.
        machineThread.Join()
    End Sub

    Private Sub ActThis(ByVal sender As Object, ByVal e As System.EventArgs) Handles _state1.Act, _state2.Act, _state3.Act
        Dim state As State = CType(sender, State)
        Console.WriteLine("{0} is running...", state.Name)
    End Sub

    Private Sub EnterThis(ByVal sender As Object, ByVal e As System.EventArgs) Handles _state1.Enter, _state2.Enter, _state3.Enter
        Dim state As State = CType(sender, State)
        Console.WriteLine("{0} entered.", state.Name)
    End Sub

    Private Sub LeaveThis(ByVal sender As Object, ByVal e As System.EventArgs) Handles _state1.Leave, _state2.Leave, _state3.Leave
        Dim state As State = CType(sender, State)
        Console.WriteLine("{0} left.", state.Name)
    End Sub

    Private Sub SigmaMachine_Acknowledge(ByVal sender As Object, ByVal e As System.EventArgs) Handles _sigmaMachine.Acknowledge
        Console.WriteLine("Got input: {0}", Me._sigmaMachine.PeekInput())
    End Sub

    Private Sub SignalMachine_Acknowledge(ByVal sender As Object, ByVal e As System.EventArgs) Handles _signalMachine.Acknowledge
        Console.WriteLine("Got input: {0}", Me._signalMachine.PeekInput())
    End Sub

    Private Sub StringMachine_Acknowledge(ByVal sender As Object, ByVal e As System.EventArgs) Handles _stringMachine.Acknowledge
        Console.WriteLine("Got input: {0}", Me._stringMachine.PeekInput())
    End Sub

    Private Sub Machine_Started(ByVal sender As Object, ByVal e As System.EventArgs) Handles _sigmaMachine.Started, _signalMachine.Started, _stringMachine.Started
        Console.WriteLine("Machine Started")
    End Sub

    Private Sub Machine_Stopped(ByVal sender As Object, ByVal e As System.EventArgs) Handles _sigmaMachine.Stopped, _signalMachine.Stopped, _stringMachine.Stopped
        Console.WriteLine("Machine Stopped")
    End Sub

    Private Sub Transition1_Completed(ByVal sender As Object, ByVal e As System.EventArgs) Handles _sigmaTransition1.Completed, _sigmaTransition2.Completed,
                                                                        _sigmaTransition3.Completed, _signalTransition1.Completed, _signalTransition2.Completed,
                                                                        _signalTransition3.Completed, _stringTransition1.Completed, _stringTransition2.Completed, _stringTransition3.Completed
        Console.WriteLine("transition completed.")

    End Sub

    Private Sub Transition1_Starting(ByVal sender As Object, ByVal e As System.EventArgs) Handles _sigmaTransition1.Starting, _sigmaTransition2.Starting, _sigmaTransition3.Starting,
                                                                        _signalTransition1.Starting, _signalTransition2.Starting, _signalTransition3.Starting,
                                                                        _stringTransition1.Starting, _stringTransition2.Starting, _stringTransition3.Starting
        Console.WriteLine("transition Starting.")

    End Sub

#End Region

#Region " QUERY MACHINE "

    Private WithEvents _QueryMachine As Moore.QueryAutomaton

    Public Sub RunQueryMachine()
        Me._queryMachine = New Moore.QueryAutomaton(1000)
        Me._queryMachine.Interval = 100
        Dim machineThread As Thread = New Thread(New ThreadStart(AddressOf Me.queryMachine.Activate))
        machineThread.Start()
        Do
            Thread.Sleep(75)
        Loop While Not Me._queryMachine.IsStopRequested
        ' wait till thread completes all actions.
        If machineThread.IsAlive Then
            Console.WriteLine("thread still alive")
        Else
            Console.WriteLine("thread not alive")
        End If
        machineThread.Join()
        Console.WriteLine("machine thread joined with main thread")
    End Sub

    Private Sub QueryMachine_Query(ByVal sender As Object, ByVal e As System.ComponentModel.HandledEventArgs) Handles _queryMachine.Query
        Dim value As Double = New Random(Now.Millisecond).NextDouble
        If value < 1 / 3 Then
            Console.WriteLine("handling query.")
            e.Handled = True
        ElseIf value < 2 / 3 Then
            Console.WriteLine("not handling query.")
            e.Handled = False
        Else
            Throw New Exception("Query failed")
        End If
    End Sub

    Private Sub QueryMachine_Started(ByVal sender As Object, ByVal e As System.EventArgs) Handles _queryMachine.Started
        Console.WriteLine("query machine started.")
    End Sub

    Private Sub QueryMachine_StateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _queryMachine.StateChanged
        Console.WriteLine("query machine state = {0}.", Me._queryMachine.CurrentStateCaption)
    End Sub

    Private Sub QueryMachine_Stopped(ByVal sender As Object, ByVal e As System.EventArgs) Handles _queryMachine.Stopped
        If Me._queryMachine.IsTimeout Then
            Console.WriteLine("query machine timeout.")
        ElseIf Me._queryMachine.IsCountOut Then
            Console.WriteLine("query machine count out.")
        Else
            Console.WriteLine("query machine stopped.")
        End If
    End Sub


#End Region

End Class


Public Enum SigmaSignal
    None
    One
    Two
    Three
End Enum

Public Class Sigma
    Public Sub New(ByVal value As SigmaSignal)
        Id = value
    End Sub
    Public Id As SigmaSignal
    Public Overrides Function ToString() As String
        Return String.Format("Sigma={0}", Id)
    End Function
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, Sigma))
    End Function
    Public Overloads Function Equals(ByVal value As isr.Algorithms.Automata.Sigma) As Boolean
        Return Me.Id.Equals(value.Id)
    End Function
End Class