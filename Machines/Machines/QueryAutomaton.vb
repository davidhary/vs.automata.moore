Imports System.ComponentModel
Imports isr.Core.EventHandlerExtensions
Imports isr.Automata

''' <summary>
''' An automaton class for implementing a finite automata for querying.
''' The automaton can be set to automatically stop after the query is done, failed, or timed out.
''' This automaton can be used to implement an automation for waiting for a service request. 
''' </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2010-09-06, 3.0.3901.x">
''' created
''' </para></remarks>
Public Class QueryAutomaton
    Inherits TimeoutMachine(Of QueryAutomatonSymbol, QueryAutomatonState)
    Implements IQueryAutomaton

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="queryTimeout">Specifies the query timeout interval in milliseconds</param>
    Public Sub New(ByVal queryTimeout As TimeSpan)

        MyBase.New()

        MyBase.OnsetDelay = TimeSpan.FromMilliseconds(0)
        MyBase.Interval = TimeSpan.FromMilliseconds(100)

        ' define the states.
        Me._readyState = MyBase.Add(QueryAutomatonState.Ready)
        Me._failureState = MyBase.Add(QueryAutomatonState.Failure)
        Me._queryingState = MyBase.Add(QueryAutomatonState.Querying, queryTimeout)
        Me._stopState = MyBase.Add(QueryAutomatonState.Stop)

        ' define the signals.
        For Each value As QueryAutomatonSymbol In [Enum].GetValues(GetType(QueryAutomatonSymbol))
            MyBase.Add(value)
        Next

        ' define the transitions

        ' READY TRANSITION
        For Each state As IState(Of QueryAutomatonState) In MyBase.States
            MyBase.Add(state, QueryAutomatonSymbol.Ready, Me._readyState)
        Next

        ' FAILURE TRANSITION
        For Each state As IState(Of QueryAutomatonState) In MyBase.States
            MyBase.Add(state, QueryAutomatonSymbol.Failure, Me._failureState)
        Next

        ' STOP TRANSITION
        For Each state As IState(Of QueryAutomatonState) In MyBase.States
            MyBase.Add(state, QueryAutomatonSymbol.Stop, Me._stopState)
        Next

        ' allow going from failure back to query.
        MyBase.Add(Me._failureState, QueryAutomatonSymbol.Query, Me._queryingState)
        MyBase.Add(Me._readyState, QueryAutomatonSymbol.Query, Me._queryingState)
        MyBase.Add(Me._queryingState, QueryAutomatonSymbol.Query, Me._queryingState)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' Free managed resources when explicitly called
                If Me.QueryEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.QueryEvent?.GetInvocationList
                        RemoveHandler Me.Query, CType(d, Global.System.EventHandler(Of HandledEventArgs))
                    Next
                End If
                If Me._failureState IsNot Nothing Then Me._failureState = Nothing
                If Me._readyState IsNot Nothing Then Me._readyState = Nothing
                If Me._queryingState IsNot Nothing Then Me._queryingState = Nothing
                If Me._stopState IsNot Nothing Then Me._stopState = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " STATES "

    Private WithEvents _ReadyState As IState(Of QueryAutomatonState)
    ''' <summary>
    ''' Gets reference to the <see cref="ReadyState">Ready State</see>.
    ''' </summary>
    Protected ReadOnly Property ReadyState() As IState(Of QueryAutomatonState)
        Get
            Return Me._readyState
        End Get
    End Property

    Private WithEvents _QueryingState As ITimeoutState(Of QueryAutomatonState)
    ''' <summary>
    ''' Gets reference to the <see cref="QueryingState">Querying State</see>.
    ''' </summary>
    Protected ReadOnly Property QueryingState() As ITimeoutState(Of QueryAutomatonState)
        Get
            Return Me._queryingState
        End Get
    End Property

    Private WithEvents _FailureState As IState(Of QueryAutomatonState)
    ''' <summary>
    ''' Gets reference to the <see cref="FailureState">Failure State</see>.
    ''' </summary>
    Protected ReadOnly Property FailureState() As IState(Of QueryAutomatonState)
        Get
            Return Me._failureState
        End Get
    End Property

    Private WithEvents _StopState As IState(Of QueryAutomatonState)
    ''' <summary>
    ''' Gets reference to the <see cref="StopState">Stop State</see>.
    ''' </summary>
    Protected ReadOnly Property StopState() As IState(Of QueryAutomatonState)
        Get
            Return Me._stopState
        End Get
    End Property

#End Region

#Region " OVERRIDES "

    ''' <summary> Enumerates symbol name in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process symbol name in this collection.
    ''' </returns>
    Public Overrides Function InputSymbolNames() As IEnumerable(Of String)
        Return [Enum].GetNames(GetType(QueryAutomatonSymbol))
    End Function

    ''' <summary> Gets the Fail input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property FailInputSymbol As QueryAutomatonSymbol
        Get
            Return QueryAutomatonSymbol.Failure
        End Get
    End Property

    ''' <summary> Gets the Initial input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property InitialInputSymbol As QueryAutomatonSymbol
        Get
            Return QueryAutomatonSymbol.Ready
        End Get
    End Property

    ''' <summary> Gets the terminal input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property TerminalInputSymbol As QueryAutomatonSymbol
        Get
            Return QueryAutomatonSymbol.Stop
        End Get
    End Property

    Public Overrides ReadOnly Property InitialStateIdentity As QueryAutomatonState
        Get
            Return QueryAutomatonState.Ready
        End Get
    End Property

    Public Overrides ReadOnly Property NullStateIdentity As QueryAutomatonState
        Get
            Return QueryAutomatonState.None
        End Get
    End Property

    Public Overrides ReadOnly Property TerminalStateIdentity As QueryAutomatonState
        Get
            Return QueryAutomatonState.Stop
        End Get
    End Property

#End Region

#Region " STATE EVENTS "

    Public Overrides Sub EnqueueStopSymbol()
        Me.EnqueueInput(QueryAutomatonSymbol.Stop, True)
    End Sub

    ''' <summary>
    ''' Handles failure state action. Sends the machine to the stop state if the failure was handled. If 
    ''' handling failed, sends the machine to another failure. 
    ''' </summary>
    Private Sub FailureState_enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _failureState.Enter
        Me.EnqueueInput(QueryAutomatonSymbol.Stop, True)
    End Sub

    Private Sub Timeout(ByVal sender As Object, ByVal e As System.EventArgs) Handles _queryingState.Timeout
        Me._isTimeout = True
        EnqueueInput(QueryAutomatonSymbol.Failure, True)
    End Sub

    Private Sub QueryingState_enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _queryingState.Enter
        Me.EnqueueInput(QueryAutomatonSymbol.Query)
    End Sub

    ''' <summary>
    ''' Issues the query request. The query request should turn on the handled flag when done. Otherwise, the 
    ''' query will repeat until satisfied or timed out.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub QueryingState_Reenter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _queryingState.Reenter
        Dim eventArgs As New HandledEventArgs
        Me.OnQuery(eventArgs)
        If eventArgs.Handled Then
            Me.EnqueueInput(QueryAutomatonSymbol.Stop, True)
        Else
            Me.EnqueueInput(QueryAutomatonSymbol.Query)
        End If
    End Sub

    Private Sub StopState_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _stopState.Enter
        MyBase.RequestStop()
    End Sub

#End Region

#Region " EXECUTION "

    ''' <summary>
    ''' Starts the state machine by <see cref="Initiate">initiating</see> the start state, enqueuing 
    ''' the <see cref="QueryAutomatonSymbol.Query">query</see> signal and then 
    ''' <see cref="Sequence">repeating the sequence</see> until receiving a <see cref="RequestStop">stop request.</see>
    ''' </summary>
    Protected Overrides Sub StartSequence() Implements IQueryAutomaton.StartSequence
        Me.Initiate(Me.OnsetDelay)
        Me.EnqueueInput(QueryAutomatonSymbol.Query, False, False)
        Me.Sequence()
    End Sub

    ''' <summary>
    ''' Moves the state machine to the Start State. This action clears the input QUEUE. It leaves the
    ''' machine stopped with the
    ''' <see cref="P:isr.Automata.Machine`2.IsStopRequested">
    ''' stop requested sentinel</see>
    ''' turned on, which is required for the machine to resume its actions.
    ''' </summary>
    ''' <param name="onsetDelay"> The onset delay. </param>
    Public Overrides Sub Initiate(ByVal onsetDelay As TimeSpan) Implements IQueryAutomaton.Initiate
        Me._IsTimeout = False
        MyBase.Initiate(onsetDelay)
    End Sub

    ''' <summary>
    ''' Runs the state machine sequence until receiving
    ''' <see cref="M:isr.Automata.Machine`2.RequestStop">
    ''' stop request.</see>
    ''' The sequence will run only after a previous stop requested that leaves
    ''' <see cref="P:isr.Automata.Machine`2.IsStopRequested">
    ''' stop requested sentinel</see>
    ''' turned on.
    ''' </summary>
    Protected Overrides Sub Sequence() Implements IQueryAutomaton.Sequence
        Me._isTimeout = False
        MyBase.Sequence()
    End Sub

    ''' <summary> Initiates this object. </summary>
    Public Overloads Sub Initiate() Implements IQueryAutomaton.Initiate
        MyBase.Initiate(Me.OnsetDelay)
    End Sub

#End Region

#Region " ACTION EVENTS: QUERY "

    ''' <summary>Raises the Query event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="HandledEventArgs">event handled arguments</see>
    ''' </param>
    Protected Overridable Sub OnQuery(ByVal e As HandledEventArgs) Implements IQueryAutomaton.OnQuery
        Me.QueryEvent.SyncNotify(Me, e) ' .SafeBeginEndInvoke(Me, e)
    End Sub

    ''' <summary>Occurs upon state deactivation (exit).</summary>
    ''' <remarks>Use this event to notify the container class that the state was deactivated.</remarks>
    Public Event Query As EventHandler(Of HandledEventArgs) Implements IQueryAutomaton.Query

#End Region

#Region " ACTION EVENTS: TIMEOUT "

    Private _IsTimeout As Boolean
    ''' <summary>
    ''' Gets the sentinel indicating that a state timed out.
    ''' </summary>
    Public ReadOnly Property IsTimeout() As Boolean Implements IQueryAutomaton.IsTimeout
        Get
            Return Me._isTimeout
        End Get
    End Property

#End Region

#Region " PROGRESS INDICATOR "

    ''' <summary> Gets the Query progress indicator maximum. </summary>
    ''' <value> The Query progress indicator maximum. </value>
    Public Shared ReadOnly Property ProgressIndicatorMinimum As Integer
        Get
            Return ProgressIndicator(QueryAutomatonState.None)
        End Get
    End Property

    ''' <summary> Gets the Query progress indicator minimum. </summary>
    ''' <value> The Query progress indicator minimum. </value>
    Public Shared ReadOnly Property ProgressIndicatorMaximum As Integer
        Get
            Return ProgressIndicator(QueryAutomatonState.Querying)
        End Get
    End Property

    Private Shared _ProgressIndicatorDix As Dictionary(Of QueryAutomatonState, Integer)

    ''' <summary> Query progress indicator. </summary>
    ''' <param name="state"> The state. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function ProgressIndicator(ByVal state As QueryAutomatonState) As Integer
        Dim result As Integer
        If QueryAutomaton._ProgressIndicatorDix Is Nothing Then
            QueryAutomaton._ProgressIndicatorDix = New Dictionary(Of QueryAutomatonState, Integer) From {
                {QueryAutomatonState.None, 0},
                {QueryAutomatonState.Failure, 0},
                {QueryAutomatonState.Stop, 0},
                {QueryAutomatonState.Ready, 1},
                {QueryAutomatonState.Querying, 2}
            }
            For Each value As QueryAutomatonState In [Enum].GetValues(GetType(QueryAutomatonState))
                Try
                    result = QueryAutomaton._ProgressIndicatorDix(value)
                Catch ex As System.Collections.Generic.KeyNotFoundException
                    ex.Data.Add($"Coding Error {DateTime.Now.Ticks}", $"Progress indicator for Query Automaton is missing value={value}")
                    Throw ex
                End Try
            Next
        End If
        result = QueryAutomaton._ProgressIndicatorDix(state)
        Return result
    End Function

    ''' <summary> Updates the progress indicator percent. </summary>
    Public Overrides Sub UpdateProgressIndicatorPercent()
        Dim value As Integer = CInt(100 * (ProgressIndicator(Me.CurrentState.Identity) - ProgressIndicatorMinimum) /
                                         (ProgressIndicatorMaximum - ProgressIndicatorMinimum))
        value = If(value < 0, 0, If(value > 100, 100, value))
        Me.ProgressIndicatorPercent = value
        Me.PercentProgress = Me.ProgressIndicatorPercent
    End Sub

    Public Overrides Sub UpdatePercentProgress()
        Me.PercentProgress = Me.ProgressIndicatorPercent
    End Sub

#End Region

End Class

''' <summary>
''' Enumerates the state transition symbols for the 
''' <see cref="QueryAutomaton">Ready-Query</see> automaton.
''' </summary>
Public Enum QueryAutomatonSymbol
    <Description("Not Defined")> None
    <Description("Query: Ready, Querying -> Querying")> Query
    <Description("Failure: Any State -> Failure")> Failure
    <Description("Ready: Any state -> Ready")> Ready
    <Description("Stop: Any state -> Stop")> [Stop]
End Enum

''' <summary> Values that represent query automaton states. </summary>
Public Enum QueryAutomatonState
    <Description("Not Defined")> None
    <Description("Ready")> Ready
    <Description("Querying")> Querying
    <Description("Failure")> Failure
    <Description("Stop")> [Stop]
End Enum
