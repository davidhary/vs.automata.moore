Imports System.ComponentModel
Imports isr.Core.EventHandlerExtensions
''' <summary>
''' A base automaton class for implementing a finite automata for opening, working, and closing constructs.
''' </summary>
''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' created
''' </para></remarks>
Public Class ConnectWorkDisconnectAutomaton
    Inherits Machine(Of ConnectWorkDisconnectSymbol, ConnectWorkDisconnectState)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>Constructs this class.</summary>
    Public Sub New()
        MyBase.New("Connect Work")

        _EventLocker = New Object
        Me.OnsetDelay = TimeSpan.FromMilliseconds(500)
        Me.Interval = TimeSpan.FromMilliseconds(1000)

        ' define the states.
        Me._IdleState = MyBase.Add(ConnectWorkDisconnectState.Idle)
        Me._FailureState = MyBase.Add(ConnectWorkDisconnectState.Failure)
        Me._ConnectingState = MyBase.Add(ConnectWorkDisconnectState.Connecting)
        Me._ConnectedState = MyBase.Add(ConnectWorkDisconnectState.Connected)
        Me._WorkingState = MyBase.Add(ConnectWorkDisconnectState.Working)
        Me._WorkDoneState = MyBase.Add(ConnectWorkDisconnectState.WorkDone)
        Me._DisconnectingState = MyBase.Add(ConnectWorkDisconnectState.Disconnecting)
        Me._DisconnectedState = MyBase.Add(ConnectWorkDisconnectState.Disconnected)

        ' define the signals.
        For Each value As ConnectWorkDisconnectSymbol In [Enum].GetValues(GetType(ConnectWorkDisconnectSymbol))
            MyBase.Add(value)
        Next

        ' define the transitions

        ' IDLE TRANSITION
        For Each state As IState(Of ConnectWorkDisconnectState) In MyBase.States
            MyBase.Add(state, ConnectWorkDisconnectSymbol.Idle, Me._IdleState)
        Next

        ' FAILURE TRANSITION
        For Each state As IState(Of ConnectWorkDisconnectState) In MyBase.States
            MyBase.Add(state, ConnectWorkDisconnectSymbol.Failure, Me._FailureState)
        Next

        MyBase.Add(Me._IdleState, ConnectWorkDisconnectSymbol.StartConnecting, Me._ConnectingState)

        MyBase.Add(Me._ConnectingState, ConnectWorkDisconnectSymbol.FinishConnecting, Me._ConnectedState)
        MyBase.Add(Me._ConnectingState, ConnectWorkDisconnectSymbol.StartDisconnecting, Me._DisconnectingState)

        MyBase.Add(Me._ConnectedState, ConnectWorkDisconnectSymbol.StartWorking, Me._WorkingState)
        MyBase.Add(Me._ConnectedState, ConnectWorkDisconnectSymbol.StartDisconnecting, Me._DisconnectingState)

        MyBase.Add(Me._WorkingState, ConnectWorkDisconnectSymbol.FinishWorking, Me._WorkDoneState)
        MyBase.Add(Me._WorkingState, ConnectWorkDisconnectSymbol.StartDisconnecting, Me._DisconnectingState)

        MyBase.Add(Me._WorkDoneState, ConnectWorkDisconnectSymbol.StartWorking, Me._WorkingState)
        MyBase.Add(Me._WorkDoneState, ConnectWorkDisconnectSymbol.StartDisconnecting, Me._DisconnectingState)

        MyBase.Add(Me._DisconnectingState, ConnectWorkDisconnectSymbol.FinishDisconnecting, Me._DisconnectedState)

        MyBase.Add(Me._DisconnectedState, ConnectWorkDisconnectSymbol.StartConnecting, Me._ConnectingState)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.ConnectedEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.ConnectedEvent?.GetInvocationList
                        RemoveHandler Me.Connected, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If
                If Me.ConnectingEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.ConnectingEvent?.GetInvocationList
                        RemoveHandler Me.Connecting, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If
                If Me.DisconnectedEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.DisconnectedEvent?.GetInvocationList
                        RemoveHandler Me.Disconnected, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If
                If Me.DisconnectingEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.DisconnectingEvent?.GetInvocationList
                        RemoveHandler Me.Disconnecting, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If
                If Me.FailureEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.FailureEvent?.GetInvocationList
                        RemoveHandler Me.Failure, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If
                If Me.ReadyEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.ReadyEvent?.GetInvocationList
                        RemoveHandler Me.Ready, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If
                If Me.WorkDoneEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.WorkDoneEvent?.GetInvocationList
                        RemoveHandler Me.WorkDone, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If
                If Me.WorkingEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.WorkingEvent?.GetInvocationList
                        RemoveHandler Me.Working, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If

                ' Free managed resources when explicitly called
                If Me._IdleState IsNot Nothing Then Me._IdleState = Nothing
                If Me._ConnectingState IsNot Nothing Then Me._ConnectingState = Nothing
                If Me._ConnectedState IsNot Nothing Then Me._ConnectedState = Nothing
                If Me._WorkingState IsNot Nothing Then Me._WorkingState = Nothing
                If Me._WorkDoneState IsNot Nothing Then Me._WorkDoneState = Nothing
                If Me._DisconnectingState IsNot Nothing Then Me._DisconnectingState = Nothing
                If Me._DisconnectedState IsNot Nothing Then Me._DisconnectedState = Nothing
                If Me._FailureState IsNot Nothing Then Me._FailureState = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EVENTS "

    Private ReadOnly _EventLocker As Object

    ''' <summary>Raises the Ready event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnReady(ByVal e As System.EventArgs)
        SyncLock _EventLocker
            Me.ReadyEvent.SyncNotify(Me, e)
        End SyncLock
    End Sub

    ''' <summary>Occurs upon machine Ready.</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Public Event Ready As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the Connecting event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnConnecting(ByVal e As System.EventArgs)
        SyncLock _EventLocker
            Me.ConnectingEvent.SyncNotify(Me, e) ' .SafeBeginEndInvoke(Me, e)
        End SyncLock
    End Sub

    ''' <summary>Occurs upon machine connecting.</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Public Event Connecting As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the Connected event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnConnected(ByVal e As System.EventArgs)
        SyncLock _EventLocker
            Me.ConnectedEvent.SyncNotify(Me, e) ' .SafeBeginEndInvoke(Me, e)
        End SyncLock
    End Sub

    ''' <summary>Occurs upon machine Connected.</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Public Event Connected As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the Disconnecting event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnDisconnecting(ByVal e As System.EventArgs)
        SyncLock _EventLocker
            Me.DisconnectingEvent.SyncNotify(Me, e) ' .SafeBeginEndInvoke(Me, e)
        End SyncLock
    End Sub

    ''' <summary>Occurs upon machine Disconnecting.</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Public Event Disconnecting As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the Disconnected event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnDisconnected(ByVal e As System.EventArgs)
        SyncLock _EventLocker
            Me.DisconnectedEvent.SyncNotify(Me, e) ' .SafeBeginEndInvoke(Me, e)
        End SyncLock
    End Sub

    ''' <summary>Occurs upon machine Disconnected.</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Public Event Disconnected As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the Working event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnWorking(ByVal e As System.EventArgs)
        SyncLock _EventLocker
            Me.WorkingEvent.SyncNotify(Me, e) ' .SafeBeginEndInvoke(Me, e)
        End SyncLock
    End Sub

    ''' <summary>Occurs upon machine Working.</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Public Event Working As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the WorkDone event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnWorkDone(ByVal e As System.EventArgs)
        SyncLock _EventLocker
            Me.WorkDoneEvent.SyncNotify(Me, e) ' .SafeBeginEndInvoke(Me, e)
        End SyncLock
    End Sub

    ''' <summary>Occurs upon machine WorkDone.</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Public Event WorkDone As EventHandler(Of System.EventArgs)

    ''' <summary> Gets the failure count down. </summary>
    ''' <value> The failure count down. </value>
    Private Property FailureCountDown As Integer

    ''' <summary>Raises the Failure event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnFailure(ByVal e As System.EventArgs)

        SyncLock _EventLocker
            Me.FailureEvent.SyncNotify(Me, e) ' .SafeBeginEndInvoke(Me, e)
            If Me.FailureCountDown > 0 Then Me.FailureCountDown -= 1
            If Me.FailureCountDown <= 0 Then Me.FailureState.OnFailed()
        End SyncLock
    End Sub

    ''' <summary>Occurs upon machine Failure.</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Public Event Failure As EventHandler(Of System.EventArgs)

#End Region

#Region " STATES "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _IdleState As IState(Of ConnectWorkDisconnectState)
#Enable Warning IDE1006 ' Naming Styles
    ''' <summary>
    ''' Gets reference to the <see cref="IdleState">Idle State</see>.
    ''' </summary>
    Public ReadOnly Property IdleState() As IState(Of ConnectWorkDisconnectState)
        Get
            Return Me._idleState
        End Get
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _ConnectingState As IState(Of ConnectWorkDisconnectState)
#Enable Warning IDE1006 ' Naming Styles
    ''' <summary>
    ''' Gets reference to the <see cref="ConnectingState">Connecting State</see>.
    ''' </summary>
    Public ReadOnly Property ConnectingState() As IState(Of ConnectWorkDisconnectState)
        Get
            Return Me._connectingState
        End Get
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _ConnectedState As IState(Of ConnectWorkDisconnectState)
#Enable Warning IDE1006 ' Naming Styles
    ''' <summary>
    ''' Gets reference to the <see cref="ConnectedState">Connected State</see>.
    ''' </summary>
    Public ReadOnly Property ConnectedState() As IState(Of ConnectWorkDisconnectState)
        Get
            Return Me._connectedState
        End Get
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _WorkingState As IState(Of ConnectWorkDisconnectState)
#Enable Warning IDE1006 ' Naming Styles
    ''' <summary>
    ''' Gets reference to the <see cref="WorkingState">Working State</see>.
    ''' </summary>
    Public ReadOnly Property WorkingState() As IState(Of ConnectWorkDisconnectState)
        Get
            Return Me._workingState
        End Get
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _WorkDoneState As IState(Of ConnectWorkDisconnectState)
#Enable Warning IDE1006 ' Naming Styles
    ''' <summary>
    ''' Gets reference to the <see cref="WorkDoneState">WorkDone State</see>.
    ''' </summary>
    Public ReadOnly Property WorkDoneState() As IState(Of ConnectWorkDisconnectState)
        Get
            Return Me._workDoneState
        End Get
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DisconnectingState As IState(Of ConnectWorkDisconnectState)
#Enable Warning IDE1006 ' Naming Styles
    ''' <summary>
    ''' Gets reference to the <see cref="DisconnectingState">Disconnecting State</see>.
    ''' </summary>
    Public ReadOnly Property DisconnectingState() As IState(Of ConnectWorkDisconnectState)
        Get
            Return Me._disconnectingState
        End Get
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DisconnectedState As IState(Of ConnectWorkDisconnectState)
#Enable Warning IDE1006 ' Naming Styles
    ''' <summary>
    ''' Gets reference to the <see cref="DisconnectedState">Disconnected State</see>.
    ''' </summary>
    Public ReadOnly Property DisconnectedState() As IState(Of ConnectWorkDisconnectState)
        Get
            Return Me._disconnectedState
        End Get
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _FailureState As IState(Of ConnectWorkDisconnectState)
#Enable Warning IDE1006 ' Naming Styles
    ''' <summary>
    ''' Gets reference to the <see cref="FailureState">Failure State</see>.
    ''' </summary>
    Public ReadOnly Property FailureState() As IState(Of ConnectWorkDisconnectState)
        Get
            Return Me._failureState
        End Get
    End Property

#End Region

#Region " OVERRIDES "

    ''' <summary> Enumerates symbol name in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process symbol name in this collection.
    ''' </returns>
    Public Overrides Function InputSymbolNames() As IEnumerable(Of String)
        Return [Enum].GetNames(GetType(ConnectWorkDisconnectSymbol))
    End Function

    ''' <summary> Gets the Fail input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property FailInputSymbol As ConnectWorkDisconnectSymbol
        Get
            Return ConnectWorkDisconnectSymbol.Failure
        End Get
    End Property

    ''' <summary> Gets the Initial input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property InitialInputSymbol As ConnectWorkDisconnectSymbol
        Get
            Return ConnectWorkDisconnectSymbol.Idle
        End Get
    End Property

    ''' <summary> Gets the terminal input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property TerminalInputSymbol As ConnectWorkDisconnectSymbol
        Get
            Return ConnectWorkDisconnectSymbol.FinishDisconnecting
        End Get
    End Property

    Public Overrides ReadOnly Property InitialStateIdentity As ConnectWorkDisconnectState
        Get
            Return ConnectWorkDisconnectState.Idle
        End Get
    End Property

    Public Overrides ReadOnly Property NullStateIdentity As ConnectWorkDisconnectState
        Get
            Return ConnectWorkDisconnectState.None
        End Get
    End Property

    Public Overrides ReadOnly Property TerminalStateIdentity As ConnectWorkDisconnectState
        Get
            Return ConnectWorkDisconnectState.WorkDone
        End Get
    End Property

#End Region

#Region " STATE EVENTS "

    Private Sub ConnectingState_enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _connectingState.Enter
        Me.OnConnecting(System.EventArgs.Empty)
    End Sub

    Private Sub ConnectedState_enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _connectedState.Enter
        Me.OnConnected(System.EventArgs.Empty)
    End Sub

    Private Sub DisconnectingState_enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _disconnectingState.Enter
        Me.OnDisconnecting(System.EventArgs.Empty)
    End Sub

    Private Sub DisconnectedState_enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _disconnectedState.Enter
        Me.OnDisconnected(System.EventArgs.Empty)
    End Sub

    Private Sub FailureState_enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _failureState.Enter
        Me.OnFailure(System.EventArgs.Empty)
    End Sub

    Private Sub IdleState_enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _idleState.Enter
        Me.OnReady(System.EventArgs.Empty)
    End Sub

    Private Sub WorkDoneState_enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _workDoneState.Enter
        Me.OnWorkDone(System.EventArgs.Empty)
    End Sub

    Private Sub WorkingState_enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles _workingState.Enter
        Me.OnWorking(System.EventArgs.Empty)
    End Sub

    Public Overrides Sub EnqueueStopSymbol()
        Me.EnqueueInput(ConnectWorkDisconnectSymbol.FinishWorking)
    End Sub

#End Region

#Region " ACTIVATION "

    Protected Overrides Sub StartSequence()
        Me.FailureCountDown = 3
        MyBase.StartSequence()
    End Sub

#End Region

#Region " PROGRESS INDICATOR "

    ''' <summary> Gets the Connect-Work-Disconnect progress indicator maximum. </summary>
    ''' <value> The Connect-Work-Disconnect progress indicator maximum. </value>
    Public Shared ReadOnly Property ProgressIndicatorMinimum As Integer
        Get
            Return ProgressIndicator(ConnectWorkDisconnectState.Failure)
        End Get
    End Property

    ''' <summary> Gets the Connect-Work-Disconnect progress indicator minimum. </summary>
    ''' <value> The Connect-Work-Disconnect progress indicator minimum. </value>
    Public Shared ReadOnly Property ProgressIndicatorMaximum As Integer
        Get
            Return ProgressIndicator(ConnectWorkDisconnectState.WorkDone)
        End Get
    End Property

    Private Shared _ProgressIndicatorDix As Dictionary(Of ConnectWorkDisconnectState, Integer)

    ''' <summary> Connect-Work-Disconnect progress indicator. </summary>
    ''' <param name="state"> The state. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function ProgressIndicator(ByVal state As ConnectWorkDisconnectState) As Integer
        Dim result As Integer
        If ConnectWorkDisconnectAutomaton._ProgressIndicatorDix Is Nothing Then
            ConnectWorkDisconnectAutomaton._ProgressIndicatorDix = New Dictionary(Of ConnectWorkDisconnectState, Integer) From {
                {ConnectWorkDisconnectState.Failure, 0},
                {ConnectWorkDisconnectState.Idle, 1},
                {ConnectWorkDisconnectState.Disconnected, 2},
                {ConnectWorkDisconnectState.Disconnecting, 3},
                {ConnectWorkDisconnectState.Connecting, 4},
                {ConnectWorkDisconnectState.Connected, 5},
                {ConnectWorkDisconnectState.Working, 6},
                {ConnectWorkDisconnectState.WorkDone, 7}
            }
            For Each value As ConnectWorkDisconnectState In [Enum].GetValues(GetType(ConnectWorkDisconnectState))
                Try
                    result = ConnectWorkDisconnectAutomaton._ProgressIndicatorDix(value)
                Catch ex As System.Collections.Generic.KeyNotFoundException
                    ex.Data.Add($"Coding Error {DateTime.Now.Ticks}", $"Progress indicator for Connect-Work-Disconnect Automaton is missing value={value}")
                    Throw ex
                End Try
            Next
        End If
        result = ConnectWorkDisconnectAutomaton._ProgressIndicatorDix(state)
        Return result
    End Function

    ''' <summary> Updates the progress indicator percent. </summary>
    Public Overrides Sub UpdateProgressIndicatorPercent()
        Dim value As Integer = CInt(100 * (ProgressIndicator(Me.CurrentState.Identity) - ProgressIndicatorMinimum) /
                                         (ProgressIndicatorMaximum - ProgressIndicatorMinimum))
        value = If(value < 0, 0, If(value > 100, 100, value))
        Me.ProgressIndicatorPercent = value
        Me.PercentProgress = Me.ProgressIndicatorPercent
    End Sub

    Public Overrides Sub UpdatePercentProgress()
        Me.PercentProgress = Me.ProgressIndicatorPercent
    End Sub
#End Region

End Class

''' <summary>
''' Enumerates the state transition symbols for the 
''' <see cref="ConnectWorkDisconnectAutomaton">Connection</see> automaton.
''' </summary>
Public Enum ConnectWorkDisconnectSymbol
    <Description("Not Defined")> None
    <Description("Start Connecting: Idle -> Connecting")> StartConnecting
    <Description("Finish Connecting: Connecting -> Connected")> FinishConnecting
    <Description("Start Working: Idle -> Working")> StartWorking
    <Description("Finish Working: Working -> Work Done")> FinishWorking
    <Description("Start Disconnecting: Idle -> Disconnecting")> StartDisconnecting
    <Description("Finish Disconnecting: Disconnecting -> Disconnected")> FinishDisconnecting
    <Description("Failure: Any State -> Failure")> Failure
    <Description("Idle: .. -> Idle")> Idle
End Enum

''' <summary> Values that represent connect work disconnect states. </summary>
Public Enum ConnectWorkDisconnectState
    <Description("Not specified")> None
    <Description("Idle")> Idle
    <Description("Connecting")> Connecting
    <Description("Connected")> Connected
    <Description("Working")> Working
    <Description("WorkDone")> WorkDone
    <Description("Disconnecting")> Disconnecting
    <Description("Disconnected")> Disconnected
    <Description("Failure")> Failure
End Enum
