Imports System.ComponentModel
Imports isr.Automata.Finite
''' <summary>
''' OSHA-Compatible Bi-Manual Enabler State Machine
''' </summary>
''' <remarks> (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 1998-05-02, 1.15.0000. </para></remarks>
Public Class BimanualEnabler
    Inherits Engine(Of BimanualEnablerSymbol, BimanualEnablerState)

#Region " CONSTRUCTION and CLEANUP "

    Public Sub New()
        MyBase.New("Bi-Manual Enabler")

        Me._ResponseStopwatch = New Stopwatch

        ' define the states.
        ' The origin will be the start state.
        Me._OriginState = MyBase.Add(BimanualEnablerState.Origin)
        Me._readyState = MyBase.Add(BimanualEnablerState.Ready)
        Me._SetState = MyBase.Add(BimanualEnablerState.Set)
        Me._GoState = MyBase.Add(BimanualEnablerState.Go)
        Me._EnabledState = MyBase.Add(BimanualEnablerState.Enabled)
        Me._EstopState = MyBase.Add(BimanualEnablerState.Estop)
        Me._failureState = MyBase.Add(BimanualEnablerState.Failure)

        ' define the signals.
        For Each value As BimanualEnablerSymbol In [Enum].GetValues(GetType(BimanualEnablerSymbol))
            MyBase.Add(value)
        Next

        ' define the transitions

        ' FAILURE TRANSITION
        For Each state As IState(Of BimanualEnablerState) In MyBase.States
            MyBase.Add(state, BimanualEnablerSymbol.Failure, Me._failureState)
        Next

        ' ORIGIN TRANSITION
        For Each state As IState(Of BimanualEnablerState) In MyBase.States
            MyBase.Add(state, BimanualEnablerSymbol.Origin, Me._OriginState)
        Next

        ' READY TRANSITION
        For Each state As IState(Of BimanualEnablerState) In MyBase.States
            MyBase.Add(state, BimanualEnablerSymbol.Ready, Me._readyState)
        Next

        ' ESTOP TRANSITION
        For Each state As IState(Of BimanualEnablerState) In MyBase.States
            MyBase.Add(state, BimanualEnablerSymbol.Estop, Me._EstopState)
        Next

        ' BUTTON PRESSED TRANSITION
        MyBase.Add(Me._OriginState, BimanualEnablerSymbol.ButtonPressed, Me._OriginState)
        MyBase.Add(Me._readyState, BimanualEnablerSymbol.ButtonPressed, Me._SetState)
        MyBase.Add(Me._SetState, BimanualEnablerSymbol.ButtonPressed, Me._GoState)

        ' BUTTON REEASED TRANSITION
        MyBase.Add(Me._OriginState, BimanualEnablerSymbol.ButtonReleased, Me._OriginState)
        MyBase.Add(Me._readyState, BimanualEnablerSymbol.ButtonReleased, Me._readyState)
        MyBase.Add(Me._SetState, BimanualEnablerSymbol.ButtonReleased, Me._readyState)
        MyBase.Add(Me._GoState, BimanualEnablerSymbol.ButtonReleased, Me._readyState)
        MyBase.Add(Me._EnabledState, BimanualEnablerSymbol.ButtonReleased, Me._readyState)

        ' TIMEOUT TRANSITION
        MyBase.Add(Me._SetState, BimanualEnablerSymbol.Timeout, Me._readyState)

        ' ACKNOWLEDGE TRANSITION
        MyBase.Add(Me._GoState, BimanualEnablerSymbol.Acknowledge, Me._EnabledState)

    End Sub

#End Region

#Region " STATES "

    Private WithEvents _OriginState As IState(Of BimanualEnablerState)
    ''' <summary>
    ''' Gets reference to the <see cref="OriginState">Origin State</see>.
    ''' </summary>
    Public ReadOnly Property OriginState() As IState(Of BimanualEnablerState)
        Get
            Return Me._OriginState
        End Get
    End Property


    Private WithEvents _ReadyState As IState(Of BimanualEnablerState)
    ''' <summary>
    ''' Gets reference to the <see cref="ReadyState">Ready State</see>.
    ''' </summary>
    Public ReadOnly Property ReadyState() As IState(Of BimanualEnablerState)
        Get
            Return Me._readyState
        End Get
    End Property

    Private WithEvents _SetState As IState(Of BimanualEnablerState)
    ''' <summary>
    ''' Gets reference to the <see cref="SetState">Set State</see>.
    ''' </summary>
    Public ReadOnly Property SetState() As IState(Of BimanualEnablerState)
        Get
            Return Me._SetState
        End Get
    End Property

    Private WithEvents _GoState As IState(Of BimanualEnablerState)
    ''' <summary>
    ''' Gets reference to the <see cref="GoState">Go State</see>.
    ''' </summary>
    Public ReadOnly Property GoState() As IState(Of BimanualEnablerState)
        Get
            Return Me._GoState
        End Get
    End Property

    Private WithEvents _EnabledState As IState(Of BimanualEnablerState)
    ''' <summary>
    ''' Gets reference to the <see cref="EnabledState">Enabled State</see>.
    ''' </summary>
    Public ReadOnly Property EnabledState() As IState(Of BimanualEnablerState)
        Get
            Return Me._EnabledState
        End Get
    End Property

    Private WithEvents _EstopState As IState(Of BimanualEnablerState)
    ''' <summary>
    ''' Gets reference to the <see cref="EstopState">EStop State</see>.
    ''' </summary>
    Public ReadOnly Property EstopState() As IState(Of BimanualEnablerState)
        Get
            Return Me._EstopState
        End Get
    End Property

    Private WithEvents _FailureState As IState(Of BimanualEnablerState)
    ''' <summary>
    ''' Gets reference to the <see cref="FailureState">Failure State</see>.
    ''' </summary>
    Public ReadOnly Property FailureState() As IState(Of BimanualEnablerState)
        Get
            Return Me._failureState
        End Get
    End Property

#End Region

#Region " OVERRIDES "

    ''' <summary> Enumerates symbol name in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process symbol name in this collection.
    ''' </returns>
    Public Overrides Function InputSymbolNames() As IEnumerable(Of String)
        Return [Enum].GetNames(GetType(BimanualEnablerSymbol))
    End Function

    ''' <summary> Gets the Fail input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property FailInputSymbol As BimanualEnablerSymbol
        Get
            Return BimanualEnablerSymbol.Failure
        End Get
    End Property

    ''' <summary> Gets the Initial input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property InitialInputSymbol As BimanualEnablerSymbol
        Get
            Return BimanualEnablerSymbol.Origin
        End Get
    End Property

    ''' <summary> Gets the terminal input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property TerminalInputSymbol As BimanualEnablerSymbol
        Get
            Return BimanualEnablerSymbol.Estop
        End Get
    End Property

    Public Overrides ReadOnly Property InitialStateIdentity As BimanualEnablerState
        Get
            Return BimanualEnablerState.Origin
        End Get
    End Property

    Public Overrides ReadOnly Property NullStateIdentity As BimanualEnablerState
        Get
            Return BimanualEnablerState.None
        End Get
    End Property

    Public Overrides ReadOnly Property TerminalStateIdentity As BimanualEnablerState
        Get
            Return BimanualEnablerState.Estop
        End Get
    End Property

#End Region

#Region " STATE EVENTS "

    Private Sub SetState_Enter(sender As Object, e As EventArgs) Handles _SetState.Enter
        Me.ResponseStopwatch.Restart()
    End Sub

    Private Sub ReadyState_Enter(sender As Object, e As EventArgs) Handles _readyState.Enter
        Me.ResponseStopwatch.Reset()
    End Sub

    Private Sub GoState_Enter(sender As Object, e As EventArgs) Handles _GoState.Enter
        Me.ResponseStopwatch.Reset()
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the response stopwatch. </summary>
    ''' <value> The response stopwatch. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Property ResponseStopwatch As Stopwatch

    ''' <summary>
    ''' The required response time for closing the button.
    ''' For the machine to advance to the Go state, the operator must close both buttons 
    ''' within the minimum Response Time seconds.
    ''' </summary>
    Public Property ResponseTimeout() As TimeSpan

    ''' <summary> Gets the is response timeout. </summary>
    ''' <value> The is response timeout. </value>
    Public ReadOnly Property IsResponseTimeout As Boolean
        Get
            Return Me.ResponseStopwatch.IsRunning AndAlso Me.ResponseStopwatch.Elapsed > Me.ResponseTimeout
        End Get
    End Property

#End Region

#Region " COMMANDs "

    Public Sub ButtonPressed()
        If Me.IsResponseTimeout Then
            Me.ProcessInput(BimanualEnablerSymbol.Timeout)
        Else
            Me.ProcessInput(BimanualEnablerSymbol.ButtonPressed)
        End If
    End Sub

    Public Sub ButtonReleased()
        Me.ProcessInput(BimanualEnablerSymbol.ButtonReleased)
    End Sub

    Public Sub EstopPressed()
        Me.ProcessInput(BimanualEnablerSymbol.Estop)
    End Sub

    Public Sub Ready()
        Me.ProcessInput(BimanualEnablerSymbol.Ready)
    End Sub

    Public Sub Origin()
        Me.ProcessInput(BimanualEnablerSymbol.Origin)
    End Sub

    Public Sub Acknowledge()
        Me.ProcessInput(BimanualEnablerSymbol.Acknowledge)
    End Sub

#End Region

#Region " PROGRESS INDICATOR "

    ''' <summary> Gets the soak progress indicator maximum. </summary>
    ''' <value> The soak progress indicator maximum. </value>
    Public Shared ReadOnly Property ProgressIndicatorMinimum As Integer
        Get
            Return ProgressIndicator(BimanualEnablerState.Failure)
        End Get
    End Property

    ''' <summary> Gets the soak progress indicator minimum. </summary>
    ''' <value> The soak progress indicator minimum. </value>
    Public Shared ReadOnly Property ProgressIndicatorMaximum As Integer
        Get
            Return ProgressIndicator(BimanualEnablerState.Go)
        End Get
    End Property

    Private Shared _ProgressIndicatorDix As Dictionary(Of BimanualEnablerState, Integer)

    ''' <summary> Soak progress indicator. </summary>
    ''' <param name="state"> The state. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function ProgressIndicator(ByVal state As BimanualEnablerState) As Integer
        Dim result As Integer
        If BimanualEnabler._ProgressIndicatorDix Is Nothing Then
            BimanualEnabler._ProgressIndicatorDix = New Dictionary(Of BimanualEnablerState, Integer) From {
                {BimanualEnablerState.None, 0},
                {BimanualEnablerState.Failure, 0},
                {BimanualEnablerState.Estop, 0},
                {BimanualEnablerState.Origin, 1},
                {BimanualEnablerState.Ready, 2},
                {BimanualEnablerState.Set, 3},
                {BimanualEnablerState.Go, 4},
                {BimanualEnablerState.Enabled, 5}
            }
            For Each value As BimanualEnablerState In [Enum].GetValues(GetType(BimanualEnablerState))
                Try
                    result = BimanualEnabler._ProgressIndicatorDix(value)
                Catch ex As System.Collections.Generic.KeyNotFoundException
                    ex.Data.Add($"Coding Error {DateTime.Now.Ticks}", $"Progress indicator for Bi-manual Enabled is missing value={value}")
                    Throw ex
                End Try
            Next
        End If
        result = BimanualEnabler._ProgressIndicatorDix(state)
        Return result
    End Function

    ''' <summary> Updates the progress indicator percent. </summary>
    Public Overrides Sub UpdateProgressIndicatorPercent()
        Dim value As Integer = CInt(100 * (ProgressIndicator(Me.CurrentState.Identity) - ProgressIndicatorMinimum) /
                                         (ProgressIndicatorMaximum - ProgressIndicatorMinimum))
        value = If(value < 0, 0, If(value > 100, 100, value))
        Me.ProgressIndicatorPercent = value
        Me.PercentProgress = Me.ProgressIndicatorPercent
    End Sub

    Public Overrides Sub UpdatePercentProgress()
        Me.PercentProgress = Me.ProgressIndicatorPercent
    End Sub

#End Region

End Class

#Region " TYPES "

''' <summary> Values that represent bimanual enabler states. </summary>
Public Enum BimanualEnablerState
    <Description("Not Defined")> None
    <Description("Origin State")> Origin
    <Description("Waiting for first button")> Ready
    <Description("One button is closed")> [Set]
    <Description("Buttons closed")> [Go]
    <Description("Enabled")> [Enabled]
    <Description("Error occurred")> Failure
    <Description("Emergency stop")> Estop
End Enum

''' <summary> Values that represent bimanual enabler symbols. </summary>
Public Enum BimanualEnablerSymbol
    <Description("Not Defined")> None
    <Description("Origin: .. -> Origin")> Origin
    <Description("Ready: Origin -> Ready")> Ready
    <Description("Button Pressed: Ready/Set -> Set/Go")> ButtonPressed
    <Description("Button Released: .. -> Ready")> ButtonReleased
    <Description("Acknowledge: Go -> Enabled")> Acknowledge
    <Description("Timeout: ... -> Ready")> Timeout
    <Description("EStope: .. -> Estop")> Estop
    <Description("Failure: Any State -> Failure")> Failure
End Enum

#End Region
