Imports System.ComponentModel

''' <summary>
''' A base automaton class for implementing a finite automata for starting, restarting, working, and stopping constructs.
''' </summary>
''' <remarks> (c) 2090 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' created
''' </para></remarks>
Public Class ReadyRestartWorkStopAutomaton
    Inherits TimeoutMachine(Of ReadyRestartWorkStopSymbol, ReadyRestartWorkStopState)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>Constructs this class.</summary>
    Public Sub New(ByVal restartTimeout As TimeSpan, ByVal startingTimeout As TimeSpan,
                   ByVal startedTimeout As TimeSpan, ByVal stoppingTimeout As TimeSpan)
        MyBase.New()
        MyBase.OnsetDelay = TimeSpan.FromMilliseconds(500)
        MyBase.Interval = TimeSpan.FromMilliseconds(1000)

        ' define the states.
        Me._readyState = MyBase.Add(ReadyRestartWorkStopState.Ready)
        Me._FailureState = MyBase.Add(ReadyRestartWorkStopState.Failure)
        AddHandler Me.FailureState.Enter, AddressOf Me.FailureState_Enter
        Me._restartState = MyBase.Add(ReadyRestartWorkStopState.Restart, restartTimeout)
        Me._startingState = MyBase.Add(ReadyRestartWorkStopState.Starting, startingTimeout)
        Me._startedState = MyBase.Add(ReadyRestartWorkStopState.Started, startedTimeout)
        Me._workingState = MyBase.Add(ReadyRestartWorkStopState.Working)
        Me._workDoneState = MyBase.Add(ReadyRestartWorkStopState.WorkDone)
        Me._stoppingState = MyBase.Add(ReadyRestartWorkStopState.Stopping, stoppingTimeout)
        Me._stoppedState = MyBase.Add(ReadyRestartWorkStopState.Stopped)

        ' define the signals.
        For Each value As ReadyRestartWorkStopSymbol In [Enum].GetValues(GetType(ReadyRestartWorkStopSymbol))
            MyBase.Add(value)
        Next

        ' define the transitions

        ' READY TRANSITION
        For Each state As IState(Of ReadyRestartWorkStopState) In MyBase.States
            If state.Equals(Me._readyState) OrElse state.Equals(Me._failureState) OrElse state.Equals(Me._stoppedState) Then
                MyBase.Add(state, ReadyRestartWorkStopSymbol.Ready, Me._readyState)
            End If
        Next

        ' FAILURE TRANSITION
        For Each state As IState(Of ReadyRestartWorkStopState) In MyBase.States
            MyBase.Add(state, ReadyRestartWorkStopSymbol.Failure, Me._failureState)
        Next

        MyBase.Add(Me._readyState, ReadyRestartWorkStopSymbol.Start, Me._startingState)
        MyBase.Add(Me._readyState, ReadyRestartWorkStopSymbol.Restart, Me._restartState)

        MyBase.Add(Me._restartState, ReadyRestartWorkStopSymbol.Start, Me._startingState)

        MyBase.Add(Me._startingState, ReadyRestartWorkStopSymbol.Finish, Me._startedState)
        MyBase.Add(Me._startingState, ReadyRestartWorkStopSymbol.Stop, Me._stoppingState)

        MyBase.Add(Me._startedState, ReadyRestartWorkStopSymbol.Start, Me._workingState)
        MyBase.Add(Me._startedState, ReadyRestartWorkStopSymbol.Stop, Me._stoppingState)

        MyBase.Add(Me._workingState, ReadyRestartWorkStopSymbol.Finish, Me._workDoneState)
        MyBase.Add(Me._workingState, ReadyRestartWorkStopSymbol.Stop, Me._stoppingState)

        MyBase.Add(Me._workDoneState, ReadyRestartWorkStopSymbol.Stop, Me._stoppingState)

        MyBase.Add(Me._stoppingState, ReadyRestartWorkStopSymbol.Finish, Me._stoppedState)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._readyState IsNot Nothing Then
                        Me._readyState = Nothing
                    End If
                    If Me._startingState IsNot Nothing Then
                        Me._startingState = Nothing
                    End If
                    If Me._startedState IsNot Nothing Then
                        Me._startedState = Nothing
                    End If
                    If Me._workingState IsNot Nothing Then
                        Me._workingState = Nothing
                    End If
                    If Me._workDoneState IsNot Nothing Then
                        Me._workDoneState = Nothing
                    End If
                    If Me._stoppingState IsNot Nothing Then
                        Me._stoppingState = Nothing
                    End If
                    If Me._stoppedState IsNot Nothing Then
                        Me._stoppedState = Nothing
                    End If
                    If Me._failureState IsNot Nothing Then
                        Me._failureState = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " STATES "

    ''' <summary>
    ''' Gets reference to the <see cref="ReadyState">Ready State</see>.
    ''' </summary>
    Public ReadOnly Property ReadyState() As IState(Of ReadyRestartWorkStopState)

    ''' <summary>
    ''' Gets reference to the <see cref="restartState">restart State</see>.
    ''' </summary>
    Public ReadOnly Property RestartState() As IState(Of ReadyRestartWorkStopState)

    ''' <summary>
    ''' Gets reference to the <see cref="StartingState">Starting State</see>.
    ''' </summary>
    Public ReadOnly Property StartingState() As IState(Of ReadyRestartWorkStopState)

    ''' <summary>
    ''' Gets reference to the <see cref="StartedState">Started State</see>.
    ''' </summary>
    Public ReadOnly Property StartedState() As IState(Of ReadyRestartWorkStopState)

    ''' <summary>
    ''' Gets reference to the <see cref="WorkingState">Working State</see>.
    ''' </summary>
    Public ReadOnly Property WorkingState() As IState(Of ReadyRestartWorkStopState)

    ''' <summary>
    ''' Gets reference to the <see cref="WorkDoneState">WorkDone State</see>.
    ''' </summary>
    Public ReadOnly Property WorkDoneState() As IState(Of ReadyRestartWorkStopState)

    ''' <summary>
    ''' Gets reference to the <see cref="StoppingState">Stopping State</see>.
    ''' </summary>
    Public ReadOnly Property StoppingState() As IState(Of ReadyRestartWorkStopState)

    ''' <summary>
    ''' Gets reference to the <see cref="StoppedState">Stopped State</see>.
    ''' </summary>
    Public ReadOnly Property StoppedState() As IState(Of ReadyRestartWorkStopState)

    ''' <summary> Gets the failure count down. </summary>
    ''' <value> The failure count down. </value>
    Private Property FailureCountDown As Integer

    ''' <summary>
    ''' Gets reference to the <see cref="FailureState">Failure State</see>.
    ''' </summary>
    Public ReadOnly Property FailureState() As IState(Of ReadyRestartWorkStopState)

#End Region

#Region " OVERRIDES "

    ''' <summary> Enumerates symbol name in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process symbol name in this collection.
    ''' </returns>
    Public Overrides Function InputSymbolNames() As IEnumerable(Of String)
        Return [Enum].GetNames(GetType(ReadyRestartWorkStopSymbol))
    End Function

    ''' <summary> Gets the Fail input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property FailInputSymbol As ReadyRestartWorkStopSymbol
        Get
            Return ReadyRestartWorkStopSymbol.Failure
        End Get
    End Property

    ''' <summary> Gets the Initial input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property InitialInputSymbol As ReadyRestartWorkStopSymbol
        Get
            Return ReadyRestartWorkStopSymbol.Start
        End Get
    End Property

    ''' <summary> Gets the terminal input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public Overrides ReadOnly Property TerminalInputSymbol As ReadyRestartWorkStopSymbol
        Get
            Return ReadyRestartWorkStopSymbol.Stop
        End Get
    End Property

    Public Overrides ReadOnly Property InitialStateIdentity As ReadyRestartWorkStopState
        Get
            Return ReadyRestartWorkStopState.Starting
        End Get
    End Property

    Public Overrides ReadOnly Property NullStateIdentity As ReadyRestartWorkStopState
        Get
            Return ReadyRestartWorkStopState.None
        End Get
    End Property

    Public Overrides ReadOnly Property TerminalStateIdentity As ReadyRestartWorkStopState
        Get
            Return ReadyRestartWorkStopState.WorkDone
        End Get
    End Property

#End Region

#Region " ACTIVATION "

    Public Overrides Sub EnqueueStopSymbol()
        Me.EnqueueInput(ReadyRestartWorkStopSymbol.Stop, True)
    End Sub

    Protected Overrides Sub StartSequence()
        Me.FailureCountDown = 3
        MyBase.StartSequence()
    End Sub

    Private Sub FailureState_Enter(sender As Object, e As EventArgs)
        If Me.FailureCountDown > 0 Then Me.FailureCountDown -= 1
        If Me.FailureCountDown <= 0 Then Me.FailureState.OnFailed()
    End Sub

#End Region

#Region " PROGRESS INDICATOR "

    ''' <summary> Gets the Ready-Restart-Work-Stop progress indicator maximum. </summary>
    ''' <value> The Ready-Restart-Work-Stop progress indicator maximum. </value>
    Public Shared ReadOnly Property ProgressIndicatorMinimum As Integer
        Get
            Return ProgressIndicator(ReadyRestartWorkStopState.None)
        End Get
    End Property

    ''' <summary> Gets the Ready-Restart-Work-Stop progress indicator minimum. </summary>
    ''' <value> The Ready-Restart-Work-Stop progress indicator minimum. </value>
    Public Shared ReadOnly Property ProgressIndicatorMaximum As Integer
        Get
            Return ProgressIndicator(ReadyRestartWorkStopState.WorkDone)
        End Get
    End Property

    Private Shared _ProgressIndicatorDix As Dictionary(Of ReadyRestartWorkStopState, Integer)

    ''' <summary> Ready-Restart-Work-Stop progress indicator. </summary>
    ''' <param name="state"> The state. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function ProgressIndicator(ByVal state As ReadyRestartWorkStopState) As Integer
        Dim result As Integer
        If ReadyRestartWorkStopAutomaton._ProgressIndicatorDix Is Nothing Then
            ReadyRestartWorkStopAutomaton._ProgressIndicatorDix = New Dictionary(Of ReadyRestartWorkStopState, Integer) From {
                {ReadyRestartWorkStopState.None, 0},
                {ReadyRestartWorkStopState.Failure, 0},
                {ReadyRestartWorkStopState.Stopped, 0},
                {ReadyRestartWorkStopState.Stopping, 0},
                {ReadyRestartWorkStopState.Ready, 1},
                {ReadyRestartWorkStopState.Restart, 2},
                {ReadyRestartWorkStopState.Starting, 3},
                {ReadyRestartWorkStopState.Started, 4},
                {ReadyRestartWorkStopState.Working, 5},
                {ReadyRestartWorkStopState.WorkDone, 6}
            }
            For Each value As ReadyRestartWorkStopState In [Enum].GetValues(GetType(ReadyRestartWorkStopState))
                Try
                    result = ReadyRestartWorkStopAutomaton._ProgressIndicatorDix(value)
                Catch ex As System.Collections.Generic.KeyNotFoundException
                    ex.Data.Add($"Coding Error {DateTime.Now.Ticks}", $"Progress indicator for Ready-Restart-Work-Stop Automaton is missing value={value}")
                    Throw ex
                End Try
            Next
        End If
        result = ReadyRestartWorkStopAutomaton._ProgressIndicatorDix(state)
        Return result
    End Function

    ''' <summary> Updates the progress indicator percent. </summary>
    Public Overrides Sub UpdateProgressIndicatorPercent()
        Dim value As Integer = CInt(100 * (ProgressIndicator(Me.CurrentState.Identity) - ProgressIndicatorMinimum) /
                                         (ProgressIndicatorMaximum - ProgressIndicatorMinimum))
        value = If(value < 0, 0, If(value > 100, 100, value))
        Me.ProgressIndicatorPercent = value
        Me.PercentProgress = Me.ProgressIndicatorPercent
    End Sub

    Public Overrides Sub UpdatePercentProgress()
        Me.PercentProgress = Me.ProgressIndicatorPercent
    End Sub

#End Region

End Class

''' <summary>
''' Enumerates the state transition symbols for the 
''' <see cref="ReadyRestartWorkStopAutomaton">Start</see> automaton.
''' </summary>
Public Enum ReadyRestartWorkStopSymbol
    <Description("Not Defined")> None
    <Description("Restart: Ready to Restart")> Restart
    <Description("Start: Ready, Restart -> Start; Started -> Working; Done Work -> Stopping")> Start
    <Description("Finish: Starting -> Started; Working -> Done Work; Stopping -> Stopped")> Finish
    <Description("Stop: Ready, Restart -> Stopped; Starting, Started -> Stopping")> [Stop]
    <Description("Failure: Any State -> Failure")> Failure
    <Description("Ready: .. -> Ready")> Ready
End Enum

''' <summary> Values that represent ready restart work stop states. </summary>
Public Enum ReadyRestartWorkStopState
    <Description("Not Defined")> None
    <Description("Ready")> Ready
    <Description("Restart")> Restart
    <Description("Starting")> Starting
    <Description("Started")> Started
    <Description("Working")> Working
    <Description("Work Done")> WorkDone
    <Description("Stopping")> Stopping
    <Description("Stopped")> Stopped
    <Description("Failure")> Failure
End Enum

