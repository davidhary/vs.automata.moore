## ISR Automata Finite
### To Do 

*2016-08-30*  
See if new state events (entering etc.) allows to reduce the
number of states. This may allow connecting with only 4 states: Idle,
Disconnected, Connected, Failure.

*2016-04-16*  
Check using timeout tasks in place of the time states. If
works, remove the Build from Moore automata.
