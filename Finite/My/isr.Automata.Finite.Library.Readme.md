## ISR Automata Finite<sub>&trade;</sub>: Finite State Machine Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*5.2.6624 2018-02-19*  
Fixes adding dispatched exception to the blocking
collection.

*5.2.6319 2017-04-20*  
Move Dashboards to the Dashboard library and machines
to the Machines library.

*5.1.6101 2016-09-14*  
Adds dashboard. Splits state interface into two for
using with the Dashboard.

*5.0.6086 2016-08-30*  
Replaces the Act event with the Reenter event. Changes
interval and onset delay to time span. Updates the interface to include
task and thread start.

*4.0.5956 2016-04-22*  
Removes the Moore name space.

*4.0.5955 2016-04-21*  
Added transition events for the state.

*4.0.5950 2016-04-16*  
Built from Moore automata.

\(C\) 2006 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Automata Libraries](https://bitbucket.org/davidhary/vs.automata.moore)  
[Moore State Machine in C\#](http://www.codeproject.com/KB/recipes/MooreMachine.aspx)
