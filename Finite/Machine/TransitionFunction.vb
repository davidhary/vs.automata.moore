''' <summary>
''' Defines the state transition function or table.
''' </summary>
''' <remarks> David, 2009-04-26, 2.3.3403.x
''' Based on Moore Engine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreEngine.aspx <para>
''' The transition tables defines how a state machine transits between states. </para><para>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved.  </para><para>
''' Licensed under The MIT License. </para></remarks>
<ComponentModel.Description("Finite State Machine Transition Function")>
Public Class TransitionFunction(Of TSymbol, TState)
    Implements ITransitionFunction(Of TSymbol, TState)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>Constructs this class.</summary>
    Public Sub New()
        Me._transitions = New LinkedList(Of ITransition(Of TSymbol, TState))()
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._transitions IsNot Nothing Then
                        For Each t As Transition(Of TSymbol, TState) In Me._Transitions
                            If t IsNot Nothing Then t.Dispose()
                        Next
                        Me._transitions.Clear()
                        Me._transitions = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " TRANSITION TABLE CONSTRUCTION AND ANALYSIS "

    Private _AnalysisReport As System.Text.StringBuilder
    ''' <summary>
    ''' Returns the analysis report.
    ''' </summary>
    Public ReadOnly Property AnalysisReport() As String Implements ITransitionFunction(Of TSymbol, TState).AnalysisReport
        Get
            Return If(Me._analysisReport Is Nothing, "Analysis not run", Me._analysisReport.ToString)
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the state machine is deterministic, i.e., if each state and symbol have a unique
    ''' next state. Namely, if all transitions are unique.
    ''' </summary>
    Public ReadOnly Property IsDeterministic() As Boolean Implements ITransitionFunction(Of TSymbol, TState).IsDeterministic

    ''' <summary>
    ''' Gets or sets the linked-list of transitions.
    ''' </summary>
    Private _Transitions As LinkedList(Of ITransition(Of TSymbol, TState))

    ''' <summary>
    ''' Adds a transition to the transition table.
    ''' </summary>
    ''' <param name="state">Specifies the current state.</param>
    ''' <param name="symbol">Specifies the input symbol that signals the transition.</param>
    ''' <param name="nextState">Specifies the next state.</param>
    Public Function Add(ByVal state As IState(Of TState), ByVal symbol As TSymbol, ByVal nextState As IState(Of TState)) As ITransition(Of TSymbol, TState) Implements ITransitionFunction(Of TSymbol, TState).Add

        Me._analysisReport = New System.Text.StringBuilder
        Me._AnalysisReport.AppendLine($"Deterministic analysis report {DateTimeOffset.Now}")

        Me._IsDeterministic = True
        Dim candidateTransition As ITransition(Of TSymbol, TState)
        candidateTransition = New Transition(Of TSymbol, TState)(state, symbol, nextState)

        If Me._transitions.Count > 1 Then

            Dim node As LinkedListNode(Of ITransition(Of TSymbol, TState)) = Me._transitions.First
            Do While node IsNot Nothing

                If node.Value.Equals(candidateTransition) Then
                    Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                        "New Transition from State '{1}' with Symbol '{3}' to State '{2}' conflicts with transition {0}", Environment.NewLine,
                                                        candidateTransition.State.Identity, candidateTransition.Symbol.ToString, candidateTransition.NextState.Identity)
                    Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                        "               from State '{1}' with Symbol '{2}' to State '{3}'{0}", Environment.NewLine,
                                                        node.Value.State.Identity, node.Value.Symbol.ToString, node.Value.NextState.Identity)
                    Me._IsDeterministic = False
                End If
                node = node.Next
            Loop
        End If

        If Me._IsDeterministic Then
            Me._analysisReport.Append("Transition function is deterministic.")
        End If

        Me._transitions.AddLast(candidateTransition)

        Return candidateTransition

    End Function

#End Region

#Region " INPUT SYMBOL PROCESSING "

    ''' <summary>
    ''' Traverses the transition function and transition specified for the current state and the 
    ''' input symbol.
    ''' </summary>
    ''' <param name="state">Specifies the current state.</param>
    ''' <param name="symbol">Specifies the input symbol that signals the transition.</param>
    Friend Function SelectTransition(ByVal state As IState(Of TState), ByVal symbol As TSymbol) As ITransition(Of TSymbol, TState) Implements ITransitionFunction(Of TSymbol, TState).SelectTransition

        Dim node As LinkedListNode(Of ITransition(Of TSymbol, TState)) = Me._transitions.First

        Do While node IsNot Nothing

            Dim t As ITransition(Of TSymbol, TState) = node.Value

            If t.State.Equals(state) AndAlso t.Symbol.Equals(symbol) Then

                Return t

            End If

            node = node.Next

        Loop

        Return New Transition(Of TSymbol, TState)(state, symbol, Nothing)

    End Function


#End Region

End Class
