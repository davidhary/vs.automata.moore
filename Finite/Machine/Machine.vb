Imports System.Threading
Imports System.ComponentModel
Imports isr.Core
Imports isr.Automata.Finite.ExceptionExtensions
''' <summary>
''' A base automaton class for handling finite automata sequencing.
''' </summary>
''' <remarks> David, 2009-04-26, 2.3.3403.x
''' Based on Moore Engine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreEngine.aspx <para>
''' An automaton is a mathematical model for a finite state Engine. A finite state Engine is a Engine that,
''' given an input of symbols, "jumps", or transitions, through a series of states according to a transition 
''' function (which can be expressed as a table). In the common "Mealy" variety of final state Engines, this transition function 
''' tells the automaton which state to go to next given a current state and a current input symbol. </para><para>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para></remarks>
<Description("Finite State Machine"), DefaultEvent("SymbolAcknowledged"), DefaultProperty("Name")>
Public MustInherit Class Machine(Of TSymbol, TState)
    Inherits Engine(Of TSymbol, TState)
    Implements IMachine(Of TSymbol, TState)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>Constructs this class.</summary>
    Protected Sub New(ByVal name As String)
        MyBase.New(name)
        Me._Interval = TimeSpan.FromMilliseconds(1000)
        Me._SymbolQueue = New Queue(Of TSymbol)
        Me.SymbolQueueLocker = New Object
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="_startState", Justification:="The states are disposed as art of the collection")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="_currentState", Justification:="The states are disposed as art of the collection")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="_previousState", Justification:="The states are disposed as art of the collection")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveStoppedEventHandlers()
                If Me.IsRunning Then Me.RequestCancellation()
                If Me.CancellationTokenSource IsNot Nothing Then
                    Me.CancellationTokenSource.Dispose()
                    Me.CancellationTokenSource = Nothing
                End If
                If Me.PauseTokenSource IsNot Nothing Then
                    Me.PauseTokenSource.Dispose()
                    Me.PauseTokenSource = Nothing
                End If
                If Me.StopTokenSource IsNot Nothing Then
                    Me.StopTokenSource.Dispose()
                    Me.StopTokenSource = Nothing
                End If
                If Me.ManualResetEvent IsNot Nothing Then
                    Me.ManualResetEvent.Dispose()
                    Me.ManualResetEvent = Nothing
                End If
                If Me._MachineThread IsNot Nothing Then
                    Me._MachineThread = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " SYMBOL MANAGEMENT "

    Private ReadOnly Property SymbolQueueLocker As Object

    ''' <summary>
    ''' Clears the symbol queue. This is required in machines where both internal and external elements
    ''' might feed inputs.
    ''' </summary>
    Protected Sub ClearSymbolQueue() Implements IMachine.ClearSymbolQueue
        Me.SymbolQueue.Clear()
        Me.AsyncNotifyPropertyChanged(NameOf(IMachine.HasInput))
    End Sub

    ''' <summary>
    ''' Gets reference to the input symbol queue.
    ''' </summary>
    Private ReadOnly Property SymbolQueue() As Queue(Of TSymbol)

    ''' <summary>
    ''' Retrieves the input.
    ''' </summary>
    Protected Function DequeueInput() As TSymbol
        SyncLock Me.SymbolQueueLocker
            Return Me.SymbolQueue.Dequeue
        End SyncLock
    End Function

    ''' <summary>
    ''' Enqueues an input symbol.
    ''' </summary>
    ''' <param name="value">The input symbol to add to the input queue.</param>
    Public Sub EnqueueInput(ByVal value As TSymbol) Implements IMachine(Of TSymbol, TState).EnqueueInput
        SyncLock Me.SymbolQueueLocker
            Me.SymbolQueue.Enqueue(value)
        End SyncLock
        Me.AsyncNotifyPropertyChanged(NameOf(IMachine.HasInput))
    End Sub

    ''' <summary> Enqueues an input symbol. </summary>
    ''' <param name="value">      The input symbol to add to the input queue. </param>
    ''' <param name="clearFirst"> Clears the queue before enqueuing the value. </param>
    Public Sub EnqueueInput(ByVal value As TSymbol, ByVal clearFirst As Boolean) Implements IMachine(Of TSymbol, TState).EnqueueInput
        SyncLock Me.SymbolQueueLocker
            Me.SymbolQueue.Clear()
            Me.SymbolQueue.Enqueue(value)
        End SyncLock
        Me.AsyncNotifyPropertyChanged(NameOf(IMachine.HasInput))
    End Sub

    ''' <summary>
    ''' Enqueues an input symbol.
    ''' </summary>
    ''' <param name="value">The input symbol to add to the input queue.</param>
    ''' <param name="clearFirst">Clears the queue before enqueuing the value</param>
    ''' <param name="ifEmpty">If true, the signal is queued only if the queue is empty.</param>
    Public Sub EnqueueInput(ByVal value As TSymbol, ByVal clearFirst As Boolean, ByVal ifEmpty As Boolean) Implements IMachine(Of TSymbol, TState).EnqueueInput
        SyncLock Me.SymbolQueueLocker
            If clearFirst AndAlso Me.SymbolQueue.Any Then Me.SymbolQueue.Clear()
            If Not (ifEmpty AndAlso Me.SymbolQueue.Any) Then Me.SymbolQueue.Enqueue(value)
        End SyncLock
        Me.AsyncNotifyPropertyChanged(NameOf(IMachine.HasInput))
    End Sub

    ''' <summary>
    ''' Enqueues an input only if the queue is empty. This allows a state to request a re-entry.
    ''' The queue is locked while it is enqueued.
    ''' </summary>
    ''' <param name="values">The input symbols to add to the input queue.</param>
    Public Sub EnqueueInput(ByVal values As TSymbol()) Implements IMachine(Of TSymbol, TState).EnqueueInput
        If values IsNot Nothing AndAlso values.Length > 0 Then
            SyncLock Me.SymbolQueueLocker
                For Each value As TSymbol In values
                    Me.SymbolQueue.Enqueue(value)
                Next
            End SyncLock
            Me.AsyncNotifyPropertyChanged(NameOf(IMachine.HasInput))
        End If
    End Sub

    ''' <summary>
    ''' Enqueues an input only if the queue is empty. This allows a state to request a re-entry.
    ''' The queue is locked while it is enqueued.
    ''' </summary>
    ''' <param name="values">The input symbols to add to the input queue.</param>
    ''' <param name="clearFirst">Clears the queue before enqueuing the value</param>
    ''' <param name="ifEmpty">If true, the signal is queued only if the queue is empty.</param>
    Public Sub EnqueueInput(ByVal values As TSymbol(), ByVal clearFirst As Boolean, ByVal ifEmpty As Boolean) Implements IMachine(Of TSymbol, TState).EnqueueInput
        If values IsNot Nothing AndAlso values.Length > 0 Then
            SyncLock Me.SymbolQueueLocker
                If clearFirst AndAlso Me.SymbolQueue.Any Then Me.SymbolQueue.Clear()
                If Not (ifEmpty AndAlso Me.SymbolQueue.Any) Then
                    For Each value As TSymbol In values
                        Me.SymbolQueue.Enqueue(value)
                    Next
                End If
            End SyncLock
            Me.AsyncNotifyPropertyChanged(NameOf(IMachine.HasInput))
        End If
    End Sub

    ''' <summary>
    ''' Returns true if the state machine received an input.
    ''' </summary>
    Public Function HasInput() As Boolean Implements IMachine.HasInput
        SyncLock Me.SymbolQueueLocker
            Return Me.SymbolQueue.Any
        End SyncLock
    End Function

    ''' <summary>
    ''' Gets the input without removing it from the queue.
    ''' </summary>
    Public Function PeekInput() As TSymbol Implements IMachine(Of TSymbol, TState).PeekInput
        SyncLock Me.SymbolQueueLocker
            Return If(Me.SymbolQueue.Any, Me.SymbolQueue.Peek, Nothing)
        End SyncLock
    End Function

    ''' <summary> Gets the name of the next input symbol. </summary>
    ''' <value> The name of the next input symbol. </value>
    Public ReadOnly Property NextSymbolName As String Implements IMachine.NextSymbolName
        Get
            Return Me.PeekInput?.ToString
        End Get
    End Property


#End Region

#Region " INITIATE / TERMINATE "

    Private Sub NotifyStateChange1()
        ' !@# 20190130: this may need to be restored. Me.NotifyPropertyChanging(NameOf(IMachine.CurrentState))
        'Me.SyncNotifyPropertyChanging(NameOf(IMachine.CurrentState))
        Me.CurrentState.OnEnter(System.EventArgs.Empty)
        Me.UpdateProgressIndicatorPercent()
    End Sub

    ''' <summary>
    ''' Moves the state machine to the Start State. 
    ''' This action clears the input QUEUE. It leaves the machine stopped with the 
    ''' <see cref="IsStopRequested">stop requested sentinel</see> turned on, which is required for the 
    ''' machine to resume its actions.
    ''' </summary>
    ''' <remarks> 
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    Public Overrides Sub Initiate(ByVal onsetDelay As TimeSpan)

        SyncLock Me.ExecuteLocker

            ' clear and build signals if not set in case of using non-threading initiation.
            If Me.ManualResetEvent Is Nothing Then Me.ClearSignals()

            ' set the current state.
            Me.CurrentState = Me.StartState

            ' clear the input queue
            Me.ClearSymbolQueue()

            ' wait for the onset delay
            If onsetDelay > TimeSpan.Zero Then Me.ManualResetEvent.WaitOne(onsetDelay)

            If Me.IsStopRequested OrElse Me.IsCancellationRequested Then

                ' this prevents starting the state operations
                Me.CurrentState = Nothing

            Else

                ' start the ball rolling.
                Me.OnInitiated(System.EventArgs.Empty)

            End If

        End SyncLock

        If Me.IsStopRequested OrElse Me.IsCancellationRequested Then
            ' this prevents starting the state operations
            Me.CurrentState = Nothing
        Else
            ' this starts the state operations.
            Me.NotifyStateChange1()
        End If

    End Sub

    ''' <summary> Sends this automaton to its terminal state. </summary>
    ''' <returns>
    ''' <c>true</c> if the termination action was initiated; otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Terminate() As Boolean
        Dim result As Boolean = Me.IsTerminable AndAlso Me.IsRunning
        If result Then
            Me.EnqueueInput(Me.TerminalInputSymbol)
        End If
        Return result
    End Function

#End Region

#Region " PROCESS INPUT CORE "

    ''' <summary>
    ''' Immediately processes a single input ignoring the input queue. This allows running the state
    ''' Engine based on signals alone.
    ''' </summary>
    ''' <remarks> David, 2009-04-26, 2.3.3403.x. Allows makings a transition into the same state in which case the
    ''' <see cref="IState.Leave">Leave</see> and <see cref="IState.Enter">Enter</see> events
    ''' do not occur. Use the <see cref="IState.Reenter">Reenter</see> event to process state actions. <para>
    ''' Exceptions must be trapped in the calling routing to catch exceptions thrown by the state and
    ''' transition event handlers. </para> </remarks>
    ''' <param name="symbol"> Specifies the input to process. </param>
    ''' <param name="e">      Cancel details event information. </param>
    Protected Overrides Sub ProcessInputCore(ByVal symbol As TSymbol, ByVal e As Core.ActionEventArgs)

        If e Is Nothing Then
            Return
        ElseIf Me.CurrentState Is Nothing Then
            e.RegisterFailure($"{Me.Name} current state is nothing.")
            Return
        End If

        ' notify of elapsed time change
        Me.Elapsed = Me.Stopwatch.Elapsed
        ' Me.AsyncNotifyPropertyChanged(NameOf(IMachine.Stopwatch))

        Dim args As New ComponentModel.CancelEventArgs

        ' clear the failure state
        Me.FailureStatus = FailureStatus.None

        ' notify of input
        Me.LastInputSymbol = symbol

        Me.OnSymbolAcknowledged(New SymbolEventArgs(Of TSymbol)(symbol))

        Dim newTranstion As ITransition(Of TSymbol, TState) = Me.TransitionTable.SelectTransition(Me.CurrentState, symbol)

        If newTranstion.NextState Is Nothing Then

            Me.CurrentState = Me.CurrentState
            e.RegisterFailure($"{Me.Name} transition from '{Me.CurrentState.Identity}' with input '{symbol}' not found")

        Else

            If Me.CancellationToken.IsCancellationRequested Then e.RegisterFailure("Cancellation token requested")

            newTranstion.OnStarting(args)

            If Not e.Failed Then

                If Me.CurrentState.Equals(newTranstion.NextState) Then

                    ' reentering
                    Me.CurrentState.OnReentering(args)

                    If Not e.Failed Then Me.CurrentState.OnReenter(System.EventArgs.Empty)

                Else

                    Me.CurrentState.OnLeaving(args)

                    If Not e.Failed Then

                        Me.CurrentState.OnLeave(args)

                        Dim previousPreviousState As IState(Of TState) = Me.PreviousState

                        Me.PreviousState = Me.States.Item(Me.CurrentState.Identity)

                        Me.CurrentState = newTranstion.NextState

                        Me.CurrentState.OnEntering(args)

                        If e.Failed Then

                            Me.CurrentState = Me.PreviousState

                            Me.PreviousState = previousPreviousState

                        Else
                            Me.NotifyStateChange1()

                        End If

                    End If

                End If

                newTranstion.OnCompleted(args)

            End If

        End If

    End Sub

#End Region

#Region " MACHINE(Of TSymbol, TState) PROCERSS INPUT QUEUE "

    ''' <summary>
    ''' Process a single input if any.  This allows running the state machine 
    ''' based on signals alone.
    ''' Catches exceptions and reports failures if exception occurred.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overloads Function TryProcessInput() As Boolean Implements IMachine.TryProcessInput
        Dim errorArgs As IO.ErrorEventArgs = Nothing
        Dim e As New ActionEventArgs
        If Me.HasInput() Then
            Dim input As TSymbol = Me.DequeueInput
            Try
                Try
                    Me.ProcessInputCore(input, e)
                    If e.Failed Then
                        ' notify if failed
                        errorArgs = New IO.ErrorEventArgs(New SequenceFailedException(e.Details))
                    End If
                Catch te As System.Reflection.TargetInvocationException
                    If te.InnerException Is Nothing Then Throw
                    Dim innerException As Exception = te.InnerException
                    Dim saveStack As ThreadStart = TryCast(System.Delegate.CreateDelegate(GetType(ThreadStart), innerException,
                                                                                          "InternalPreserveStackTrace", False, False), ThreadStart)
                    If saveStack IsNot Nothing Then saveStack()
                    Throw innerException ' -- now we can re-throw without trashing the stack
                Catch ex As Exception
                    Throw
                End Try
            Catch ex As Exception
                e.RegisterError($"{Me.Name} exception processing {input} at the {Me.CurrentStateName} after {Me.PreviousStateName} state;. {ex.ToFullBlownString}")
                errorArgs = New IO.ErrorEventArgs(New SequenceFailedException(e.Details, ex))
            Finally
                Me.AsyncNotifyPropertyChanged(NameOf(IMachine.HasInput))
            End Try
        ElseIf Me.AutoReenterEnabled Then
            Try
                Try
                    ' notify of elapsed time change
                    Me.Elapsed = Me.Stopwatch.Elapsed
                    Dim cancelEventArgs As New System.ComponentModel.CancelEventArgs
                    Me.CurrentState.OnReentering(cancelEventArgs)
                    If Not cancelEventArgs.Cancel Then Me.CurrentState.OnReenter(System.EventArgs.Empty)
                Catch te As System.Reflection.TargetInvocationException
                    If te.InnerException Is Nothing Then Throw
                    Dim innerException As Exception = te.InnerException
                    Dim saveStack As ThreadStart = TryCast(System.Delegate.CreateDelegate(GetType(ThreadStart), innerException,
                                                                                          "InternalPreserveStackTrace", False, False), ThreadStart)
                    If saveStack IsNot Nothing Then saveStack()
                    Throw innerException ' -- now we can re-throw without trashing the stack
                Catch ex As Exception
                    Throw
                End Try
            Catch ex As Exception
                e.RegisterError($"{Me.Name} exception auto entering at the {Me.CurrentStateName} state;. {ex.ToFullBlownString}")
                errorArgs = New IO.ErrorEventArgs(New SequenceFailedException(e.Details, ex))
            End Try
        End If
        ' notify if failed
        If errorArgs IsNot Nothing Then Me.OnFailed(errorArgs)
        Return errorArgs Is Nothing
    End Function

#End Region

#Region " SEQUENCE "

#Region " EVENTS: STOPPED "

    ''' <summary>Raises the Stopped event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnStopped(ByVal e As System.EventArgs) Implements IMachine.OnStopped
        Me.Stopwatch.Stop()
        Me._StoppedEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Removes the  <see cref="Stopped"/> event handlers. </summary>
    Protected Sub RemoveStoppedEventHandlers()
        Me._StoppedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The  <see cref="Stopped"/> event handlers. </summary>
    Private ReadOnly _StoppedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in  <see cref="Stopped"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Stopped As EventHandler(Of System.EventArgs) Implements IMachine.Stopped
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._StoppedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._StoppedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._StoppedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

#End Region

    ''' <summary>
    ''' Starts the state machine by
    ''' <see cref="M:isr.autoamta.Finite.IMachine`2.Initiate(System.TimeSpan)">
    ''' initiating</see>
    ''' the start state and
    ''' <see cref="M:isr.Automata.IMachine`2.Sequence">
    ''' repeating the sequence</see>
    ''' until receiving a
    ''' <see cref="M:isr.Automata.IMachine`2.RequestStop">
    ''' stop request.</see>
    ''' </summary>
    Protected Overridable Sub StartSequence() Implements IMachine.StartSequence
        Me.Initiate(Me.OnsetDelay)
        Me.Sequence()
    End Sub

    ''' <summary>
    ''' Starts the state machine by <see cref="Initiate">initiating</see> the start state and
    ''' <see cref="Sequence">repeating the sequence</see> until receiving a
    ''' <see cref="RequestStop">stop request.</see>
    ''' </summary>
    ''' <param name="onsetDelay"> Gets or sets the time to wait before starting the machine cycle. </param>
    ''' <param name="interval">   Specifies the state machine period. </param>
    Public Overridable Sub StartSequence(ByVal onsetDelay As TimeSpan, ByVal interval As TimeSpan) Implements IMachine.StartSequence
        Me.OnsetDelay = onsetDelay
        Me.Interval = interval
        Me.StartSequence()
    End Sub

    ''' <summary>
    ''' Specifies the state machine period.
    ''' </summary>
    Public Overridable Property Interval() As TimeSpan Implements IMachine.Interval

    ''' <summary> Updates the interval described by interval. </summary>
    ''' <param name="interval"> Specifies the state machine period. </param>
    Public Sub UpdateInterval(ByVal interval As TimeSpan)
        If Me.Interval < interval Then
            Me.Interval = interval
        ElseIf Me.Interval > interval.Add(interval) Then
            If Me.Stopwatch.Elapsed.Subtract(Me.PreviousElapsed) > interval Then
                ' if the remaining time is longer than the new interval, stop the current wait first
                Me.ManualResetEvent.Set()
            End If
            Me.Interval = interval
        End If
    End Sub

    ''' <summary>
    ''' Runs the state machine sequence until receiving <see cref="RequestStop">stop request.</see>
    ''' The sequence will run only after a previous stop requested that leaves 
    ''' <see cref="IsStopRequested">stop requested sentinel</see> turned on.
    ''' </summary>
    Protected Overridable Sub Sequence() Implements IMachine.Sequence

        ' this exits if the initiation was canceled. 
        If Me.CurrentState Is Nothing Then Return
        SyncLock Me.ExecuteLocker

            Do Until Me.IsStopRequested OrElse Me.IsCancellationRequested

                ' process a single input
                If Not Me.IsPauseRequested Then Me.TryProcessInput()

                ' wait till completed or signaled.
                Me.ManualResetEvent.WaitOne(Me.Interval)

            Loop

            Me.OnStopped(System.EventArgs.Empty)

        End SyncLock

    End Sub

    ''' <summary> Resumes operations after a stop. </summary>
    Protected Overridable Sub ResumeSequence() Implements IMachine.ResumeSequence

        SyncLock Me.ExecuteLocker

            ' clear and build signals if not set in case of using non-threading initiation.
            If Me.ManualResetEvent Is Nothing Then Me.ClearSignals()

        End SyncLock

        If Me.IsStopRequested OrElse Me.IsCancellationRequested Then
            ' this prevents starting the state operations
            Me.CurrentState = Nothing
        Else
            ' this starts the state operations.
            Me.NotifyStateChange1()
        End If

        Me.Sequence()

    End Sub

    ''' <summary> The running locker to halt checking on the running status if the machine is set to start. </summary>
    Private ReadOnly Property RunningLocker As New Object

    ''' <summary> Gets the sentinel indicating if the machine is running in the background. </summary>
    ''' <value> <c>true</c> if running; otherwise <c>false</c> </value>
    Public ReadOnly Property IsRunning As Boolean Implements IMachine.IsRunning
        Get
            SyncLock Me.RunningLocker
                If Me._Task IsNot Nothing Then
                    Return Me._Task.Status = TaskStatus.Running
                ElseIf Me.MachineThread IsNot Nothing Then
                    Return Me.MachineThread.IsAlive
                Else
                    Return False
                End If
            End SyncLock
        End Get
    End Property

#End Region

#Region " SIGNALS "

    ''' <summary> Gets or sets the manual reset event. </summary>
    ''' <value> The manual reset event. </value>
    Private Property ManualResetEvent As ManualResetEvent

    ''' <summary> Resets the interval described by interval. </summary>
    ''' <param name="interval"> Specifies the state machine period. </param>
    Public Sub ResetInterval(ByVal interval As TimeSpan)
        ' stops the wait.
        If Me.ManualResetEvent IsNot Nothing Then Me.ManualResetEvent.Set()
        Me.Interval = interval
    End Sub

    ''' <summary> The Pause token source. </summary>
    Private Property PauseTokenSource As CancellationTokenSource

    ''' <summary> The Pause token. </summary>
    Private Property PauseToken As CancellationToken

    ''' <summary> Gets the condition indicating that a Pause was requested. </summary>
    ''' <value> <c>true</c> if Pause requested; otherwise <c>false</c> </value>
    Public ReadOnly Property IsPauseRequested() As Boolean Implements IMachine.IsPauseRequested
        Get
            Return Me.PauseToken.IsCancellationRequested
        End Get
    End Property

    ''' <summary>
    ''' Requests the state machine to Pause running after completion of the last action.
    ''' </summary>
    Public Overridable Sub RequestPause() Implements IMachine.RequestPause
        If Not Me.IsPauseRequested Then
            ' Pauses the wait.
            Me.PauseTokenSource.Cancel()
            Me.AsyncNotifyPropertyChanged(NameOf(IMachine.IsPauseRequested))
        End If
    End Sub

    ''' <summary>
    ''' Requests the state machine to resume after a Pause.
    ''' </summary>
    Public Overridable Sub ResumePause() Implements IMachine.ResumePause
        If Me.IsPauseRequested Then
            If Me._PauseTokenSource IsNot Nothing Then
                Me._PauseTokenSource.Dispose()
                Me._PauseTokenSource = Nothing
            End If
            Me.PauseTokenSource = New CancellationTokenSource
            Me.PauseToken = Me._PauseTokenSource.Token
            Me.AsyncNotifyPropertyChanged(NameOf(IMachine.IsPauseRequested))
        End If
    End Sub


    ''' <summary> The stop token source. </summary>
    Private Property StopTokenSource As CancellationTokenSource

    ''' <summary> The stop token. </summary>
    Private Property StopToken As CancellationToken

    ''' <summary> Gets the condition indicating that a stop was requested. </summary>
    ''' <value> <c>true</c> if stop requested; otherwise <c>false</c> </value>
    Public ReadOnly Property IsStopRequested() As Boolean Implements IMachine.IsStopRequested
        Get
            Return Me.StopToken.IsCancellationRequested
        End Get
    End Property

    ''' <summary>
    ''' Requests the state machine to stop running after completion of the last action.
    ''' </summary>
    Public Overridable Sub RequestStop() Implements IMachine.RequestStop
        If Not Me.IsStopRequested Then
            ' stops the wait.
            Me.ManualResetEvent.Set()
            Me.StopTokenSource.Cancel()
            Me.AsyncNotifyPropertyChanged(NameOf(IMachine.IsStopRequested))
        End If
    End Sub


    ''' <summary> The cancellation token source. </summary>
    Private Property CancellationTokenSource As CancellationTokenSource

    ''' <summary> The cancellation token. </summary>
    Private Property CancellationToken As CancellationToken

    ''' <summary>
    ''' Requests the state machine to immediately stop operation.
    ''' </summary>
    Public Overridable Sub RequestCancellation() Implements IMachine.RequestCancellation
        If Not Me.IsCancellationRequested Then
            ' stops the wait.
            Me.ManualResetEvent.Set()
            If Me.MachineThread IsNot Nothing Then Me.MachineThread.Abort()
            Me.CancellationTokenSource.Cancel()
            Me.AsyncNotifyPropertyChanged(NameOf(IMachine.IsCancellationRequested))
        End If
    End Sub

    ''' <summary> Query if cancellation requested. </summary>
    ''' <value> <c>true</c> if cancellation requested; otherwise <c>false</c> </value>
    Public ReadOnly Property IsCancellationRequested() As Boolean Implements IMachine.IsCancellationRequested
        Get
            Return If(Me.MachineThread IsNot Nothing,
                Me.CancellationToken.IsCancellationRequested OrElse Me.MachineThread.ThreadState = ThreadState.AbortRequested,
                Me.CancellationToken.IsCancellationRequested)
        End Get
    End Property

    ''' <summary> Clears the signals. </summary>
    Private Sub ClearSignals()
        If Me._CancellationTokenSource IsNot Nothing Then
            Me._CancellationTokenSource.Dispose()
            Me._CancellationTokenSource = Nothing
        End If
        If Me._PauseTokenSource IsNot Nothing Then
            Me._PauseTokenSource.Dispose()
            Me._PauseTokenSource = Nothing
        End If

        If Me._StopTokenSource IsNot Nothing Then
            Me._StopTokenSource.Dispose()
            Me._StopTokenSource = Nothing
        End If
        If Me.ManualResetEvent IsNot Nothing Then
            Me.ManualResetEvent.Dispose()
            Me._ManualResetEvent = Nothing
        End If

        ' instantiate cancellation token
        Me.CancellationTokenSource = New CancellationTokenSource
        Me.CancellationToken = Me._CancellationTokenSource.Token

        ' instantiate Pause token
        Me.PauseTokenSource = New CancellationTokenSource
        Me.PauseToken = Me._PauseTokenSource.Token

        ' instantiate stop token
        Me.StopTokenSource = New CancellationTokenSource
        Me.StopToken = Me._StopTokenSource.Token

        ' use a wait handler to wait for any time intervals
        Me.ManualResetEvent = New ManualResetEvent(False)
    End Sub

    ''' <summary> Enqueue stop symbol. </summary>
    Public MustOverride Sub EnqueueStopSymbol() Implements IMachine.EnqueueStopSymbol


#End Region

#Region " TASK "

    ''' <summary> The task. </summary>
    Private _Task As Task

    ''' <summary> Gets the task. </summary>
    ''' <value> The task. </value>
    Protected ReadOnly Property Task As Task Implements IMachine.Task
        Get
            Return Me._Task
        End Get
    End Property

    ''' <summary> Activates the machine asynchronous task. </summary>
    ''' <remarks> 
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <returns> A Task. </returns>
    Public Function ActivateTask() As Task Implements IMachine.ActivateTask
        SyncLock Me.RunningLocker
            Me.ClearSignals()
            Me._Task = New Task(AddressOf Me.StartSequence)
            Me.Task.Start()
        End SyncLock
        Return Me.Task
    End Function

    ''' <summary> Asynchronous task. </summary>
    ''' <remarks> 
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <returns> A Task. </returns>
    Public Async Function AsyncTask() As Task
        SyncLock Me.RunningLocker
            Me.ClearSignals()
        End SyncLock
        Await Task.Run(AddressOf Me.StartSequence)
    End Function

    ''' <summary> Activates the machine asynchronous task. </summary>
    ''' <remarks> 
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="onsetDelay"> Gets or sets the time to wait before starting the machine cycle. </param>
    ''' <param name="interval">   Specifies the state machine period. </param>
    ''' <returns> A Task. </returns>
    Public Function ActivateTask(ByVal onsetDelay As TimeSpan, ByVal interval As TimeSpan) As Task Implements IMachine.ActivateTask
        Me.OnsetDelay = onsetDelay
        Me.Interval = interval
        Return Me.ActivateTask()
    End Function

    ''' <summary> Resumes the machine asynchronous task. </summary>
    ''' <returns> A Task. </returns>
    Public Function ResumeTask() As Task Implements IMachine.ResumeTask
        SyncLock Me.RunningLocker
            Me.ClearSignals()
            Me._Task = New Task(AddressOf Me.ResumeSequence)
            Me.Task.Start()
            Return Me.Task
        End SyncLock
    End Function

#End Region

#Region " THREADING "

    ''' <summary> Gets the machine thread. </summary>
    ''' <value> The machine thread. </value>
    Protected ReadOnly Property MachineThread As Threading.Thread Implements IMachine.MachineThread

    ''' <summary> Asynchronously activate the machine. </summary>
    ''' <remarks> 
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="onsetDelay"> Gets or sets the time to wait before starting the machine cycle. </param>
    ''' <param name="interval">   Specifies the state machine period. </param>
    Public Sub ActivateThread(ByVal onsetDelay As TimeSpan, ByVal interval As TimeSpan) Implements IMachine.ActivateThread
        Me.OnsetDelay = onsetDelay
        Me.Interval = interval
        Me.ActivateThread()
    End Sub

    ''' <summary> Activates the machine asynchronous thread operations. </summary>
    ''' <remarks> 
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    Public Sub ActivateThread() Implements IMachine.ActivateThread
        SyncLock Me.RunningLocker
            Me.ClearSignals()
            Me._MachineThread = New Threading.Thread(New Threading.ThreadStart(AddressOf Me.StartSequence))
            Me._MachineThread.Start()
        End SyncLock
    End Sub

    Public Sub ResumeThread() Implements IMachine.ResumeThread
        SyncLock Me.RunningLocker
            Me.ClearSignals()
            Me._MachineThread = New Threading.Thread(New Threading.ThreadStart(AddressOf Me.ResumeSequence))
            Me._MachineThread.Start()
        End SyncLock
    End Sub

#End Region

End Class

