Imports System.ComponentModel
Imports isr.Core.EventHandlerExtensions
''' <summary>
''' Implements a state in a finite automata sequencing.
''' </summary>
''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' based on Moore machine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreMachine.aspx
''' Applies to expansions from the original.
''' </para></remarks>
<Description("Finite State Machine Timeout State"), DefaultEvent("Reenter")>
Public Class TimeoutState(Of TState)
    Inherits State(Of TState)
    Implements ITimeoutState(Of TState)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs the state. </summary>
    ''' <param name="identity">        The state ID. </param>
    ''' <param name="timeoutInterval"> Specifies the state timeout interval. </param>
    Public Sub New(ByVal identity As TState, ByVal timeoutInterval As TimeSpan)
        MyBase.New(identity)
        Me._timeoutInterval = timeoutInterval
        Me._TimeoutLocker = New Object
    End Sub

    ''' <summary> Constructs the state. </summary>
    ''' <param name="identity"> The state ID. </param>
    Public Sub New(ByVal identity As TState)
        Me.New(identity, TimeSpan.MaxValue)
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.TimeoutEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.TimeoutEvent?.GetInvocationList
                        RemoveHandler Me.Timeout, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If
                SyncLock Me.EventLocker
                    If Me._TimeoutTimer IsNot Nothing Then Me._TimeoutTimer.Dispose() : Me._TimeoutTimer = Nothing
                End SyncLock
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>Raises the Enter event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overrides Sub OnEnter(ByVal e As System.EventArgs)
        SyncLock Me.EventLocker
            ' start the timeout timer. Timeout interval is infinite (-1) if timeout is non-positive.
            Me._IsTimeout = False
            Me._TimeoutTimer = New System.Threading.Timer(New System.Threading.TimerCallback(AddressOf Me.OnTimer),
                                                              New TimerState,
                                                              Me.ThreadingTimeout,
                                                              System.Threading.Timeout.Infinite)
        End SyncLock
        MyBase.OnEnter(e)
    End Sub

    ''' <summary>Raises the Leave event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overrides Sub OnLeave(ByVal e As System.EventArgs)
        SyncLock Me.EventLocker
            ' stop the timeout timer.
            Me._TimeoutTimer.Change(Threading.Timeout.Infinite, Threading.Timeout.Infinite)
            Me._TimeoutTimer.Dispose()
            Me._TimeoutTimer = Nothing
        End SyncLock
        MyBase.OnLeave(e)
    End Sub

    ''' <summary>Raises the Timeout event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnTimeout(ByVal e As System.EventArgs) Implements ITimeoutState(Of TState).OnTimeout
        Me._IsTimeout = True
        Me.OnFailed()
        Me.TimeoutEvent.SyncNotify(Me, e) ' .SafeBeginEndInvoke(Me, e)
    End Sub

    ''' <summary>Occurs upon state deactivation (exit).</summary>
    ''' <remarks>Use this event to notify the container class that the state was deactivated.</remarks>
    Public Event Timeout As EventHandler(Of System.EventArgs) Implements ITimeoutState(Of TState).Timeout

#End Region

#Region " MEMBERS "

    ''' <summary>
    ''' Returns true if state hit timeout. 
    ''' </summary>
    Public ReadOnly Property IsTimeout() As Boolean Implements ITimeoutState(Of TState).IsTimeout

    ''' <summary>
    ''' Returns a value timeout value.
    ''' </summary>
    Public ReadOnly Property ThreadingTimeout() As Integer
        Get
            Dim result As Integer = If(Me.TimeoutInterval = TimeSpan.MaxValue OrElse Me.TimeoutInterval = TimeSpan.Zero,
                Threading.Timeout.Infinite,
                CInt(Me.TimeoutInterval.Ticks \ TimeSpan.TicksPerMillisecond))
            Return result
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the timeout interval.
    ''' </summary>
    Public Property TimeoutInterval() As TimeSpan Implements ITimeoutState(Of TState).TimeoutInterval

#End Region

#Region " TIMEOUT TIMER "

    ''' <summary>Gets or sets reference to the thread timer, which events run the sequencing engine.</summary>
    Private _TimeoutTimer As System.Threading.Timer

    Private ReadOnly _TimeoutLocker As Object
    ''' <summary>
    ''' Handles timer events. These depend on the reader state.
    ''' </summary>
    Protected Overridable Sub OnTimer(ByVal state As Object)

        ' cast the timer state so that we can access the timer as necessary.
        Dim currentTimerState As TimerState = CType(state, TimerState)

        ' check if already active
        If currentTimerState.IsBusy Then
            ' if already active, increment the count of missed ticks.
            currentTimerState.TicksMissed += 1
            ' and get out
            Return
        End If

        SyncLock Me._TimeoutLocker

            ' if the event disposed the timer than exit.
            If Me._TimeoutTimer Is Nothing Then
                Return
            End If

            ' tag the counter as busy.
            currentTimerState.IsBusy = True

            ' increment to total count of missed ticks
            currentTimerState.TicksMissedTotal += currentTimerState.TicksMissed

            ' and clear the count since last timer event
            currentTimerState.TicksMissed = 0

            ' throw the timeout event.
            Me.OnTimeout(System.EventArgs.Empty)

            ' tag the counter as no longer busy.
            If currentTimerState IsNot Nothing Then
                currentTimerState.IsBusy = False
            End If

        End SyncLock


    End Sub

    ''' <summary>
    ''' Provides a timer state to the timer event.</summary>
    ''' <remarks> David, 2004-07-10, 2.0.1652
    ''' From <see cref="Automata">automata library.</see> </remarks>
    Protected Class TimerState

        ''' <summary>
        ''' Gets or sets a value indicating whether this instance is busy.
        ''' </summary>
        ''' <value><c>True</c> if this instance is busy; otherwise, <c>False</c>.</value>
        Public Property IsBusy() As Boolean

        ''' <summary>
        ''' Gets or sets the ticks missed.
        ''' </summary>
        ''' <value>The ticks missed.</value>
        Public Property TicksMissed() As Integer

        ''' <summary>
        ''' Gets or sets the ticks missed total.
        ''' </summary>
        ''' <value>The ticks missed total.</value>
        Public Property TicksMissedTotal() As Integer

    End Class

#End Region

End Class
