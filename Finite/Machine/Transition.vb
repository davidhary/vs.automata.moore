Imports System.ComponentModel
Imports isr.Core
Imports isr.Core.EventHandlerExtensions
''' <summary>
''' Defines a transition between two states.
''' </summary>
''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' based on Moore machine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreMachine.aspx
''' Applies to expansions from the original.
''' </para></remarks>
<Description("Finite State Machine Transition"), DefaultEvent("Starting")>
Public Class Transition(Of TSymbol, TState)
    Implements ITransition(Of TSymbol, TState)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="Transition(Of TSymbol, TState)">transition</see>
    ''' </summary>
    ''' <param name="state">Specifies the current state.</param>
    ''' <param name="symbol">Specifies the input symbol to move to the <paramref name="nextState">next state</paramref></param>
    ''' <param name="nextState">Specifies the next state</param>
    Public Sub New(ByVal state As IState(Of TState), ByVal symbol As TSymbol, ByVal nextState As IState(Of TState))
        Me._state = state
        Me._symbol = symbol
        Me._nextState = nextState
        Me._EventLocker = New Object
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveStartingEventHandlers()
                Me.RemoveCompletedEventHandlers()
                SyncLock Me._EventLocker
                End SyncLock
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " MEMBERS "

    ''' <summary>
    ''' Gets or sets the current state for this  <see cref="Transition(Of TSymbol, TState)">transition</see>
    ''' </summary>
    Public ReadOnly Property State() As IState(Of TState) Implements ITransition(Of TSymbol, TState).State

    ''' <summary>
    ''' Gets or sets the symbol for transitioning to the next state.
    ''' </summary>
    Public ReadOnly Property Symbol() As TSymbol Implements ITransition(Of TSymbol, TState).Symbol

    ''' <summary>
    ''' Gets or sets the reference to the next state identified by this <see cref="Transition(Of TSymbol, TState)">transition</see>
    ''' </summary>
    Public ReadOnly Property NextState() As IState(Of TState) Implements ITransition(Of TSymbol, TState).NextState

#End Region

#Region " EVENTS "

    Private ReadOnly _EventLocker As Object


#Region " STARTING "

    ''' <summary>Raises the Starting event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnStarting(ByVal e As System.ComponentModel.CancelEventArgs) Implements ITransition(Of TSymbol, TState).OnStarting
        If e Is Nothing OrElse e.Cancel Then Return
        SyncLock Me._EventLocker
            Me._StartingEventHandlers.Send(Me, e)
        End SyncLock
    End Sub

    ''' <summary> Removes the<see cref="Starting"/> event handlers. </summary>
    Protected Sub RemoveStartingEventHandlers()
        Me._StartingEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="Starting"/> event handlers. </summary>
    Private ReadOnly _StartingEventHandlers As New EventHandlerContextCollection(Of CancelEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="Starting"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Starting As EventHandler(Of CancelEventArgs) Implements ITransition(Of TSymbol, TState).Starting
        AddHandler(value As EventHandler(Of CancelEventArgs))
            Me._StartingEventHandlers.Add(New EventHandlerContext(Of CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of CancelEventArgs))
            Me._StartingEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As CancelEventArgs)
            Me._StartingEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

#End Region

#Region " Completed "

    ''' <summary>Raises the Completed event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnCompleted(ByVal e As System.ComponentModel.CancelEventArgs) Implements ITransition(Of TSymbol, TState).OnCompleted
        SyncLock Me._EventLocker
            Me._CompletedEventHandlers.Send(Me, e)
        End SyncLock
    End Sub

    ''' <summary> Removes the<see cref="Completed"/> event handlers. </summary>
    Protected Sub RemoveCompletedEventHandlers()
        Me._CompletedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="Completed"/> event handlers. </summary>
    Private ReadOnly _CompletedEventHandlers As New EventHandlerContextCollection(Of CancelEventArgs)


    ''' <summary> Event queue for all listeners interested in <see cref="Completed"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Completed As EventHandler(Of CancelEventArgs) Implements ITransition(Of TSymbol, TState).Completed
        AddHandler(value As EventHandler(Of CancelEventArgs))
            Me._CompletedEventHandlers.Add(New EventHandlerContext(Of CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of CancelEventArgs))
            Me._CompletedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As CancelEventArgs)
            Me._CompletedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

#End Region

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Compares the <see cref="Transition(Of TSymbol, TState)">transition</see> to a generic object.
    ''' </summary>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean Implements ITransition(Of TSymbol, TState).Equals
        Return Me.Equals(TryCast(obj, Transition(Of TSymbol, TState)))
    End Function

    ''' <summary>
    ''' Compares to another <see cref="Transition(Of TSymbol, TState)">transition</see>.
    ''' Transitions are compared using only the current state and symbol as 
    ''' this state machine must be deterministic.
    ''' </summary>
    Public Overloads Function Equals(ByVal value As ITransition(Of TSymbol, TState)) As Boolean Implements ITransition(Of TSymbol, TState).Equals

        Return value IsNot Nothing AndAlso
                   Me._Symbol.GetHashCode().Equals(value.Symbol.GetHashCode) AndAlso
                   Me._State.GetHashCode().Equals(value.State.GetHashCode)

    End Function

    ''' <summary>
    ''' Returns a unique hash code for this transition.
    ''' </summary>
    Public Overrides Function GetHashCode() As Integer Implements ITransition(Of TSymbol, TState).GetHashCode
        Return Me._Symbol.GetHashCode() Xor Me._State.GetHashCode()
    End Function

    ''' <summary>Returns True if equal.</summary>
    ''' <param name="left">The left hand side item to compare for equality</param>
    ''' <param name="right">The left hand side item to compare for equality</param>
    Public Shared Operator =(ByVal left As Transition(Of TSymbol, TState), ByVal right As Transition(Of TSymbol, TState)) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.Equals(right)
        End If
    End Operator

    ''' <summary>Returns True if not equal.</summary>
    ''' <param name="left">The left hand side item to compare for equality</param>
    ''' <param name="right">The left hand side item to compare for equality</param>
    Public Shared Operator <>(ByVal left As Transition(Of TSymbol, TState), ByVal right As Transition(Of TSymbol, TState)) As Boolean
        If left Is Nothing Then
            Return right IsNot Nothing
        ElseIf right Is Nothing Then
            Return True
        Else
            Return Not left.Equals(right)
        End If
    End Operator

#End Region

#Region " TO STRING "

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String Implements ITransition(Of TSymbol, TState).ToString
        Return $"{Me.Symbol}: {Me.State}>{Me.NextState}"
    End Function

#End Region

End Class

''' <summary> A queue of transitions. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-05-02 </para></remarks>
Public Class TransitionQueue(Of TSymbol, TState)
    Inherits Collections.Generic.Queue(Of ITransition(Of TSymbol, TState))

    Private Const _DefaultPrefix As String = "    t:s>s>"
    ''' <summary> Transition trace. </summary>
    ''' <returns> A String. </returns>
    Public Function TransitionTrace(ByVal prefix As String) As String
        Dim builder As New System.Text.StringBuilder
        For Each transition As Transition(Of TSymbol, TState) In Me
            builder.AppendLine($"{prefix}{transition}")
        Next
        Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
    End Function

    ''' <summary> Transition trace. </summary>
    ''' <returns> A String. </returns>
    Public Function TransitionTrace() As String
        Return Me.TransitionTrace(_DefaultPrefix)
    End Function

End Class
