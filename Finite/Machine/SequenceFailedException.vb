''' <summary> Handles sequence failed exceptions. </summary>
''' <remarks> Use this class to handle exceptions that might be thrown exercising open, close,
''' hardware access, and other similar operations. (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-04-23, 4.5956.x. </para></remarks>
<Serializable()>
Public Class SequenceFailedException
    Inherits isr.Core.ExceptionBase

#Region " CONSTRUCTION and CLEANUP "

    Private Const _DefaultMessage As String = "Sequence failed."

    ''' <summary> Initializes a new instance of the <see cref="T:OperationFailedException" /> class. Uses
    ''' the internal default message. </summary>
    Public Sub New()
        MyBase.New(SequenceFailedException._DefaultMessage)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:OperationFailedException" /> class. </summary>
    ''' <param name="message"> The message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:OperationFailedException" /> class. </summary>
    ''' <param name="message">        The message. </param>
    ''' <param name="innerException"> Specifies the exception that was trapped for throwing this
    ''' exception. </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:OperationFailedException" /> class. </summary>
    ''' <param name="format"> The format. </param>
    ''' <param name="args">   The arguments. </param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(format, args)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:OperationFailedException" /> class. </summary>
    ''' <param name="innerException"> Specifies the InnerException. </param>
    ''' <param name="format">         Specifies the exception formatting. </param>
    ''' <param name="args">           Specifies the message arguments. </param>
    Public Sub New(ByVal innerException As System.Exception, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(innerException, format, args)
    End Sub

    ''' <summary> Initializes a new instance of the class with serialized data. </summary>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    ''' that holds the serialized object data about the exception being thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    ''' that contains contextual information about the source or destination. </param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

#Region " GET OBJECT DATA "

    ''' <summary> Overrides the GetObjectData method to serialize custom values. </summary>
    ''' <param name="info">    Represents the SerializationInfo of the exception. </param>
    ''' <param name="context"> Represents the context information of the exception. </param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)>
    Public Overrides Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo,
                                       ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

End Class

