Imports System.ComponentModel
Imports System.Runtime.ExceptionServices
Imports isr.Core
Imports isr.Core.EventHandlerExtensions

''' <summary>
''' Implements a state in a finite automata sequencing.
''' </summary>
''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' based on Moore machine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreMachine.aspx
''' Applies to expansions from the original.
''' </para></remarks>
<Description("Finite State Machine State"), DefaultEvent("Reenter")>
Public Class State
    Implements IState

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs the state. </summary>
    Public Sub New(ByVal name As String)
        MyBase.New()
        Me.Name = name
        Me.EventLocker = New Object
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.ExceptionsDispatched IsNot Nothing Then Me._ExceptionsDispatched.Dispose() : Me._ExceptionsDispatched = Nothing
                ' Free managed resources when explicitly called
                Me.RemoveEnterEventHandlers()
                Me.RemoveEnteringEventHandlers()
                Me.RemoveLeaveEventHandlers()
                Me.RemoveLeavingEventHandlers()
                Me.RemoveReenterEventHandlers()
                Me.RemoveReenteringEventHandlers()
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary> Gets or sets the event locker. </summary>
    ''' <value> The event locker. </value>
    Protected ReadOnly Property EventLocker As Object

    ''' <summary> Gets the exception dispatched. </summary>
    ''' <value> The exception dispatched. </value>
    Public ReadOnly Property ExceptionsDispatched As Concurrent.BlockingCollection(Of ExceptionDispatchInfo) Implements IState.ExceptionsDispatched

    ''' <summary> Gets or sets the dispatcher collection timeout. </summary>
    ''' <value> The dispatcher collection timeout. </value>
    Public Shared Property DispatcherCollectionTimeout As TimeSpan = TimeSpan.FromMilliseconds(10)

#Region " ENTERING "

    ''' <summary>Raises the Entering event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnEntering(ByVal e As System.ComponentModel.CancelEventArgs) Implements IState.OnEntering
        Me.SyncNotifyEntering(e)
    End Sub

    ''' <summary> Removes the<see cref="Entering"/> event handlers. </summary>
    Protected Sub RemoveEnteringEventHandlers()
        Me._EnteringEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="Entering"/> event handlers. </summary>
    Private ReadOnly _EnteringEventHandlers As New EventHandlerContextCollection(Of CancelEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="Entering"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Entering As EventHandler(Of CancelEventArgs) Implements IState.Entering
        AddHandler(value As EventHandler(Of CancelEventArgs))
            Me._EnteringEventHandlers.Add(New EventHandlerContext(Of CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of CancelEventArgs))
            Me._EnteringEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As CancelEventArgs)
            Me._EnteringEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="Entering">Entering Event</see>. </summary>
    ''' <param name="e"> The <see cref="CancelEventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyEntering(ByVal e As CancelEventArgs)
        ' clear any dispatched exceptions.
        Me.ExceptionsDispatched?.Dispose()
        Try
            Me._EnteringEventHandlers.Send(Me, e)
        Catch ex As Exception
            Me._ExceptionsDispatched = New Concurrent.BlockingCollection(Of ExceptionDispatchInfo)
            Me._ExceptionsDispatched.TryAdd(ExceptionDispatchInfo.Capture(ex), CInt(DispatcherCollectionTimeout.TotalMilliseconds))
            Throw
        Finally
        End Try
    End Sub

#End Region

#Region " ENTER "

    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnEnter(ByVal e As System.EventArgs) Implements IState.OnEnter
        Me.SyncNotifyEnter(e)
    End Sub

    ''' <summary> Removes the  <see cref="Enter"/> event handlers. </summary>
    Protected Sub RemoveEnterEventHandlers()
        Me._EnterEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The  <see cref="Enter"/> event handlers. </summary>
    Private ReadOnly _EnterEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in  <see cref="Enter"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Enter As EventHandler(Of System.EventArgs) Implements IState.Enter
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._EnterEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._EnterEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._EnterEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="Enter">Enter Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyEnter(ByVal e As System.EventArgs)
        ' clear any dispatched exceptions.
        Me.ExceptionsDispatched?.Dispose()
        Try
            Me._Failed = False
            Me._EnterEventHandlers.Send(Me, e)
        Catch ex As Exception
            Me._ExceptionsDispatched = New Concurrent.BlockingCollection(Of ExceptionDispatchInfo)
            Me._ExceptionsDispatched.TryAdd(ExceptionDispatchInfo.Capture(ex), CInt(DispatcherCollectionTimeout.TotalMilliseconds))
            Throw
        Finally
        End Try
    End Sub

#End Region

#Region " LEAVING "

    ''' <summary>Raises the Leaving event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnLeaving(ByVal e As System.ComponentModel.CancelEventArgs) Implements IState.OnLeaving
        Me.SyncNotifyLeaving(e)
    End Sub

    ''' <summary> Removes the<see cref="Leaving"/> event handlers. </summary>
    Protected Sub RemoveLeavingEventHandlers()
        Me._LeavingEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="Leaving"/> event handlers. </summary>
    Private ReadOnly _LeavingEventHandlers As New EventHandlerContextCollection(Of CancelEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="Leaving"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Leaving As EventHandler(Of CancelEventArgs) Implements IState.Leaving
        AddHandler(value As EventHandler(Of CancelEventArgs))
            Me._LeavingEventHandlers.Add(New EventHandlerContext(Of CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of CancelEventArgs))
            Me._LeavingEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As CancelEventArgs)
            Me._LeavingEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="Leaving">Leaving Event</see>. </summary>
    ''' <param name="e"> The <see cref="CancelEventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyLeaving(ByVal e As CancelEventArgs)
        ' clear any dispatched exceptions.
        Me.ExceptionsDispatched?.Dispose()
        Try
            Me._LeavingEventHandlers.Send(Me, e)
        Catch ex As Exception
            Me._ExceptionsDispatched = New Concurrent.BlockingCollection(Of ExceptionDispatchInfo)
            Me._ExceptionsDispatched.TryAdd(ExceptionDispatchInfo.Capture(ex), CInt(DispatcherCollectionTimeout.TotalMilliseconds))
            Throw
        Finally
        End Try
    End Sub

#End Region

#Region " LEAVE "

    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnLeave(ByVal e As System.EventArgs) Implements IState.OnLeave
        Me.SyncNotifyLeave(e)
    End Sub

    ''' <summary> Removes the  <see cref="Leave"/> event handlers. </summary>
    Protected Sub RemoveLeaveEventHandlers()
        Me._LeaveEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The  <see cref="Leave"/> event handlers. </summary>
    Private ReadOnly _LeaveEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in  <see cref="Leave"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Leave As EventHandler(Of System.EventArgs) Implements IState.Leave
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._LeaveEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._LeaveEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._LeaveEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="Leave">Leave Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyLeave(ByVal e As System.EventArgs)
        ' clear any dispatched exceptions.
        Me.ExceptionsDispatched?.Dispose()
        Try
            Me._LeaveEventHandlers.Send(Me, e)
        Catch ex As Exception
            Me._ExceptionsDispatched = New Concurrent.BlockingCollection(Of ExceptionDispatchInfo)
            Me._ExceptionsDispatched.TryAdd(ExceptionDispatchInfo.Capture(ex), CInt(DispatcherCollectionTimeout.TotalMilliseconds))
            Throw
        Finally
        End Try
    End Sub

#End Region

#Region " REENTERING "

    ''' <summary>Raises the Reentering event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnReentering(ByVal e As System.ComponentModel.CancelEventArgs) Implements IState.OnReentering
        Me.SyncNotifyReentering(e)
    End Sub

    ''' <summary> Removes the<see cref="Reentering"/> event handlers. </summary>
    Protected Sub RemoveReenteringEventHandlers()
        Me._ReenteringEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="Reentering"/> event handlers. </summary>
    Private ReadOnly _ReenteringEventHandlers As New EventHandlerContextCollection(Of CancelEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="Reentering"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Reentering As EventHandler(Of CancelEventArgs) Implements IState.Reentering
        AddHandler(value As EventHandler(Of CancelEventArgs))
            Me._ReenteringEventHandlers.Add(New EventHandlerContext(Of CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of CancelEventArgs))
            Me._ReenteringEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As CancelEventArgs)
            Me._ReenteringEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="Reentering">Reentering Event</see>. </summary>
    ''' <param name="e"> The <see cref="CancelEventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyReentering(ByVal e As CancelEventArgs)
        ' clear any dispatched exceptions.
        Me.ExceptionsDispatched?.Dispose()
        Try
            Me._ReenteringEventHandlers.Send(Me, e)
        Catch ex As Exception
            Me._ExceptionsDispatched = New Concurrent.BlockingCollection(Of ExceptionDispatchInfo)
            Me._ExceptionsDispatched.TryAdd(ExceptionDispatchInfo.Capture(ex), CInt(DispatcherCollectionTimeout.TotalMilliseconds))
            Throw
        Finally
        End Try
    End Sub

#End Region

#Region " REENTER "

    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnReenter(ByVal e As System.EventArgs) Implements IState.OnReenter
        Me.SyncNotifyReenter(e)
    End Sub

    ''' <summary> Removes the  <see cref="Reenter"/> event handlers. </summary>
    Protected Sub RemoveReenterEventHandlers()
        Me._ReenterEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The  <see cref="Reenter"/> event handlers. </summary>
    Private ReadOnly _ReenterEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in  <see cref="Reenter"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Reenter As EventHandler(Of System.EventArgs) Implements IState.Reenter
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._ReenterEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._ReenterEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._ReenterEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="Reenter">Reenter Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyReenter(ByVal e As System.EventArgs)
        ' clear any dispatched exceptions.
        Me.ExceptionsDispatched?.Dispose()
        Try
            Me._Failed = False
            Me._ReenterEventHandlers.Send(Me, e)
        Catch ex As Exception
            Me._ExceptionsDispatched = New Concurrent.BlockingCollection(Of ExceptionDispatchInfo)
            Me._ExceptionsDispatched.TryAdd(ExceptionDispatchInfo.Capture(ex), CInt(DispatcherCollectionTimeout.TotalMilliseconds))
            Throw
        Finally
        End Try
    End Sub

#End Region

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Returns true if the instance equals another instance.
    ''' </summary>
    ''' <param name="left">Specifies a left hand value for comparison</param>
    ''' <param name="right">Specifies a right hand value for comparison</param>
    Public Overloads Shared Function Equals(ByVal left As IState, ByVal right As IState) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return String.Equals(left.Name, right.Name, StringComparison.OrdinalIgnoreCase)
        End If
    End Function

    ''' <summary>
    ''' Implements comparison with a generic object.
    ''' </summary>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean Implements IState.Equals
        Return State.Equals(Me, TryCast(obj, IState))
    End Function

    ''' <summary>
    ''' Returns the hash code for the state.
    ''' </summary>
    Public Overrides Function GetHashCode() As Integer Implements IState.GetHashCode
        Return Me.Name.GetHashCode()
    End Function

#End Region

#Region " MEMBERS "

    ''' <summary>
    ''' Gets the state ID.
    ''' </summary>
    Public ReadOnly Property Name() As String Implements IState.Name

    ''' <summary>
    ''' Gets or sets the failed indicator, which is used by count out and timeout states.
    ''' </summary>
    ''' <value> The failed. </value>
    Public ReadOnly Property Failed As Boolean Implements IState.Failed

    ''' <summary> Executes the failed action. </summary>
    Public Overridable Sub OnFailed() Implements IState.OnFailed
        Me._Failed = True
    End Sub

#End Region

End Class


''' <summary>
''' Implements a state in a finite automata sequencing.
''' </summary>
''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' based on Moore machine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreMachine.aspx
''' Applies to expansions from the original.
''' </para></remarks>
<Description("Finite State Machine State"), DefaultEvent("Reenter")>
Public Class State(Of TState)
    Inherits State
    Implements IState(Of TState)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs the state. </summary>
    ''' <param name="identity">        The state ID. </param>
    Public Sub New(ByVal identity As TState)
        MyBase.New(identity.ToString)
        Me.Identity = identity
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Returns true if the instance equals another instance.
    ''' </summary>
    Public Overloads Function Equals(ByVal value As IState(Of TState)) As Boolean Implements IState(Of TState).Equals
        Return State(Of TState).Equals(Me, value)
    End Function

    ''' <summary>
    ''' Returns the hash code for the state.
    ''' </summary>
    Public Overrides Function GetHashCode() As Integer Implements IState(Of TState).GetHashCode
        Return Me.Identity.GetHashCode()
    End Function

#End Region

#Region " MEMBERS "

    ''' <summary>
    ''' Gets the state ID.
    ''' </summary>
    Public ReadOnly Property Identity() As TState Implements IState(Of TState).Identity

#End Region

End Class

