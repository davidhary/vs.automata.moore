Imports System.Threading
Imports System.ComponentModel
Imports System.Runtime.ExceptionServices
Imports isr.Core
Imports isr.Core.StackTraceExtensions
Imports isr.Automata.Finite.ExceptionExtensions
''' <summary>
''' A base automaton class for handling finite automata sequencing but without loop cycling.
''' </summary>
''' <remarks> David, 2009-04-26, 2.3.3403.x
''' Based on Moore Engine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreEngine.aspx <para>
''' An automaton is a mathematical model for a finite state Engine. A finite state Engine is a Engine that,
''' given an input of symbols, "jumps", or transitions, through a series of states according to a transition 
''' function (which can be expressed as a table). In the common "Mealy" variety of final state Engines, this transition function 
''' tells the automaton which state to go to next given a current state and a current input symbol. </para><para>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para></remarks>
<Description("Finite State Engine"), DefaultEvent("SymbolAcknowledged"), DefaultProperty("Name")>
Public MustInherit Class Engine(Of TSymbol, TState)
    Inherits isr.Core.Models.ViewModelBase
    Implements IEngine(Of TSymbol, TState)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>Constructs this class.</summary>
    Protected Sub New(ByVal name As String)

        MyBase.New()
        Me._Name = name
        Me._symbols = New Collections.Generic.Dictionary(Of Integer, TSymbol)
        Me._states = New StateCollection(Of TState)()
        Me._transitionTable = New TransitionFunction(Of TSymbol, TState)
        Me._OnsetDelay = TimeSpan.Zero
        Me.ExecuteLocker = New Object
        Me._Stopwatch = New Stopwatch
        Me._Elapsed = TimeSpan.Zero
        Me._MinimumElapsed = TimeSpan.FromMilliseconds(100)
        Me._TransitionTrace = New TransitionQueue(Of TSymbol, TState)
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets the is disposed. </summary>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases the
    ''' managed resources.
    ''' </summary>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="_startState", Justification:="The states are disposed as part of the collection")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="_currentState", Justification:="The states are disposed as part of the collection")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="_previousState", Justification:="The states are disposed as part of the collection")>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveSymbolAcknowledgedEventHandlers()
                Me.RemoveFailedEventHandlers()
                Me.RemoveInitiatedEventHandlers()
                Me.RemovePropertyChangedEventHandlers()
                'SyncLock ExecuteLocker
                ' terminate the references
                Me._States?.DisposeStates() : Me._States = Nothing
                Me._Symbols?.Clear() : Me._Symbols = Nothing
                If Me._TransitionTable IsNot Nothing Then Me._TransitionTable.Dispose() : Me._TransitionTable = Nothing
                Me._StartState = Nothing
                Me._CurrentState = Nothing
                Me._PreviousState = Nothing
                'End SyncLock
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#End Region

#Region " EVENTS: MANAGERS "

    ''' <summary>
    ''' Used to lock execution for synchronization.
    ''' </summary>
    Protected ReadOnly Property ExecuteLocker As Object

#End Region

#Region " EVENTS: SYMBOL ACKNOWLEDGED "

    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnSymbolAcknowledged(ByVal e As SymbolEventArgs(Of TSymbol)) Implements IEngine(Of TSymbol, TState).OnSymbolAcknowledge
        Me.SyncNotifySymbolAcknowledged(e)
    End Sub

    ''' <summary> Removes the  <see cref="SymbolAcknowledged"/> event handlers. </summary>
    Protected Sub RemoveSymbolAcknowledgedEventHandlers()
        Me._SymbolAcknowledgedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The  <see cref="SymbolAcknowledged"/> event handlers. </summary>
    Private ReadOnly _SymbolAcknowledgedEventHandlers As New EventHandlerContextCollection(Of SymbolEventArgs(Of TSymbol))

    ''' <summary> Event queue for all listeners interested in  <see cref="SymbolAcknowledged"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event SymbolAcknowledged As EventHandler(Of SymbolEventArgs(Of TSymbol)) Implements IEngine(Of TSymbol, TState).SymbolAcknowledged
        AddHandler(value As EventHandler(Of SymbolEventArgs(Of TSymbol)))
            Me._SymbolAcknowledgedEventHandlers.Add(New EventHandlerContext(Of SymbolEventArgs(Of TSymbol))(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of SymbolEventArgs(Of TSymbol)))
            Me._SymbolAcknowledgedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As SymbolEventArgs(Of TSymbol))
            Me._SymbolAcknowledgedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="SymbolAcknowledged">SymbolAcknowledged Event</see>. </summary>
    ''' <param name="e"> The <see cref="SymbolEventArgs(Of TSymbol)" /> instance containing the event data. </param>
    Protected Sub SyncNotifySymbolAcknowledged(ByVal e As SymbolEventArgs(Of TSymbol))
        Me._SymbolAcknowledgedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " EVENTS: INITIATED "

    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnInitiated(ByVal e As System.EventArgs) Implements IEngine.OnInitiated
        Me.ConsecutiveFailureCount = 0
        Me.Elapsed = TimeSpan.Zero
        Me.Stopwatch.Reset()
        Me.Stopwatch.Start()
        Me.SyncNotifyInitiated(e)
    End Sub

    ''' <summary> Removes the  <see cref="Initiated"/> event handlers. </summary>
    Protected Sub RemoveInitiatedEventHandlers()
        Me._InitiatedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The  <see cref="Initiated"/> event handlers. </summary>
    Private ReadOnly _InitiatedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in  <see cref="Initiated"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Initiated As EventHandler(Of System.EventArgs) Implements IEngine.Initiated
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._InitiatedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._InitiatedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._InitiatedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="Initiated">Initiated Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyInitiated(ByVal e As System.EventArgs)
        Me._InitiatedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " EVENTS: FAILED "

    ''' <summary>Raises the Failed event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnFailed(ByVal e As System.IO.ErrorEventArgs) Implements IEngine.OnFailed
        If e IsNot Nothing Then
            Me.ConsecutiveFailureCount += 1
            Me.FailureStatus = FailureStatus.SequenceException
            Me.FailureDetails = e.GetException.ToFullBlownString
            Me.SyncNotifyFailed(e)
        End If
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:isr.Automata.IEngine`2.Failed">
    ''' Failed</see>
    '''  event.
    ''' </summary>
    ''' <param name="exception"> The exception. </param>
    Protected Overridable Sub OnFailed(ByVal exception As Exception) Implements IEngine.OnFailed
        If exception Is Nothing Then Return
        exception.Data.Add("Current State", Me.CurrentStateName)
        exception.Data.Add("Previous State", Me.PreviousStateName)
        exception.Data.Add("Last Input", Me.LastInputSymbolName)
        Me.OnFailed(New System.IO.ErrorEventArgs(exception))
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:isr.Automata.IEngine`2.Failed">
    ''' Failed</see>
    '''  event.
    ''' </summary>
    ''' <param name="exception">     The exception. </param>
    ''' <param name="detailsFormat"> The details format. </param>
    ''' <param name="args">          A variable-length parameters list containing arguments. </param>
    Protected Overridable Sub OnFailed(ByVal exception As Exception, ByVal detailsFormat As String, ByVal ParamArray args() As Object) Implements IEngine.OnFailed
        If exception IsNot Nothing Then exception.Data.Add("State", Me.CurrentState?.Identity.ToString)
        Me.OnFailed(New System.IO.ErrorEventArgs(New SequenceFailedException(exception, detailsFormat, args)))
    End Sub

    ''' <summary> Removes the  <see cref="Failed"/> event handlers. </summary>
    Protected Sub RemoveFailedEventHandlers()
        Me._FailedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The  <see cref="Failed"/> event handlers. </summary>
    Private ReadOnly _FailedEventHandlers As New EventHandlerContextCollection(Of System.IO.ErrorEventArgs)

    ''' <summary> Event queue for all listeners interested in  <see cref="Failed"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Failed As EventHandler(Of System.IO.ErrorEventArgs) Implements IEngine.Failed
        AddHandler(value As EventHandler(Of System.IO.ErrorEventArgs))
            Me._FailedEventHandlers.Add(New EventHandlerContext(Of System.IO.ErrorEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.IO.ErrorEventArgs))
            Me._FailedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.IO.ErrorEventArgs)
            Me._FailedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="Failed">Failed Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.IO.ErrorEventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyFailed(ByVal e As System.IO.ErrorEventArgs)
        Me._FailedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " PROGRESS "

    Private _ConsecutiveFailureCount As Integer
    ''' <summary> Gets or sets the number of consecutive failures. </summary>
    ''' <value> The number of consecutive failures. </value>
    Public Property ConsecutiveFailureCount As Integer Implements IEngine.ConsecutiveFailureCount
        Get
            Return Me._ConsecutiveFailureCount
        End Get
        Set(value As Integer)
            Me._ConsecutiveFailureCount = value
            Me.AsyncNotifyPropertyChanged()
        End Set
    End Property

    Private _ConsecutiveFailureAbortCount As Integer
    ''' <summary> Gets or sets the number of consecutive failure aborts. </summary>
    ''' <value> The number of consecutive failure aborts. </value>
    Public Property ConsecutiveFailureAbortCount As Integer Implements IEngine.ConsecutiveFailureAbortCount
        Get
            Return Me._ConsecutiveFailureAbortCount
        End Get
        Set(value As Integer)
            Me._ConsecutiveFailureAbortCount = value
            Me.AsyncNotifyPropertyChanged()
        End Set
    End Property

    Private _PercentProgress As Integer
    Public Overridable Property PercentProgress() As Integer Implements IEngine.PercentProgress
        Get
            Return Me._PercentProgress
        End Get
        Set(value As Integer)
            If Me.PercentProgress <> value Then
                Me._PercentProgress = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Updates the percent progress. </summary>
    Public MustOverride Sub UpdatePercentProgress() Implements IEngine.UpdatePercentProgress

    Private _ProgressIndicatorPercent As Integer
    Public Overridable Property ProgressIndicatorPercent() As Integer Implements IEngine.ProgressIndicatorPercent
        Get
            Return Me._ProgressIndicatorPercent
        End Get
        Set(value As Integer)
            If Me.ProgressIndicatorPercent <> value Then
                Me._ProgressIndicatorPercent = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Updates the progress indicator percent. </summary>
    Public MustOverride Sub UpdateProgressIndicatorPercent() Implements IEngine.UpdateProgressIndicatorPercent

    ''' <summary> Gets the Engine stopwatch. </summary>
    ''' <value> The Engine stopwatch. </value>
    Protected ReadOnly Property Stopwatch As Stopwatch Implements IEngine.Stopwatch

    ''' <summary> Gets or sets the previous elapsed. </summary>
    ''' <value> The previous elapsed. </value>
    Protected Property PreviousElapsed As TimeSpan

    Private _Elapsed As TimeSpan

    ''' <summary> Gets or sets the elapsed time since the state machine started. </summary>
    ''' <value> The elapsed time since the state machine started. </value>
    Public Overridable Property Elapsed() As TimeSpan Implements IEngine.Elapsed
        Get
            Return Me._Elapsed
        End Get
        Set(value As TimeSpan)
            If Me.Elapsed < value.Subtract(Me._MinimumElapsed) Then
                Me._PreviousElapsed = Me.Elapsed
                Me._Elapsed = value
                Me.AsyncNotifyPropertyChanged()
            ElseIf value = TimeSpan.Zero Then
                Me._PreviousElapsed = value
                Me._Elapsed = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _MinimumElapsed As TimeSpan
    Public Overridable Property MinimumElapsed() As TimeSpan Implements IEngine.MinimumElapsed
        Get
            Return Me._MinimumElapsed
        End Get
        Set(value As TimeSpan)
            If Me.MinimumElapsed <> value Then
                Me._MinimumElapsed = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property


#End Region

#Region " ENGINE STATES "

    ''' <summary> Notifies the state change. </summary>
    ''' <remarks>
    ''' It looks like state property notification cannot be used to handle state change and process
    ''' input with the Engine state machine. The Machine state machine might still work.
    ''' </remarks>
    Protected Sub NotifyStateChange()
        ' !@# 20190130: this may need to be restored: Me.NotifyPropertyChanging(NameOf(IEngine.CurrentState))
        Me.CurrentState.OnEnter(System.EventArgs.Empty)
        Me.UpdateProgressIndicatorPercent()
    End Sub

    Private ReadOnly Property __CurrentState As IState Implements IEngine.CurrentState
        Get
            Return Me.CurrentState
        End Get
    End Property

    Private _IsBusy As Boolean
    ''' <summary> Indicates is the engine is busy. That is it entered the busy sequence and was not stopped or idle. </summary>
    ''' <remarks> This is set by each state machine entering its busy sequence and cleared when existing the busy sequence. </remarks>
    ''' <value> <c>true</c> if busy; otherwise <c>false</c> </value>
    Public Property IsBusy As Boolean Implements IEngine.IsBusy
        Get
            Return Me._IsBusy
        End Get
        Set(value As Boolean)
            If value <> Me.IsBusy Then
                Me._IsBusy = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Query if this engine started; the engine started if its state is not the null state. </summary>
    ''' <returns> <c>true</c> if started; otherwise <c>false</c> </returns>
    Public Function IsStarted() As Boolean Implements IEngine.IsStarted
        Return Not Me.CurrentStateIdentity.Equals(Me.NullStateIdentity)
    End Function

    ''' <summary> Query if this engine is at the initial state. </summary>
    ''' <returns> <c>true</c> if initial; otherwise <c>false</c> </returns>
    Public Function IsInitial() As Boolean Implements IEngine.IsInitial
        Return Me.CurrentStateIdentity.Equals(Me.InitialStateIdentity)
    End Function

    ''' <summary> Query if this engine is at the terminal state. </summary>
    ''' <returns> <c>true</c> if terminal; otherwise <c>false</c> </returns>
    Public Function IsTerminal() As Boolean Implements IEngine.IsTerminal
        Return Me.CurrentStateIdentity.Equals(Me.TerminalStateIdentity)
    End Function

    ''' <summary> Query if this engine can be moved to the terminal State. </summary>
    ''' <returns> <c>true</c> if terminable; otherwise <c>false</c> </returns>
    Public Function IsTerminable() As Boolean Implements IEngine.IsTerminable
        Return Me.IsStarted AndAlso Not Me.IsTerminal
    End Function

    ''' <summary> Gets the current state name. </summary>
    ''' <value> The name of the current state. </value>
    Public ReadOnly Property CurrentStateName As String Implements IEngine.CurrentStateName
        Get
            Return Me.CurrentStateIdentity.ToString()
        End Get
    End Property

    Private ReadOnly Property __PreviousState As IState Implements IEngine.PreviousState
        Get
            Return Me.PreviousState
        End Get
    End Property

    ''' <summary> Gets the Previous state name. </summary>
    ''' <value> The name of the Previous state. </value>
    Public ReadOnly Property PreviousStateName As String Implements IEngine.PreviousStateName
        Get
            Return Me.PreviousStateIdentity.ToString()
        End Get
    End Property

#End Region

#Region " ENGINE(Of TSymbol, TState) STATES "

    Private _CurrentState As IState(Of TState)
    ''' <summary>
    ''' Gets reference to the active <see cref="IState">state</see>.
    ''' </summary>
    Public Overridable Property CurrentState() As IState(Of TState) Implements IEngine(Of TSymbol, TState).CurrentState
        Get
            Return Me._CurrentState
        End Get
        Protected Set(value As IState(Of TState))
            Me._CurrentState = value
        End Set
    End Property

    ''' <summary> Gets the null state identity. </summary>
    ''' <value> The null state identity. </value>
    Public MustOverride ReadOnly Property NullStateIdentity As TState Implements IEngine(Of TSymbol, TState).NullStateIdentity

    ''' <summary> Gets the initial state identity. </summary>
    ''' <value> The initial state identity. </value>
    Public MustOverride ReadOnly Property InitialStateIdentity As TState Implements IEngine(Of TSymbol, TState).InitialStateIdentity

    ''' <summary> Gets the terminal state identity. </summary>
    ''' <value> The terminal state identity. </value>
    Public MustOverride ReadOnly Property TerminalStateIdentity As TState Implements IEngine(Of TSymbol, TState).TerminalStateIdentity

    Public Overridable ReadOnly Property CurrentStateIdentity As TState Implements IEngine(Of TSymbol, TState).CurrentStateIdentity
        Get
            Return If(Me.CurrentState Is Nothing, Me.NullStateIdentity, Me.CurrentState.Identity)
        End Get
    End Property

    ''' <summary>
    ''' Gets reference to the previous active <see cref="IState">state</see>.
    ''' </summary>
    Public Overridable Property PreviousState() As IState(Of TState) Implements IEngine(Of TSymbol, TState).PreviousState

    Public Overridable ReadOnly Property PreviousStateIdentity As TState Implements IEngine(Of TSymbol, TState).PreviousStateIdentity
        Get
            Return If(Me.PreviousState Is Nothing, Me.NullStateIdentity, Me.PreviousState.Identity)
        End Get
    End Property

    ''' <summary> Enumerates Engine states in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process Engine states in this collection.
    ''' </returns>
    Public Overridable Function EngineStates() As IEnumerable(Of IState(Of TState)) Implements IEngine(Of TSymbol, TState).EngineStates
        Return Me._States.ToArray
    End Function

    ''' <summary>
    ''' A collection of states with a unique names.
    ''' </summary>
    Private _States As StateCollection(Of TState)
    ''' <summary>
    ''' Returns the collection of states with a unique names.
    ''' </summary>
    Protected Function States() As StateCollection(Of TState)
        Return Me._States
    End Function

    ''' <summary>
    ''' Gets the start state--the state at which the automaton is when no input has been processed yet.
    ''' </summary>
    Protected ReadOnly Property StartState() As IState(Of TState)

    ''' <summary>
    ''' Sets the start state to a state other than the first state that was added.
    ''' This also sets the current state.
    ''' </summary>
    Public Overridable Sub StartStateSetter(ByVal value As IState(Of TState)) Implements IEngine(Of TSymbol, TState).StartStateSetter
        Me._StartState = value
        Me.CurrentState = value
    End Sub

    ''' <summary>
    ''' Adds a <see cref="State">state</see> to the <see cref="Engine(Of TSymbol, TState)">finite
    ''' state Engine</see>.
    ''' </summary>
    ''' <param name="identity"> Specifies the state identity. Must be unique among all states defined
    '''                         for this state Engine. </param>
    ''' <returns> An IState(Of TState) </returns>
    Public Overridable Function Add(ByVal identity As TState) As IState(Of TState) Implements IEngine(Of TSymbol, TState).Add
        Return Me.Add(New State(Of TState)(identity))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:isr.Automata.IState`1">state</see> to the
    ''' <see cref="T:isr.Automata.IEngine`2">Finite state Engine</see>.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> Specifies the symbol value. </param>
    ''' <returns> An IState(Of TState) </returns>
    Public Overridable Function Add(ByVal value As IState(Of TState)) As IState(Of TState) Implements IEngine(Of TSymbol, TState).Add
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Me.States.Add(value)
        If Me.States.Count = 1 Then
            Me.StartStateSetter(value)
        End If
        Return value
    End Function

#End Region

#Region " INITIATE / TERMINATE "

    ''' <summary>
    ''' Gets or sets the time to wait before starting the Engine cycle.
    ''' </summary>
    Public Property OnsetDelay() As TimeSpan Implements IEngine.OnsetDelay

    ''' <summary> Initiates the engine with the default onset delay. </summary>
    Public Sub Initiate() Implements IEngine.Initiate
        Me.Initiate(Me.OnsetDelay)
    End Sub

    ''' <summary> Initiates the engine with the given onset delay. </summary>
    ''' <remarks> 
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="onsetDelay"> Gets or sets the time to wait before starting the Engine cycle. </param>
    Public Overridable Sub Initiate(ByVal onsetDelay As TimeSpan) Implements IEngine.Initiate

        ' clear the failure state
        Me.FailureStatus = FailureStatus.None

        'SyncLock ExecuteLocker
        ' clear the transition queue 
        Me.TransitionTrace.Clear()

        ' set the current state.
        Me.CurrentState = Me.StartState

        ' wait for the onset delay
        If onsetDelay > TimeSpan.Zero Then
            Using onsetDelayResetEvent As New ManualResetEvent(False)
                onsetDelayResetEvent.WaitOne(onsetDelay)
            End Using
        End If

        ' start the ball rolling.
        Me.OnInitiated(System.EventArgs.Empty)

        'End SyncLock

        ' this starts the state operations.
        Me.NotifyStateChange()

    End Sub

    ''' <summary> Terminates this object. </summary>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overridable Function Terminate() As Boolean Implements IEngine.Terminate
        Dim result As Boolean = Me.IsTerminable
        If result Then Me.TryProcessInput(Me.TerminalInputSymbol)
        ' if negative, the action was not initiated because already terminated.
        ' this means that the termination might have to be activated on a higher level rather than propagate from the lower level.
        Return result
    End Function

#End Region

#Region " ENGINE PROCESS INPUT "

    Private _FailureDetails As String

    ''' <summary> Gets or sets the failure details. </summary>
    ''' <value> The failure details. </value>
    Public Property FailureDetails As String Implements IEngine.FailureDetails
        Get
            Return Me._FailureDetails
        End Get
        Protected Set(value As String)
            If Not String.Equals(Me.FailureDetails, value) Then
                Me._FailureDetails = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _FailureStatus As FailureStatus

    ''' <summary> Gets or sets the status of the failure. </summary>
    ''' <value> The failure state. </value>
    Public Property FailureStatus As FailureStatus Implements IEngine.FailureStatus
        Get
            Return Me._FailureStatus
        End Get
        Protected Set(value As FailureStatus)
            If Not String.Equals(Me.FailureStatus, value) Then
                Me._FailureStatus = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the automatic reenter enabled sentinel;. </summary>
    ''' <value> <c>True</c> to reenter the current state if no input is provided; Otherwise, <c>False</c>. </value>
    Public Property AutoReenterEnabled As Boolean Implements IEngine.AutoReenterEnabled

    ''' <summary> Enumerates symbol name in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process symbol name in this collection.
    ''' </returns>
    Public MustOverride Function InputSymbolNames() As IEnumerable(Of String) Implements IEngine.InputSymbolNames

    Private _LastInputSymbolName As String

    ''' <summary> Gets or sets the last input symbol. </summary>
    ''' <value> The last input symbol. </value>
    Public Property LastInputSymbolName As String Implements IEngine.LastInputSymbolName
        Get
            Return Me._LastInputSymbolName
        End Get
        Protected Set(value As String)
            Me._LastInputSymbolName = value
            Me.AsyncNotifyPropertyChanged()
        End Set
    End Property

#End Region

#Region " PROCESS INPUT CORE "

    ''' <summary>
    ''' Immediately processes a single input ignoring the input queue. This allows running the state
    ''' Engine based on signals alone.
    ''' </summary>
    ''' <remarks> David, 2009-04-26, 2.3.3403.x.
    ''' Allows makings a transition into the same state in which case the
    ''' <see cref="IState.Leave">Leave</see> and <see cref="IState.Enter">Enter</see> events
    ''' do not occur. Use the <see cref="IState.Reenter">Reenter</see> event to process state actions. <para>
    ''' Exceptions must be trapped in the calling routing to catch exceptions thrown by the state and
    ''' transition event handlers. </para>
    ''' </remarks>
    ''' <param name="symbol"> Specifies the input to process. </param>
    ''' <param name="e">      Cancel details event information. </param>
    Protected Overridable Sub ProcessInputCore(ByVal symbol As TSymbol, ByVal e As Core.ActionEventArgs)

        If e Is Nothing Then
            Return
        ElseIf Me.CurrentState Is Nothing Then
            e.RegisterOutcomeEvent(TraceEventType.Warning, $"{Me.Name} current state is nothing.")
            Return
        End If

        Dim args As New ComponentModel.CancelEventArgs


        ' notify of elapsed time change
        Me.Elapsed = Me.Stopwatch.Elapsed
        ' Me.AsyncNotifyPropertyChanged(NameOf(IEngine.Stopwatch))

        ' clear the failure state
        Me.FailureStatus = FailureStatus.None

        ' notify of input
        Me.LastInputSymbol = symbol

        Me.OnSymbolAcknowledged(New SymbolEventArgs(Of TSymbol)(symbol))

        Dim newTranstion As ITransition(Of TSymbol, TState) = Me.TransitionTable.SelectTransition(Me.CurrentState, symbol)

        If newTranstion.NextState Is Nothing Then

            Me.CurrentState = Me.CurrentState
            e.RegisterOutcomeEvent(TraceEventType.Warning, $"{Me.Name} transition from '{Me.CurrentState.Identity}' with input '{symbol}' not found")

        Else

            newTranstion.OnStarting(args)

            If Not e.Failed Then

                If Me.CurrentState.Equals(newTranstion.NextState) Then

                    ' reentering
                    Me.CurrentState.OnReentering(args)

                    If Not e.Failed Then
                        If Me.CurrentState.Equals(Me.StartState) Then Me.TransitionTrace.Clear()
                        Me.TransitionTrace.Enqueue(newTranstion)
                        Me.CurrentState.OnReenter(System.EventArgs.Empty)
                    End If

                Else

                    Me.CurrentState.OnLeaving(args)

                    If Not e.Failed Then

                        Me.CurrentState.OnLeave(args)

                        Dim previousPreviousState As IState(Of TState) = Me._PreviousState

                        Me._PreviousState = Me.States.Item(Me.CurrentState.Identity)

                        Me.CurrentState = Me.States.Item(newTranstion.NextState.Identity)

                        Me.CurrentState.OnEntering(args)

                        If e.Failed Then

                            Me.CurrentState = Me.PreviousState

                            Me._PreviousState = previousPreviousState

                        Else

                            If Me.CurrentState.Equals(Me.StartState) Then Me.TransitionTrace.Clear()
                            Me.TransitionTrace.Enqueue(newTranstion)
                            Me.NotifyStateChange()

                        End If

                    End If

                End If

                newTranstion.OnCompleted(args)

            End If

        End If

    End Sub

#End Region

#Region " ENGINE(Of TSymbol, TState) PROCESS INPUT "

    ''' <summary> Gets or sets the Fail input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public MustOverride ReadOnly Property FailInputSymbol As TSymbol Implements IEngine(Of TSymbol, TState).FailInputSymbol

    ''' <summary> Gets or sets the Initial input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public MustOverride ReadOnly Property InitialInputSymbol As TSymbol Implements IEngine(Of TSymbol, TState).InitialInputSymbol

    ''' <summary> Gets or sets the terminal input symbol. </summary>
    ''' <value> The stop symbol. </value>
    Public MustOverride ReadOnly Property TerminalInputSymbol As TSymbol Implements IEngine(Of TSymbol, TState).TerminalInputSymbol

    Private _LastInputSymbol As TSymbol

    ''' <summary> Gets or sets the last input symbol. </summary>
    ''' <value> The last input symbol. </value>
    Public Overridable Property LastInputSymbol As TSymbol Implements IEngine(Of TSymbol, TState).LastInputSymbol
        Get
            Return Me._LastInputSymbol
        End Get
        Protected Set(value As TSymbol)
            Me._LastInputSymbol = value
            Me.AsyncNotifyPropertyChanged()
            Me.LastInputSymbolName = value.ToString
        End Set
    End Property

    ''' <summary> Gets the transition trace. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The transition trace. </value>
    Public ReadOnly Property TransitionTrace As TransitionQueue(Of TSymbol, TState)


#End Region

#Region " ENGINE(Of TSymbol, TState) PROCESS INPUT "

    ''' <summary> Registers the exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <param name="e">        Cancel details event information. </param>
    Private Sub RegisterException(ByVal activity As String, ByVal ex As Exception, ByVal e As ActionEventArgs)
        Dim dispatchBuilder As New System.Text.StringBuilder
        If Me.CurrentState.ExceptionsDispatched?.Any Then
            For Each edi As ExceptionDispatchInfo In Me.CurrentState.ExceptionsDispatched
                If Not ex.Equals(edi.SourceException) Then dispatchBuilder.AppendLine(edi.SourceException.ToFullBlownString)
            Next
        End If
        Dim stack As String = New StackFrame(Debugger.IsAttached).UserCallStack(2, "..> ")
        Dim builder As New System.Text.StringBuilder()
        builder.AppendLine($"{Me.Name} failed {activity}: {ex.Message.TrimEnd("."c)};. ")
        If Not String.IsNullOrWhiteSpace(stack) Then builder.AppendLine(stack)
        If dispatchBuilder.Length > 0 Then builder.AppendLine($"Dispatch: {dispatchBuilder}")
        builder.AppendLine(Core.StackTraceExtensions.StackTraceExtensionMethods.UserCallStack(Environment.StackTrace, 0, 12))
        builder.AppendLine(Me.TransitionTrace.ToString)
        builder.AppendLine(ex.ToFullBlownString)
        e.RegisterOutcomeEvent(TraceEventType.Error, builder.ToString.TrimEnd(Environment.NewLine.ToCharArray))
        Me.FailureStatus = FailureStatus.SequenceException
        Me.FailureDetails = e.Details
    End Sub

    ''' <summary> Appends the details. </summary>
    ''' <param name="e"> Action event information. </param>
    Private Sub AppendDetails(ByVal activity As String, ByVal e As ActionEventArgs)
        Dim builder As New System.Text.StringBuilder
        If Me.CurrentState?.Failed Then
            If e.HasDetails Then
                builder.AppendLine($"{Me.Name} failed {activity}: {e.Details}")
            Else
                builder.AppendLine($"{Me.Name} failed {activity}")
            End If
        End If
        builder.AppendLine(Core.StackTraceExtensions.StackTraceExtensionMethods.UserCallStack(Environment.StackTrace, 0, 12))
        builder.AppendLine(Me.TransitionTrace.ToString)
        e.RegisterOutcomeEvent(e.OutcomeEvent, builder.ToString.TrimEnd(Environment.NewLine.ToCharArray))
    End Sub

    ''' <summary>
    ''' Tries to process input. Catches exceptions and reports in the references
    ''' <paramref name="e"/> if exception occurred.
    ''' </summary>
    ''' <param name="input"> Specifies the input to process. </param>
    ''' <param name="e">     Cancel details event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Function TryProcessInputCore(ByVal input As TSymbol, ByVal e As ActionEventArgs) As Boolean

        If e Is Nothing Then Return False
        Dim activity As String = $"processing input {input} at {Me.CurrentStateName} state after {Me.PreviousStateName} state"
        Try
            Try
                Me.ProcessInputCore(input, e)
                If e.Failed Then
                    Me.AppendDetails(activity, e)
                    Me.FailureStatus = FailureStatus.SequenceError
                    Me.FailureDetails = e.Details
                End If
            Catch te As System.Reflection.TargetInvocationException
                If te.InnerException Is Nothing Then Throw
                Dim innerException As Exception = te.InnerException
                Dim saveStack As ThreadStart = TryCast(System.Delegate.CreateDelegate(GetType(ThreadStart), innerException,
                                                                                      "InternalPreserveStackTrace", False, False), ThreadStart)
                If saveStack IsNot Nothing Then saveStack()
                Throw innerException ' -- now we can re-throw without trashing the stack
            Catch
                Throw
            End Try
        Catch ex As Exception
            Me.RegisterException(activity, ex, e)
        Finally
        End Try
        Return Not e.Failed

    End Function

    ''' <summary>
    ''' Immediately processes a single input ignoring the input queue. This allows running the state
    ''' Engine based on signals alone. Catches exceptions and reports in the references
    ''' <paramref name="e"/> if exception occurred.
    ''' </summary>
    ''' <param name="input"> Specifies the input to process. </param>
    ''' <param name="e">     Cancel details event information. </param>
    ''' <returns>
    ''' True if state change was implement or false if the transition is invalid of the change was
    ''' Canceled.
    ''' </returns>
    Public Function TryProcessInput(ByVal input As TSymbol, ByVal e As ActionEventArgs) As Boolean Implements IEngine(Of TSymbol, TState).TryProcessInput

        'SyncLock ExecuteLocker
        Return Me.TryProcessInputCore(input, e)
        'End SyncLock

    End Function

    ''' <summary>
    ''' Immediately processes a series of inputs input ignoring the input queue. This allows running
    ''' the state Engine based on signals alone. Catches exceptions and reports in the references
    ''' <paramref name="e"/> if exception occurred.
    ''' </summary>
    ''' <param name="inputs"> Specifies the inputs to process. </param>
    ''' <param name="e">      if exception occurred </param>
    ''' <returns>
    ''' True if state change was implement or false if the transition is invalid of the change was
    ''' Canceled.
    ''' </returns>
    Public Function TryProcessInput(ByVal inputs As TSymbol(), ByVal e As ActionEventArgs) As Boolean Implements IEngine(Of TSymbol, TState).TryProcessInput
        Dim affirmative As Boolean = True
        If inputs?.Any Then
            'SyncLock ExecuteLocker
            For Each value As TSymbol In inputs
                affirmative = Me.TryProcessInputCore(value, e)
                If Not affirmative Then Exit For
            Next
            'End SyncLock
        Else
            ' notify of stopwatch change
            Me.Elapsed = Me.Stopwatch.Elapsed
            ' Me.AsyncNotifyPropertyChanged(NameOf(IEngine.Stopwatch))
        End If
        Return affirmative
    End Function

    ''' <summary>
    ''' Processes the input. 
    ''' </summary>
    ''' <param name="input"> Specifies the input to process. </param>
    ''' <param name="e">     Cancel details event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ProcessInput(ByVal input As TSymbol, ByVal e As ActionEventArgs) Implements IEngine(Of TSymbol, TState).ProcessInput
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = $"processing input {input} at {Me.CurrentStateName} state after {Me.PreviousStateName} state"
        Try
            Try
                'SyncLock ExecuteLocker
                Me.ProcessInputCore(input, e)
                If e.Failed Then
                    Me.AppendDetails(activity, e)
                    Me.FailureStatus = FailureStatus.SequenceError
                    Me.FailureDetails = e.Details
                End If
                'End SyncLock
            Catch te As System.Reflection.TargetInvocationException
                If te.InnerException Is Nothing Then Throw
                Dim innerException As Exception = te.InnerException
                Dim saveStack As ThreadStart = TryCast(System.Delegate.CreateDelegate(GetType(ThreadStart), innerException,
                                                                                      "InternalPreserveStackTrace", False, False), ThreadStart)
                If saveStack IsNot Nothing Then saveStack()
                Throw innerException ' -- now we can re-throw without trashing the stack
            Catch
                Throw
            End Try
        Catch ex As Exception
            Me.RegisterException(activity, ex, e)
        Finally
            If e.Failed Then Me.ConsecutiveFailureCount += 1
        End Try
    End Sub

    ''' <summary> Process the input described by input. </summary>
    ''' <param name="input"> The signal symbol. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryProcessInput(ByVal input As TSymbol) As isr.Core.ActionEventArgs Implements IEngine(Of TSymbol, TState).TryProcessInput
        Dim e As New isr.Core.ActionEventArgs
        Me.ProcessInput(input, e)
        Return e
    End Function

    ''' <summary> Process the input described by input. </summary>
    ''' <param name="input"> The input symbol. </param>
    Public Sub ProcessInput(ByVal input As TSymbol) Implements IEngine(Of TSymbol, TState).ProcessInput
        Dim e As New isr.Core.ActionEventArgs
        Try
            'SyncLock ExecuteLocker
            Me.ProcessInputCore(input, e)
            If e.Failed Then
                Throw New InvalidOperationException(e.Details)
            End If
            'End SyncLock
        Catch te As System.Reflection.TargetInvocationException
            If te.InnerException Is Nothing Then Throw
            Dim innerException As Exception = te.InnerException
            Dim saveStack As ThreadStart = TryCast(System.Delegate.CreateDelegate(GetType(ThreadStart), innerException,
                                                                                  "InternalPreserveStackTrace", False, False), ThreadStart)
            If saveStack IsNot Nothing Then saveStack()
            Throw innerException ' -- now we can re-throw without trashing the stack
        Catch
            Throw
        End Try
    End Sub

    ''' <summary> Process the input; send to failure state if failed. </summary>
    ''' <param name="input"> The input. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function ProcessInputOrFail(ByVal input As TSymbol) As isr.Core.ActionEventArgs Implements IEngine(Of TSymbol, TState).ProcessInputOrFail
        Dim e As New isr.Core.ActionEventArgs
        Dim activity As String = $"processing input {input} at {Me.CurrentStateName} state after {Me.PreviousStateName} state"
        Try
            Try
                'SyncLock ExecuteLocker
                Me.ProcessInputCore(input, e)
                If e.Failed Then
                    Me.AppendDetails(activity, e)
                    Me.FailureStatus = FailureStatus.SequenceError
                    Me.FailureDetails = e.Details
                    Me.OnFailed(New SequenceFailedException(e.Details))
                End If
                'End SyncLock
            Catch te As System.Reflection.TargetInvocationException
                If te.InnerException Is Nothing Then Throw
                Dim innerException As Exception = te.InnerException
                Dim saveStack As ThreadStart = TryCast(System.Delegate.CreateDelegate(GetType(ThreadStart), innerException,
                                                                                      "InternalPreserveStackTrace", False, False), ThreadStart)
                If saveStack IsNot Nothing Then saveStack()
                Throw innerException ' -- now we can re-throw without trashing the stack
            Catch
                Throw
            End Try
        Catch ex As Exception
            Me.RegisterException(activity, ex, e)
            Me.OnFailed(New SequenceFailedException(e.Details, ex))
        Finally
        End Try
        Return e
    End Function

#End Region

#Region " SYMBOLS "

    ''' <summary>
    ''' A finite set of symbols. Also called alphabet of the language the automaton accepts
    ''' Symbols are sometimes just called "letters" or "atoms".
    ''' </summary>
    Private _Symbols As Collections.Generic.Dictionary(Of Integer, TSymbol)

    ''' <summary>
    ''' Adds an input symbol.
    ''' The symbol must have a unique <see cref="GetHashCode">hash code</see>
    ''' </summary>
    ''' <param name="value">Specifies the symbol value</param>
    Public Function Add(ByVal value As TSymbol) As TSymbol Implements IEngine(Of TSymbol, TState).Add
        Me._Symbols.Add(value.GetHashCode, value)
        Return value
    End Function

#End Region

#Region " TRANSITION FUNCTION "

    ''' <summary>
    ''' Gets or sets the transition function. Defines the next state given the current state and current input.
    ''' </summary>
    Protected Property TransitionTable() As TransitionFunction(Of TSymbol, TState)

    ''' <summary> Adds a transition to the state Engine. </summary>
    ''' <param name="state">     The state. </param>
    ''' <param name="symbol">    The symbol. </param>
    ''' <param name="nextState"> . </param>
    ''' <returns> An ITransition(Of TSymbol, TState) </returns>
    Public Function Add(ByVal state As IState(Of TState),
                            ByVal symbol As TSymbol,
                            ByVal nextState As IState(Of TState)) As ITransition(Of TSymbol, TState) Implements IEngine(Of TSymbol, TState).Add
        Dim transition As ITransition(Of TSymbol, TState) = Me.TransitionTable.Add(state, symbol, nextState)
        Return transition
    End Function

    ''' <summary>
    ''' Checks if the specified <typeparamref name="TSymbol">symbol</typeparamref>input symbol
    ''' signals a valid transition from the current state.
    ''' </summary>
    ''' <param name="value"> The inputs <typeparamref name="TSymbol">symbol</typeparamref> which
    '''                      requires validation. </param>
    ''' <param name="e">     Cancel details event information. </param>
    ''' <returns>
    ''' True if the symbol leads to a valid transition or False if no transition from the current
    ''' state is defined for the current input symbol.
    ''' </returns>
    Public Function TryValidateInput(ByVal value As TSymbol, ByVal e As ActionEventArgs) As Boolean Implements IEngine(Of TSymbol, TState).TryValidateInput
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If Me.TransitionTable.SelectTransition(Me.CurrentState, value).NextState Is Nothing Then
            If Me.CurrentState Is Nothing Then
                e.RegisterOutcomeEvent(TraceEventType.Warning, $"Transition state not found for the '{value}' input; Current state is nothing.")
            Else
                e.RegisterOutcomeEvent(TraceEventType.Warning, $"Transition from the '{Me.CurrentState.Identity}' state not found for the '{value}' input.")
            End If
        End If
        Return Not e.Failed
    End Function

    ''' <summary>
    ''' Checks for the validity of all the transitions implied by the specified symbols.
    ''' </summary>
    ''' <param name="values">  The inputs <typeparamref name="TSymbol">symbol</typeparamref> which
    '''                        requires validation. </param>
    ''' <returns>
    ''' Returns True if all implied transitions are valid. Returns false after detecting the first
    ''' invalid implied transition.
    ''' </returns>
    Public Function TryValidateAllInputs(ByVal values As TSymbol(), ByVal e As ActionEventArgs) As Boolean Implements IEngine(Of TSymbol, TState).TryValidateAllInputs
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If values IsNot Nothing AndAlso values.Length > 0 Then
            For Each symbol As TSymbol In values
                If Not Me.TryValidateInput(symbol, e) Then
                    Return False
                End If
            Next
        End If
        Return Not e.Failed
    End Function

    ''' <summary>
    ''' Checks for the validity of one of the transitions implied by the specified symbols.
    ''' </summary>
    ''' <param name="values">  The inputs <typeparamref name="TSymbol">symbol</typeparamref> which
    '''                        requires validation. </param>
    ''' <returns>
    ''' Returns True if all implied transitions are valid. Returns false after detecting the first
    ''' invalid implied transition.
    ''' </returns>
    Public Function TryValidateInput(ByVal values() As TSymbol, ByVal e As ActionEventArgs) As Boolean Implements IEngine(Of TSymbol, TState).TryValidateInput
        If values IsNot Nothing AndAlso values.Length > 0 Then
            For Each symbol As TSymbol In values
                If Me.TryValidateInput(symbol, e) Then
                    Return True
                End If
            Next
            Return False
        End If
        Return True
    End Function

#End Region

#Region " CONSTRUCTION and ANALYSIS "

    ''' <summary> Gets the Engine name. </summary>
    ''' <value> The Engine name. </value>
    Public ReadOnly Property Name As String Implements IEngine.Name

    Private _AnalysisReport As System.Text.StringBuilder
    ''' <summary>
    ''' Returns the analysis report.
    ''' </summary>
    Public ReadOnly Property AnalysisReport() As String Implements IEngine.AnalysisReport
        Get
            Return If(Me._AnalysisReport Is Nothing, "Analysis not run", Me._AnalysisReport.ToString)
        End Get
    End Property

    Private _IsFullyDefined As Boolean?
    ''' <summary>
    ''' Returns true of the state Engine is fully defined.
    ''' The Engine is fully defined if all states have an associated transition.
    ''' </summary>
    Public ReadOnly Property IsFullyDefined() As Boolean? Implements IEngine.IsFullyDefined
        Get
            Return Me._isFullyDefined
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the state Engine is deterministic, i.e., if each state and symbol have a unique
    ''' next state. Namely, if all transitions are unique.
    ''' </summary>
    Public ReadOnly Property IsDeterministic() As Boolean Implements IEngine.IsDeterministic
        Get
            Return Me.transitionTable.IsDeterministic
        End Get
    End Property

    ''' <summary>
    ''' Analyzes the state Engine to determine if it is fully specified.
    ''' </summary>
    ''' <returns>
    ''' True if the Engine is fully specified and all signals and states are unique.
    ''' 
    ''' </returns>
    Public Function Analyze() As Boolean Implements IEngine.Analyze

        Me._AnalysisReport = New System.Text.StringBuilder
        Me._AnalysisReport.AppendLine($"Analysis report {DateTimeOffset.Now}")

        Me._isFullyDefined = True
        Dim transitions As LinkedList(Of ITransition(Of TSymbol, TState)) = New LinkedList(Of ITransition(Of TSymbol, TState))()

        Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                            "Analysis of Transitions:{0}", Environment.NewLine)
        For Each existingState As IState(Of TState) In Me.States

            For Each symbol As TSymbol In Me._symbols.Values

                Dim outcome As String
                Dim transtion As ITransition(Of TSymbol, TState) = Me.transitionTable.SelectTransition(existingState, symbol)
                If transtion.NextState Is Nothing Then

                    Me._isFullyDefined = False

                    outcome = "not found"

                    transitions.AddLast(transtion)

                Else

                    outcome = "found"

                End If

                Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                    "Transition from State '{1}' with symbol '{2}' {3}{0}", Environment.NewLine,
                                                    existingState, symbol, outcome)

            Next symbol

        Next existingState

        If Not Me._isFullyDefined Then

            Me._analysisReport.AppendLine("Following transitions are missing:")

            Dim node As LinkedListNode(Of ITransition(Of TSymbol, TState)) = transitions.First

            Do While node IsNot Nothing

                Dim existingTransition As ITransition(Of TSymbol, TState) = node.Value

                Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                    "Transition from State '{1}' with symbol '{2}' -> undefined{0}", Environment.NewLine,
                                                    existingTransition.State.Identity, existingTransition.Symbol)

                node = node.Next

            Loop

            If Me.IsDeterministic Then
                Me._analysisReport.AppendLine("State Engine is a partially-specified deterministic finite state Engine.")
            Else
                Me._analysisReport.Append(Me.transitionTable.AnalysisReport)
                Me._analysisReport.AppendLine("State Engine is a partially-specified non-deterministic finite state Engine.")
            End If

        Else

            If Me.IsDeterministic Then
                Me._analysisReport.AppendLine("State Engine is a fully-specified deterministic finite state Engine.")
            Else
                Me._analysisReport.Append(Me.transitionTable.AnalysisReport)
                Me._analysisReport.AppendLine("State Engine is a fully-specified non-deterministic finite state Engine.")
            End If

        End If

        Return Not Me._isFullyDefined.Value

    End Function

#End Region

End Class
