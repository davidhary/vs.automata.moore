Imports System.ComponentModel
''' <summary>
''' A base automaton class for handling finite automata sequencing with timeout.
''' </summary>
''' <remarks> David, 2009-04-26, 2.3.3403.x
''' Based on Moore Engine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreEngine.aspx <para>
''' An automaton is a mathematical model for a finite state Engine. A finite state Engine is a Engine that,
''' given an input of symbols, "jumps", or transitions, through a series of states according to a transition 
''' function (which can be expressed as a table). In the common "Mealy" variety of final state Engines, this transition function 
''' tells the automaton which state to go to next given a current state and a current input symbol. </para><para>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para></remarks>
<Description("Finite State Machine with Timeout"), DefaultEvent("SymbolAcknowledged")>
Public MustInherit Class TimeoutMachine(Of TSymbol, TState)
    Inherits Machine(Of TSymbol, TState)
    Implements ITimeoutMachine(Of TSymbol, TState)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>Constructs this class.</summary>
    Protected Sub New()
        MyBase.New("Timeout Machine")
    End Sub

#End Region

#Region " STATES "

    ''' <summary>
    ''' Adds a <see cref="State">state</see> to the <see cref="Machine(Of TSymbol, TState)">finite
    ''' state Machine</see>.
    ''' </summary>
    ''' <param name="identity">        Specifies the state identity. Must be unique among all states
    '''                                defined for this state machine. </param>
    ''' <param name="timeoutInterval"> Specifies the timeout interval for all state operations to
    '''                                complete. Specify a
    '''                                <see cref="F:System.Threading.Timeout.Infinite">infinite (-
    '''                                1)</see> or zero for an infinite timeout. </param>
    ''' <returns> An ITimeoutState(Of TState) </returns>
    Public Overloads Function Add(ByVal identity As TState, ByVal timeoutInterval As TimeSpan) As ITimeoutState(Of TState) Implements ITimeoutMachine(Of TSymbol, TState).Add
        Return Me.Add(New TimeoutState(Of TState)(identity, timeoutInterval))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:isr.Automata.IState`1">state</see> to the
    ''' <see cref="T:isr.Automata.IMachine`2">finite state Machine</see>.
    ''' </summary>
    ''' <param name="value"> Specifies the state. </param>
    ''' <returns> An IState(Of TState) </returns>
    Public Overloads Function Add(ByVal value As ITimeoutState(Of TState)) As ITimeoutState(Of TState) Implements ITimeoutMachine(Of TSymbol, TState).Add
        MyBase.Add(value)
        Return value
    End Function

#End Region

End Class

