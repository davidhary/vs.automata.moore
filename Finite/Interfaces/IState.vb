Imports System.Runtime.ExceptionServices

''' <summary>
''' Interface to a finite state machine state.
''' </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' created
''' </para></remarks>
Public Interface IState
    Inherits IDisposable

#Region " MEMBERS "

    ''' <summary>
    ''' Gets the state name.
    ''' </summary>
    ReadOnly Property Name() As String

    ''' <summary> Gets or sets the failed indicator, which is used by count out and timeout states. </summary>
    ''' <value> The failed. </value>
    ReadOnly Property Failed As Boolean

    ''' <summary> Executes the failed action. </summary>
    Sub OnFailed()

#End Region

#Region " EVENTS "

    ''' <summary> Gets the exception dispatched. </summary>
    ''' <value> The exception dispatched. </value>
    ReadOnly Property ExceptionsDispatched As Concurrent.BlockingCollection(Of ExceptionDispatchInfo)

    ''' <summary>Occurs before state activation (entry).</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Event Entering As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary>Raises the <see cref="Entering">Entering</see> event.</summary>
    Sub OnEntering(ByVal e As System.ComponentModel.CancelEventArgs)

    ''' <summary>Occurs upon state activation (entry).</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Event Enter As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the <see cref="Enter">Enter</see> event.</summary>
    Sub OnEnter(ByVal e As System.EventArgs)

    ''' <summary>Occurs before state deactivation (exit).</summary>
    ''' <remarks>Use this event to notify the container class that the state was deactivated.</remarks>
    Event Leaving As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary>Raises the <see cref="Leaving">Leaving</see> event.</summary>
    Sub OnLeaving(ByVal e As System.ComponentModel.CancelEventArgs)

    ''' <summary>Occurs upon state deactivation (exit).</summary>
    ''' <remarks>Use this event to notify the container class that the state was deactivated.</remarks>
    Event Leave As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the <see cref="Leave">Leave</see> event.</summary>
    Sub OnLeave(ByVal e As System.EventArgs)

    ''' <summary>Occurs before state is reentered.</summary>
    ''' <remarks>Use this event to notify the container class that the state was deactivated.</remarks>
    Event Reentering As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary>Raises the <see cref="Reentering">Reentering</see> event.</summary>
    Sub OnReentering(ByVal e As System.ComponentModel.CancelEventArgs)

    ''' <summary>Occurs upon state activation (reentry).</summary>
    ''' <remarks>Use this event to notify the container class that the state was reentered.</remarks>
    Event Reenter As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the <see cref="Reenter">Reenter</see> event.</summary>
    Sub OnReenter(ByVal e As System.EventArgs)

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Implements comparison with a generic object.
    ''' </summary>
    Function Equals(ByVal value As Object) As Boolean

    ''' <summary>
    ''' Returns the hash code for the state.
    ''' </summary>
    Function GetHashCode() As Integer

#End Region

End Interface


''' <summary>
''' Interface to a finite state machine state of type <typeparamref name="TState"/>.
''' </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' created
''' </para></remarks>
Public Interface IState(Of TState)
    Inherits IState

#Region " MEMBERS "

    ''' <summary>
    ''' Gets the state ID. 
    ''' </summary>
    ReadOnly Property Identity() As TState

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Implements comparison with another state.
    ''' </summary>
    Overloads Function Equals(ByVal value As IState(Of TState)) As Boolean

#End Region

End Interface

Public Class StateCollection(Of TState)
    Inherits Collections.ObjectModel.KeyedCollection(Of TState, IState(Of TState))
    Protected Overrides Function GetKeyForItem(item As IState(Of TState)) As TState
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.Identity
    End Function

    Public Sub DisposeStates()
        Dim q As New Queue(Of IState(Of TState))(Me)
        Do While q.Any
            Dim s As IState(Of TState) = q.Dequeue
            s.Dispose()
        Loop
        Me.Clear()
    End Sub

End Class

