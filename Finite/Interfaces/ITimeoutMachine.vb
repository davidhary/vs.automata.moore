''' <summary>
''' Interface to a finite state machine.
''' </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' created
''' </para></remarks>
Public Interface ITimeoutMachine(Of TSymbol, TState)
    Inherits IMachine(Of TSymbol, TState)
    Inherits IDisposable

#Region " STATES "

    ''' <summary>
    ''' Adds a <see cref="IState(Of TState)">state</see> to the <see cref="IMachine(Of TSymbol, TState)">finite state Machine</see>.
    ''' </summary>
    ''' <param name="identity">Specifies the state identity. Must be unique among all states defined
    ''' for this state machine.</param>
    ''' <param name="timeoutInterval">Specifies the timeout interval for all state operations to complete.
    ''' Specify a <see cref="Timespan.MaxValue">maximum</see> or <see cref="Timespan.Zero">zero</see> for an infinite timeout.</param>
    Overloads Function Add(ByVal identity As TState, ByVal timeoutInterval As TimeSpan) As ITimeoutState(Of TState)

    ''' <summary>
    ''' Adds a <see cref="IState(Of TState)">state</see> to the
    ''' <see cref="IMachine(Of TSymbol, TState)">finite state Machine</see>.
    ''' </summary>
    ''' <param name="value"> The value to add. </param>
    ''' <returns> An ITimeoutState(Of TState) </returns>
    Overloads Function Add(ByVal value As ITimeoutState(Of TState)) As ITimeoutState(Of TState)

#End Region

End Interface
