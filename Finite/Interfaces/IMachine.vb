Imports System.ComponentModel
Imports isr.Core

''' <summary> Interface for machine. </summary>
''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Interface IMachine
    Inherits IEngine

#Region " SIGNALS "

    ''' <summary>
    ''' Gets or sets the condition indicating that a Pause was requested.
    ''' </summary>
    ReadOnly Property IsPauseRequested() As Boolean

    ''' <summary>
    ''' Requests the state machine to Pause running after completion of the last action.
    ''' </summary>
    Sub RequestPause()

    ''' <summary>
    ''' Requests the state machine to resume after the pause.
    ''' </summary>
    Sub ResumePause()

    ''' <summary>
    ''' Gets or sets the condition indicating that a stop was requested.
    ''' </summary>
    ReadOnly Property IsStopRequested() As Boolean

    ''' <summary>
    ''' Requests the state machine to stop running after completion of the last action.
    ''' </summary>
    Sub RequestStop()

    ''' <summary> Query if this object is cancellation requested. </summary>
    ''' <value> <c>true</c> if cancellation requested; otherwise <c>false</c> </value>
    ReadOnly Property IsCancellationRequested() As Boolean

    ''' <summary>
    ''' Requests the state machine to immediately stop operation.
    ''' </summary>
    Sub RequestCancellation()

    ''' <summary> Enqueue stop symbol. </summary>
    Sub EnqueueStopSymbol()

#End Region

#Region " INPUT QUEUE "

    ''' <summary>
    ''' Clears the input queue. This is required in machines where both internal and external
    ''' might feed inputs.
    ''' </summary>
    Sub ClearSymbolQueue()

    ''' <summary>
    ''' Returns true if the state machine has an input in the queue.
    ''' </summary>
    Function HasInput() As Boolean

    ''' <summary> Gets or sets the name of the next input symbol. </summary>
    ''' <value> The name of the next input symbol. </value>
    ReadOnly Property NextSymbolName As String

    ''' <summary>
    ''' Process a single input if any.  This allows running the state machine 
    ''' based on signals alone.
    ''' Catches and reports failures if exception occurred.
    ''' </summary>
    ''' <returns>True if a new state</returns>
    Function TryProcessInput() As Boolean

#End Region

#Region " SEQUENCE "

    ''' <summary>Occurs upon stop of state execution.</summary>
    ''' <remarks>Use this event to notify the container class that the machine stopped.</remarks>
    Event Stopped As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the <see cref="Stopped">Stopped</see> event.</summary>
    Sub OnStopped(ByVal e As System.EventArgs)

    ''' <summary>
    ''' Starts the state machine by <see cref="Initiate">initiating</see> the start state and
    ''' <see cref="Sequence">repeating the sequence</see> until receiving a
    ''' <see cref="RequestStop">stop request.</see>
    ''' </summary>
    Sub StartSequence()

    ''' <summary>
    ''' Starts the state machine by <see cref="Initiate">
    ''' initiating</see>
    ''' the start state and
    ''' <see cref="Sequence">
    ''' repeating the sequence</see>
    ''' until receiving a
    ''' <see cref="RequestStop">
    ''' stop request.</see>
    ''' </summary>
    ''' <param name="onsetDelay"> Gets or sets the time to wait before starting the machine cycle. </param>
    ''' <param name="interval">   Specifies the state machine period. </param>
    Sub StartSequence(ByVal onsetDelay As TimeSpan, ByVal interval As TimeSpan)

    ''' <summary>
    ''' Specifies the state machine period.
    ''' </summary>
    Property Interval() As TimeSpan

    ''' <summary>
    ''' Runs the state machine sequence until receiving <see cref="RequestStop">stop request.</see>
    ''' The sequence will run only after a previous stop requested that leaves
    ''' <see cref="RequestStop">stop requested sentinel</see> turned on.
    ''' </summary>
    Sub Sequence()

    ''' <summary> Resumes operations after a stop. </summary>
    Sub ResumeSequence()

    ''' <summary> Gets the sentinel indicating if the machine is running in the background. </summary>
    ''' <returns> <c>true</c> if running; otherwise <c>false</c> </returns>
    ReadOnly Property IsRunning As Boolean

#End Region

#Region " TASK "

    ''' <summary> Gets the task. </summary>
    ''' <value> The task. </value>
    ReadOnly Property Task As Task

    ''' <summary> Activates the machine asynchronous task. </summary>
    ''' <remarks> 
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <returns> A Task. </returns>
    Function ActivateTask() As Task

    ''' <summary> Activates the machine asynchronous task. </summary>
    ''' <remarks> 
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="onsetDelay">  Gets or sets the time to wait before starting the machine cycle. </param>
    ''' <param name="interval">    Specifies the state machine period. </param>
    ''' <returns> A Task. </returns>
    Function ActivateTask(ByVal onsetDelay As TimeSpan, ByVal interval As TimeSpan) As Task

    ''' <summary> Resumes the machine asynchronous task. </summary>
    ''' <returns> A Task. </returns>
    Function ResumeTask() As Task

#End Region

#Region " THREADING "

    ''' <summary> Gets the machine thread. </summary>
    ''' <value> The machine thread. </value>
    ReadOnly Property MachineThread As Threading.Thread

    ''' <summary> Activates the machine asynchronous thread operations. </summary>
    ''' <remarks> 
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="onsetDelay">  Gets or sets the time to wait before starting the machine cycle. </param>
    ''' <param name="interval">    Specifies the state machine period. </param>
    Sub ActivateThread(ByVal onsetDelay As TimeSpan, ByVal interval As TimeSpan)

    ''' <summary> Activates the machine asynchronous thread operations. </summary>
    ''' <remarks> 
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    Sub ActivateThread()

    ''' <summary> Resumes the machine asynchronous thread operations. </summary>
    Sub ResumeThread()

#End Region

End Interface

''' <summary>
''' Interface to a finite state machine.
''' </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' created
''' </para></remarks>
Public Interface IMachine(Of TSymbol, TState)
    Inherits IMachine, IEngine(Of TSymbol, TState)

#Region " INPUT QUEUE "

    ''' <summary>
    ''' Enqueues an input symbol.
    ''' </summary>
    ''' <param name="value">The input symbol to add to the input queue.</param>
    Sub EnqueueInput(ByVal value As TSymbol)

    ''' <summary> Enqueues an input symbol. </summary>
    ''' <param name="value">      The input symbol to add to the input queue. </param>
    ''' <param name="clearFirst"> Clears the queue before enqueuing the value. </param>
    ''' <param name="ifEmpty">    If true, the signal is queued only if the queue is empty. </param>
    Sub EnqueueInput(ByVal value As TSymbol, ByVal clearFirst As Boolean, ByVal ifEmpty As Boolean)

    ''' <summary> Enqueues an input symbol. </summary>
    ''' <param name="value">      The input symbol to add to the input queue. </param>
    ''' <param name="clearFirst"> Clears the queue before enqueuing the value. </param>
    Sub EnqueueInput(ByVal value As TSymbol, ByVal clearFirst As Boolean)

    ''' <summary>
    ''' Enqueues an input symbol.
    ''' </summary>
    ''' <param name="values">The input symbols to add to the input queue.</param>
    Sub EnqueueInput(ByVal values As TSymbol())

    ''' <summary> Enqueues an input symbol. </summary>
    ''' <param name="values">     The input symbols to add to the input queue. </param>
    ''' <param name="clearFirst"> Clears the queue before enqueuing the value. </param>
    ''' <param name="ifEmpty">    If true, the signal is queued only if the queue is empty. </param>
    Sub EnqueueInput(ByVal values As TSymbol(), ByVal clearFirst As Boolean, ByVal ifEmpty As Boolean)

    ''' <summary>
    ''' Gets the input without removing it from the queue.
    ''' </summary>
    Function PeekInput() As TSymbol

#End Region

End Interface
