Imports System.ComponentModel
Imports isr.Core

''' <summary> Interface for Engine. </summary>
''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Interface IEngine
    Inherits IDisposable, INotifyPropertyChanged

#Region " EVENTS "

    ''' <summary>Occurs upon initiation of state Engine.</summary>
    ''' <remarks>Use this event to notify the container class that the Engine started.</remarks>
    Event Initiated As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the <see cref="Initiated">Started</see> event.</summary>
    Sub OnInitiated(ByVal e As System.EventArgs)

    ''' <summary>Occurs upon failure of state sequencing or an exception in state execution.</summary>
    ''' <remarks>Use this event to notify the container class that the Engine Failed.</remarks>
    Event Failed As EventHandler(Of System.IO.ErrorEventArgs)

    ''' <summary>Raises the <see cref="Failed">Failed</see> event.</summary>
    Sub OnFailed(ByVal e As System.IO.ErrorEventArgs)

    ''' <summary>Raises the <see cref="Failed">Failed</see> event.</summary>
    Sub OnFailed(ByVal exception As Exception)

    ''' <summary>Raises the <see cref="Failed">Failed</see> event.</summary>
    Sub OnFailed(ByVal exception As Exception, ByVal detailsFormat As String, ByVal ParamArray args() As Object)

#End Region

#Region " PROGRESS "

    ''' <summary> Gets or sets the number of consecutive failures. </summary>
    ''' <value> The number of consecutive failures. </value>
    Property ConsecutiveFailureCount As Integer

    ''' <summary> Gets or sets the number of consecutive failure aborts. </summary>
    ''' <value> The number of consecutive failure aborts. </value>
    Property ConsecutiveFailureAbortCount As Integer

    ''' <summary> Progress indication of the entire process. </summary>
    ''' <returns> An integer. </returns>
    Property PercentProgress() As Integer

    ''' <summary> Updates the percent progress described by value. </summary>
    Sub UpdatePercentProgress()

    ''' <summary> Progress indicator into the state sequence. </summary>
    ''' <returns> An Integer. </returns>
    Property ProgressIndicatorPercent() As Integer

    ''' <summary> Updates the progress indicator. </summary>
    Sub UpdateProgressIndicatorPercent()

    ''' <summary> Gets the Engine stopwatch. </summary>
    ''' <value> The Engine stopwatch. </value>
    ReadOnly Property Stopwatch As Stopwatch

    ''' <summary> Gets or sets the elapsed time since the state machine started. </summary>
    ''' <value> The elapsed. </value>
    Property Elapsed() As TimeSpan

    ''' <summary> Gets or sets the minimum elapsed time before the elapsed event handler is invoked. </summary>
    ''' <value> The minimum elapsed. </value>
    Property MinimumElapsed() As TimeSpan


#End Region

#Region " STATES "

    ' Sub NotifyStateChange()

    ''' <summary> Gets or sets the current state. </summary>
    ''' <value> The current state. </value>
    ReadOnly Property CurrentState As IState

    ''' <summary> Gets or sets the current state name. </summary>
    ''' <value> The name of the current state. </value>
    ReadOnly Property CurrentStateName As String

    ''' <summary> Gets or sets the state of the previous. </summary>
    ''' <value> The previous state. </value>
    ReadOnly Property PreviousState As IState

    ''' <summary> Gets or sets the name of the previous state. </summary>
    ''' <value> The name of the previous state. </value>
    ReadOnly Property PreviousStateName As String

    ''' <summary> Query if this engine started; the engine started if its state is not the null state. </summary>
    ''' <returns> <c>true</c> if started; otherwise <c>false</c> </returns>
    Function IsStarted() As Boolean

    ''' <summary> Query if this engine is at the initial state. </summary>
    ''' <returns> <c>true</c> if initial; otherwise <c>false</c> </returns>
    Function IsInitial() As Boolean

    ''' <summary> Query if this engine is at the terminal state. </summary>
    ''' <returns> <c>true</c> if terminal; otherwise <c>false</c> </returns>
    Function IsTerminal() As Boolean

    ''' <summary> Query if this engine can be moved to the terminal State. </summary>
    ''' <returns> <c>true</c> if terminable; otherwise <c>false</c> </returns>
    Function IsTerminable() As Boolean

    ''' <summary> Indicates is the engine is busy. That is it entered the busy sequence and was not stopped or idle. </summary>
    ''' <remarks> This is set by each state machine entering its busy sequence and cleared when existing the busy sequence. </remarks>
    ''' <value> <c>true</c> if busy; otherwise <c>false</c> </value>
    Property IsBusy As Boolean

#End Region

#Region " INITIATE / TERMINATE "

    ''' <summary>
    ''' Gets or sets the time in milliseconds to wait before starting the Engine cycle.
    ''' </summary>
    Property OnsetDelay() As TimeSpan

    ''' <summary>
    ''' Moves the state Engine to the Start State.
    ''' </summary>
    ''' <param name="onsetDelay"> Gets or sets the time to wait before starting the Engine cycle. </param>
    Sub Initiate(ByVal onsetDelay As TimeSpan)

    ''' <summary> Moves the state Engine to the Start State. </summary>
    Sub Initiate()

    ''' <summary> Send to the terminal state. </summary>
    ''' <returns> <c>true</c> if action taken place; otherwise, e.g., if already in the terminal state, <c>false</c> </returns>
    Function Terminate() As Boolean

#End Region

#Region " PROCESS INPUT "

    ''' <summary> Symbol names. </summary>
    ''' <returns> . </returns>
    Function InputSymbolNames() As IEnumerable(Of String)

    ''' <summary> Gets or sets the automatic reenter enabled sentinel. </summary>
    ''' <value> <c>True</c> to reenter the current state if no input is provided; Otherwise, <c>False</c>. </value>
    Property AutoReenterEnabled As Boolean

    ''' <summary> Gets or sets the name of the last input symbol. </summary>
    ''' <value> The name of the last input symbol. </value>
    Property LastInputSymbolName As String

    ''' <summary> Gets or sets the failure details. </summary>
    ''' <value> The failure details. </value>
    Property FailureDetails As String

    ''' <summary> Gets or sets the status of the failure. </summary>
    ''' <value> The failure state. </value>
    Property FailureStatus As FailureStatus

#End Region

#Region " CONSTRUCTION and ANALYSIS "

    ''' <summary> Gets the Engine name. </summary>
    ''' <value> The Engine name. </value>
    ReadOnly Property Name As String

    ''' <summary>
    ''' Returns the analysis report.
    ''' </summary>
    ReadOnly Property AnalysisReport() As String

    ''' <summary>
    ''' Returns true of the state Engine is fully defined.
    ''' The Engine is fully defined if all states have an associated transition.
    ''' </summary>
    ReadOnly Property IsFullyDefined() As Boolean?

    ''' <summary>
    ''' Returns true if the state Engine is deterministic, i.e., if each state and symbol have a unique
    ''' next state. Namely, if all transitions are unique.
    ''' </summary>
    ReadOnly Property IsDeterministic() As Boolean

    ''' <summary>
    ''' Analyzes the state Engine to determine if it is fully specified.
    ''' </summary>
    ''' <returns>
    ''' True if the Engine is fully specified and all signals and states are unique.
    ''' </returns>
    Function Analyze() As Boolean

#End Region

End Interface

''' <summary>
''' Interface to a finite state Engine.
''' </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' created
''' </para></remarks>
Public Interface IEngine(Of TSymbol, TState)
    Inherits IEngine

#Region " EVENTS "

    ''' <summary>Occurs upon Engine acknowledging receipt of a new input symbol.</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Event SymbolAcknowledged As EventHandler(Of SymbolEventArgs(Of TSymbol))

    ''' <summary>Raises the <see cref="SymbolAcknowledged">Acknowledge</see> event.</summary>
    Sub OnSymbolAcknowledge(ByVal e As SymbolEventArgs(Of TSymbol))

#End Region

#Region " STATES "

    ''' <summary> Gets or sets the null state identity. </summary>
    ''' <value> The null state identity. </value>
    ReadOnly Property NullStateIdentity As TState

    ''' <summary> Gets or sets the initial state identity. </summary>
    ''' <value> The initial state identity. </value>
    ReadOnly Property InitialStateIdentity As TState

    ''' <summary> Gets or sets the terminal state identity. </summary>
    ''' <value> The terminal state identity. </value>
    ReadOnly Property TerminalStateIdentity As TState

    ''' <summary>
    ''' Gets reference to the active <see cref="IState">state</see>.
    ''' </summary>
    Overloads ReadOnly Property CurrentState() As IState(Of TState)

    ''' <summary> Gets or sets the current state identity. </summary>
    ''' <value> The current state identity. </value>
    ReadOnly Property CurrentStateIdentity As TState

    ''' <summary>
    ''' Gets reference to the previous active <see cref="IState">state</see>.
    ''' </summary>
    Overloads Property PreviousState() As IState(Of TState)

    ReadOnly Property PreviousStateIdentity As TState

    ''' <summary> Enumerates Engine states in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process Engine states in this collection.
    ''' </returns>
    Function EngineStates() As IEnumerable(Of IState(Of TState))

    ''' <summary>
    ''' Sets the start state to a state other than the first state that was added.
    ''' </summary>
    Sub StartStateSetter(ByVal value As IState(Of TState))

    ''' <summary>
    ''' Adds a <see cref="IState(Of TState)">state</see> to the
    ''' <see cref="IEngine(Of TSymbol, TState)">finite state Engine</see>.
    ''' </summary>
    ''' <param name="identity"> Specifies the state identity. Must be unique among all states defined
    '''                         for this state Engine. </param>
    ''' <returns> An IState(Of TState) </returns>
    Function Add(ByVal identity As TState) As IState(Of TState)

    ''' <summary>
    ''' Adds a <see cref="IState(Of TState)">state</see> to the
    ''' <see cref="IEngine(Of TSymbol, TState)">finite state Engine</see>.
    ''' </summary>
    ''' <param name="value"> Specifies the symbol value. </param>
    ''' <returns> An IState(Of TState) </returns>
    Function Add(ByVal value As IState(Of TState)) As IState(Of TState)

#End Region

#Region " PROCESS INPUT "

    ''' <summary> Gets or sets the last input symbol. </summary>
    ''' <value> The last input symbol. </value>
    Property LastInputSymbol As TSymbol

    ''' <summary> Gets or sets the Fail input symbol. </summary>
    ''' <value> The stop symbol. </value>
    ReadOnly Property FailInputSymbol As TSymbol


    ''' <summary> Gets or sets the initial input symbol. </summary>
    ''' <value> The stop symbol. </value>
    ReadOnly Property InitialInputSymbol As TSymbol

    ''' <summary> Gets or sets the terminal input symbol. </summary>
    ''' <value> The stop symbol. </value>
    ReadOnly Property TerminalInputSymbol As TSymbol

    ''' <summary>
    ''' Immediately processes a single input ignoring the signal symbol queue. This allows running the state
    ''' Engine based on signals alone. Catches exceptions and reports in the references
    ''' <paramref name="e"/> if exception occurred.
    ''' </summary>
    ''' <param name="input"> Specifies the input to process. </param>
    ''' <param name="e">     Cancel details event information. </param>
    ''' <returns>
    ''' True if state change was implement or false if the transition is invalid of the change was
    ''' Canceled.
    ''' </returns>
    Overloads Function TryProcessInput(ByVal input As TSymbol, ByVal e As ActionEventArgs) As Boolean

    ''' <summary>
    ''' Immediately processes a series of inputs input ignoring the signal symbol queue. This allows running
    ''' the state Engine based on signals alone. Catches exceptions and reports in the references
    ''' <paramref name="e"/> if exception occurred.
    ''' </summary>
    ''' <remarks> David, 2009-04-26, 2.3.3403.x
    ''' Allows makings a transition into the same state in which case the
    ''' <see cref="IState.Leave">Leave</see> and <see cref="IState.Enter">Enter</see> events
    ''' do not occur. Use the <see cref="IState(Of TState).Reenter">Reenter</see> event to process
    ''' state actions. </remarks>
    ''' <param name="inputs"> Specifies the inputs to process. </param>
    ''' <param name="e">      Cancel details event information. </param>
    ''' <returns>
    ''' True if state change was implement or false if the transition is invalid of the change was
    ''' Canceled.
    ''' </returns>
    Overloads Function TryProcessInput(ByVal inputs As TSymbol(), ByVal e As ActionEventArgs) As Boolean

    ''' <summary> Process the signal symbol. </summary>
    ''' <param name="input"> Specifies the signal symbol to process. </param>
    ''' <param name="e">     Cancel details event information. </param>
    Sub ProcessInput(ByVal input As TSymbol, ByVal e As ActionEventArgs)

    ''' <summary> Process the input described by signal symbol. </summary>
    ''' <param name="input"> The signal symbol. </param>
    ''' <returns> The isr.Core.ActionEventArgs. </returns>
    Overloads Function TryProcessInput(ByVal input As TSymbol) As isr.Core.ActionEventArgs

    ''' <summary> Process the input described by the signal symbol. </summary>
    ''' <param name="input"> The signal symbol. </param>
    Sub ProcessInput(ByVal input As TSymbol)

    ''' <summary> Process the input; send to failure state if failed. </summary>
    ''' <param name="input"> The input. </param>
    ''' <returns> The isr.Core.ActionEventArgs. </returns>
    Function ProcessInputOrFail(ByVal input As TSymbol) As isr.Core.ActionEventArgs

#End Region

#Region " CONSTRACTION "

    ''' <summary>
    ''' Adds an input symbol.
    ''' The symbol must have a unique hash code.
    ''' </summary>
    ''' <param name="value">Specifies the symbol value</param>
    Function Add(ByVal value As TSymbol) As TSymbol

    ''' <summary>
    ''' Adds a transition to the state Engine.
    ''' </summary>
    Function Add(ByVal state As IState(Of TState), ByVal symbol As TSymbol,
                 ByVal nextState As IState(Of TState)) As ITransition(Of TSymbol, TState)

    ''' <summary>
    ''' Checks if the specified <typeparamref name="TSymbol">symbol</typeparamref> signals a valid
    ''' transition from the current state.
    ''' </summary>
    ''' <param name="value"> The inputs <typeparamref name="TSymbol">symbol</typeparamref> which
    '''                      requires validation. </param>
    ''' <param name="e">     Cancel details event information. </param>
    ''' <returns>
    ''' True if the symbol leads to a valid transition or False if no transition from the current
    ''' state is defined for the current input symbol.
    ''' </returns>
    Function TryValidateInput(ByVal value As TSymbol, ByVal e As ActionEventArgs) As Boolean

    ''' <summary>
    ''' Checks for the validity of all the transitions implied by the specified symbols.
    ''' </summary>
    ''' <param name="values"> The inputs <typeparamref name="TSymbol">symbols</typeparamref> which
    '''                       requires validation. </param>
    ''' <param name="e">      Cancel details event information. </param>
    ''' <returns>
    ''' Returns True if all implied transitions are valid. Returns false after detecting the first
    ''' invalid implied transition.
    ''' </returns>
    Function TryValidateAllInputs(ByVal values As TSymbol(), ByVal e As ActionEventArgs) As Boolean

    ''' <summary>
    ''' Checks for the validity of one of the transitions implied by the specified symbols.
    ''' </summary>
    ''' <param name="values"> The inputs <typeparamref name="TSymbol">symbols</typeparamref> which
    '''                       requires validation. </param>
    ''' <param name="e">      Cancel details event information. </param>
    ''' <returns>
    ''' Returns True if all implied transitions are valid. Returns false after detecting the first
    ''' invalid implied transition.
    ''' </returns>
    Function TryValidateInput(ByVal values As TSymbol(), ByVal e As ActionEventArgs) As Boolean

#End Region

End Interface

''' <summary> Values that represent failure status. </summary>
Public Enum FailureStatus
    <Description("No Error")> None
    <Description("Sequence Error")> SequenceError
    <Description("Sequence Exception")> SequenceException
End Enum
