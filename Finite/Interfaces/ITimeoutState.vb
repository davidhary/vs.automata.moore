''' <summary>
''' Interface to a finite state machine state with timeout.
''' </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-04-26, 2.3.3403.x">
''' created
''' </para></remarks>
Public Interface ITimeoutState(Of TState)
    Inherits IState(Of TState)

#Region " MEMBERS "

    ''' <summary>
    ''' Gets the sentinel indicating if the state hit timeout. 
    ''' </summary>
    ReadOnly Property IsTimeout() As Boolean

    ''' <summary>
    ''' Gets or sets the state timeout interval. 
    ''' </summary>
    Property TimeoutInterval() As TimeSpan

#End Region

#Region " EVENTS "

    ''' <summary>Occurs if state operations failed to complete before the timeout timer
    ''' fired.</summary>
    ''' <remarks>Use this event to process timeout operations.</remarks>
    Event Timeout As EventHandler(Of System.EventArgs)

    ''' <summary>
    ''' Handles timeout events.
    ''' </summary>
    Sub OnTimeout(ByVal e As System.EventArgs)

#End Region

End Interface
