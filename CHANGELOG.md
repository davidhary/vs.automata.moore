# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [5.2.6624] - 2018-02-19
* Fixes adding dispatched exception to the blocking collection.

*5.2.6319 04/20/17*  
Move Dashboards to the Dashboard library and machines
to the Machines library.

*5.1.6101 09/14/16*  
Adds dashboard. Splits state interface into two for
using with the Dashboard.

*5.0.6086 08/30/16*  
Replaces the Act event with the Reenter event. Changes
interval and onset delay to time span. Updates the interface to include
task and thread start.

*4.0.5956 04/22/16*  
Removes the Moore name space.

*4.0.5955 04/21/16*  
Added transition events for the state.

*4.0.5950 04/16/16*  
Built from Moore automata.

\(C\) 2006 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
