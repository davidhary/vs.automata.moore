## ISR Automata Moore<sub>&trade;</sub>: Moore State Machine Automata Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*4.1.6667 04/03/18*  
2018 release.

*4.0.5950 04/16/16*  
Uses .NET 4.6.2.

*3.1.5153 02/19/14*  
Uses local safe event handlers.

*3.1.4816 03/09/13*  
Adapts properties to VS10. Removes superfluous
underline characters.

*3.1.4710 11/23/12*  
Removes VB reference.

*3.1.4507 05/04/12*  
Adds x86 project.

*3.1.4498 04/17/12*  
Implements code analysis rules for .NET 4.

*3.1.4347 11/26/11*  
Converts to VS 2010.

*3.0.4232 08/03/11*  
Standardize code elements and documentation.

*3.0.4213 07/15/11*  
Fixes parsing of command line. Simplifies the assembly
information.

*3.0.4212 07/14/11*  
Removed version control keywords.

*3.0.4027 01/10/11*  
Makes transition disposable. Removes all handlers as
part of the object dispose. Disposes of all transitions when disposing
the transition function. Uses implicit targets for invoking events.

*3.0.4022 01/05/11*  
Stops the machine when disposing it. Uses safe
triggers, which prevents a deadlock if event handlers are removed before
disposing the parent of the state machine.

*3.0.3910 09/15/10*  
Allows processing multiple inputs. Allows repeat of
the action event. Sync locks inputs.

*3.0.3901 09/06/10*  
Rename state execution actions to: Initiate, Sequence
and Activate(=initiate + sequence).

*3.0.3882 08/18/10*  
Uses Cancel Event Arguments to allow abortion of state
transitions.

*3.0.3411 05/04/06*  
Adds a timeout timer to the states to monitor state
completion. Adds a base machine for start, restart, work, and stop
constructs.

*3.0.3403 04/26/06*  
Created from ISR Automata. Adds new implementation of
Moore automata.

\(C\) 2006 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Moore State Machine in C\#](http://www.codeproject.com/KB/recipes/MooreMachine.aspx)
