﻿Imports System.ComponentModel
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Friend NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        Public Const AssemblyTitle As String = "Moore Automata Library"
        Public Const AssemblyDescription As String = "Moore Automata Library"
        Public Const AssemblyProduct As String = "Moore.Automata"

    End Class

End Namespace


