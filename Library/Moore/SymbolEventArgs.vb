''' <summary>
''' Defines event arguments accepting the generic symbol for the state machine.
''' </summary>
''' <remarks>
''' http://www.CodeProject.com/KB/cs/GenericEventArgs.aspx<para>
''' Licensed under The MIT License. </para><para>  
''' David, 05/04/2009" by="Stewart and StageSoft" revision="1.2.3411.x">
''' created
''' </para></remarks>
Public Class SymbolEventArgs(Of TSymbol)
    Inherits EventArgs

    Public Sub New(ByVal value As TSymbol)
        MyBase.New()
        Me._Symbol = value
    End Sub

    ''' <summary>
    ''' Gets or sets the symbol.
    ''' </summary>
    ''' <value>The symbol.</value>
    Public Property Symbol() As TSymbol

End Class

