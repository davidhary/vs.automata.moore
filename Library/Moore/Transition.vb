Imports System.ComponentModel
Namespace Moore

    ''' <summary>
    ''' Defines a transition between two states.
    ''' </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/26/09, 2.3.3403.x">
    ''' based on Moore machine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreMachine.aspx
    ''' Applies to expansions from the original.
    ''' </para></remarks>
    <Description("Finite State Machine Transition"), DefaultEvent("Starting")>
    Public Class Transition(Of TSymbol)
        Implements ITransition(Of TSymbol)

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary>
        ''' Constructs a <see cref="Transition(Of TSymbol)">transition</see>
        ''' </summary>
        ''' <param name="state">Specifies the current state.</param>
        ''' <param name="symbol">Specifies the input symbol to move to the <paramref name="nextState">next state</paramref></param>
        ''' <param name="nextState">Specifies the next state</param>
        Public Sub New(ByVal state As IState, ByVal symbol As TSymbol, ByVal nextState As IState)
            Me._state = state
            Me._symbol = symbol
            Me._nextState = nextState
            eventLocker = New Object
        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method Overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    If Me.StartingEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.StartingEvent?.GetInvocationList
                            RemoveHandler Me.Starting, CType(d, Global.System.EventHandler(Of Global.System.ComponentModel.CancelEventArgs))
                        Next
                    End If
                    If Me.CompletedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.CompletedEvent?.GetInvocationList
                            RemoveHandler Me.Completed, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If
                    SyncLock eventLocker
                    End SyncLock
                End If
            Finally
                Me.IsDisposed = True
            End Try
        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        End Sub

#End Region

#Region " MEMBERS "

        Private _State As IState
        ''' <summary>
        ''' Gets or sets the current state for this  <see cref="Transition(Of TSymbol)">transition</see> 
        ''' </summary>
        Public ReadOnly Property State() As IState Implements ITransition(Of TSymbol).State
            Get
                Return Me._state
            End Get
        End Property

        Private _Symbol As TSymbol
        ''' <summary>
        ''' Gets or sets the symbol for transitioning to the next state.
        ''' </summary>
        Public ReadOnly Property Symbol() As TSymbol Implements ITransition(Of TSymbol).Symbol
            Get
                Return Me._symbol
            End Get
        End Property

        Private _NextState As IState
        ''' <summary>
        ''' Gets or sets the reference to the next state identified by this <see cref="Transition(Of TSymbol)">transition</see>
        ''' </summary>
        Public ReadOnly Property NextState() As IState Implements ITransition(Of TSymbol).NextState
            Get
                Return Me._nextState
            End Get
        End Property

#End Region

#Region " EVENTS "

        Private eventLocker As Object
        ''' <summary>Raises the Starting event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnStarting(ByVal e As System.ComponentModel.CancelEventArgs) Implements ITransition(Of TSymbol).OnStarting
            SyncLock eventLocker
                SafeEventHandlers.SafeBeginEndInvoke(Me.StartingEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs before the state transition.</summary>
        ''' <remarks>Use this event to notify the container class that the state actions are taking place.</remarks>
        Public Event Starting As EventHandler(Of System.ComponentModel.CancelEventArgs) Implements ITransition(Of TSymbol).Starting

        ''' <summary>Raises the Completed event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnCompleted(ByVal e As System.EventArgs) Implements ITransition(Of TSymbol).OnCompleted
            SyncLock eventLocker
                SafeEventHandlers.SafeBeginEndInvoke(Me.CompletedEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs after the state transition.</summary>
        ''' <remarks>Use this event to notify the container class that the state actions are taking place.</remarks>
        Public Event Completed As EventHandler(Of System.EventArgs) Implements ITransition(Of TSymbol).Completed

#End Region

#Region " EQUALS "

        ''' <summary>
        ''' Compares the <see cref="Transition(Of TSymbol)">transition</see> to a generic object.
        ''' </summary>
        Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean Implements ITransition(Of TSymbol).Equals
            Return Me.Equals(TryCast(obj, Transition(Of TSymbol)))
        End Function

        ''' <summary>
        ''' Compares to another <see cref="Transition(Of TSymbol)">transition</see>.
        ''' Transitions are compared using only the current state and symbol as 
        ''' this state machine must be deterministic.
        ''' </summary>
        Public Overloads Function Equals(ByVal value As ITransition(Of TSymbol)) As Boolean Implements ITransition(Of TSymbol).Equals

            Return value IsNot Nothing AndAlso
                   Me._symbol.GetHashCode().Equals(value.Symbol.GetHashCode) AndAlso
                   Me._state.GetHashCode().Equals(value.State.GetHashCode)

        End Function

        ''' <summary>
        ''' Returns a unique hash code for this transition.
        ''' </summary>
        Public Overrides Function GetHashCode() As Integer Implements ITransition(Of TSymbol).GetHashCode
            Return Me._symbol.GetHashCode() Xor Me._state.GetHashCode()
        End Function

        ''' <summary>Returns True if equal.</summary>
        ''' <param name="left">The left hand side item to compare for equality</param>
        ''' <param name="right">The left hand side item to compare for equality</param>
        Public Shared Operator =(ByVal left As Transition(Of TSymbol), ByVal right As Transition(Of TSymbol)) As Boolean
            If left Is Nothing Then
                Return right Is Nothing
            ElseIf right Is Nothing Then
                Return False
            Else
                Return left.Equals(right)
            End If
        End Operator

        ''' <summary>Returns True if not equal.</summary>
        ''' <param name="left">The left hand side item to compare for equality</param>
        ''' <param name="right">The left hand side item to compare for equality</param>
        Public Shared Operator <>(ByVal left As Transition(Of TSymbol), ByVal right As Transition(Of TSymbol)) As Boolean
            If left Is Nothing Then
                Return right IsNot Nothing
            ElseIf right Is Nothing Then
                Return True
            Else
                Return Not left.Equals(right)
            End If
        End Operator

#End Region

    End Class

End Namespace
