Imports System.Collections.Generic
Imports System.Threading
Imports System.ComponentModel

Namespace Moore

    ''' <summary>
    ''' A base automaton class for handling finite automata sequencing.
    ''' </summary>
    ''' <remarks>
    ''' An automaton is a mathematical model for a finite state machine. A finite state machine is a machine that,
    ''' given an input of symbols, "jumps", or transitions, through a series of states according to a transition 
    ''' function (which can be expressed as a table). In the common "Mealy" variety of final state machines, this transition function 
    ''' tells the automaton which state to go to next given a current state and a current input symbol.
    ''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/26/09, 2.3.3403.x">
    ''' based on Moore machine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreMachine.aspx
    ''' </para></remarks>
    <Description("Moore Finite State Machine"), DefaultEvent("Acknowledge")>
    Public Class Machine(Of TSymbol)
        Implements IMachine(Of TSymbol)

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            MyBase.New()
            Me._LastActionMessage = String.Empty
            Me._symbols = New Collections.Generic.Dictionary(Of Integer, TSymbol)
            Me._states = New Collections.Generic.Dictionary(Of String, IState)()
            Me._transitionTable = New TransitionFunction(Of TSymbol)
            Me._OnsetDelay = 0
            Me._Interval = 0
            Me._inputSymbolQueue = New Queue(Of TSymbol)
            executeLocker = New Object

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method Overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
            <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="_startState", Justification:="The states are disposed as art of the collection")>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="_currentState", Justification:="The states are disposed as art of the collection")>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="_previousState", Justification:="The states are disposed as art of the collection")>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    If Me.AcknowledgeEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.AcknowledgeEvent?.GetInvocationList
                            RemoveHandler Me.Acknowledge, CType(d, Global.System.EventHandler(Of SymbolEventArgs(Of TSymbol)))
                        Next
                    End If
                    If Me.CountOutEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.CountOutEvent?.GetInvocationList
                            RemoveHandler Me.CountOut, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If
                    If Me.CreatedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.CreatedEvent?.GetInvocationList
                            RemoveHandler Me.Created, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If
                    If Me.StartedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.StartedEvent?.GetInvocationList
                            RemoveHandler Me.Started, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If
                    If Me.StateChangedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.StateChangedEvent?.GetInvocationList
                            RemoveHandler Me.StateChanged, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If
                    If Me.StoppedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.StoppedEvent?.GetInvocationList
                            RemoveHandler Me.Stopped, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If
                    Me.RequestStop()
                    SyncLock executeLocker

                        ' terminate the references
                        Me._startState = Nothing
                        Me._currentState = Nothing
                        Me._previousState = Nothing

                        For Each state As IState In Me._states?.Values
                            state.Dispose()
                            state = Nothing
                        Next
                        Me._states?.Clear() : Me._states = Nothing
                        Me._symbols?.Clear() : Me._symbols = Nothing
                        If Me._transitionTable IsNot Nothing Then Me._transitionTable.Dispose() : Me._transitionTable = Nothing

                    End SyncLock
                End If
            Finally
                Me.IsDisposed = True
            End Try
        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        End Sub

#End Region

#Region " INPUT MANAGEMENT "

        ''' <summary>
        ''' Clears the input queue. This is required in machines where both internal and external
        ''' might feed inputs.
        ''' </summary>
        Protected Sub ClearInputs() Implements IMachine(Of TSymbol).ClearInputs
            Me.inputSymbolQueue.Clear()
        End Sub

        Private _InputSymbolQueue As Queue(Of TSymbol)
        ''' <summary>
        ''' Gets reference to the input symbol queue.
        ''' </summary>
        Private ReadOnly Property InputSymbolQueue() As Queue(Of TSymbol)
            Get
                Return Me._inputSymbolQueue
            End Get
        End Property

        ''' <summary>
        ''' Retrieves the input.
        ''' </summary>
        Protected Function DequeueInput() As TSymbol
            SyncLock Me.inputSymbolQueue
                Return Me.inputSymbolQueue.Dequeue
            End SyncLock
        End Function

        ''' <summary>
        ''' Enqueues an input symbol.
        ''' </summary>
        ''' <param name="value">The input symbol to add to the input queue.</param>
        Public Sub EnqueueInput(ByVal value As TSymbol) Implements IMachine(Of TSymbol).EnqueueInput
            SyncLock Me.inputSymbolQueue
                Me.inputSymbolQueue.Enqueue(value)
            End SyncLock
        End Sub

        ''' <summary>
        ''' Enqueues an input symbol.
        ''' </summary>
        ''' <param name="value">The input symbol to add to the input queue.</param>
        ''' <param name="clearFirst">Clears the queue before enqueuing the value</param>
        ''' <param name="ifEmpty">If true, the signal is queued only if the queue is empty.</param>
        Public Sub EnqueueInput(ByVal value As TSymbol, ByVal clearFirst As Boolean, ByVal ifEmpty As Boolean) Implements IMachine(Of TSymbol).EnqueueInput
            SyncLock Me.inputSymbolQueue
                If clearFirst AndAlso Me.inputSymbolQueue.Count > 0 Then
                    Me.inputSymbolQueue.Clear()
                End If
                If Not (ifEmpty AndAlso Me.inputSymbolQueue.Count > 0) Then
                    Me.inputSymbolQueue.Enqueue(value)
                End If
            End SyncLock
        End Sub

        ''' <summary>
        ''' Enqueues an input only if the queue is empty. This allows a state to request a re-entry.
        ''' The queue is locked while it is enqueued.
        ''' </summary>
        ''' <param name="values">The input symbols to add to the input queue.</param>
        Public Sub EnqueueInput(ByVal values As TSymbol()) Implements IMachine(Of TSymbol).EnqueueInput
            If values IsNot Nothing AndAlso values.Length > 0 Then
                SyncLock Me.inputSymbolQueue
                    For Each value As TSymbol In values
                        Me.inputSymbolQueue.Enqueue(value)
                    Next
                End SyncLock
            End If
        End Sub

        ''' <summary>
        ''' Enqueues an input only if the queue is empty. This allows a state to request a re-entry.
        ''' The queue is locked while it is enqueued.
        ''' </summary>
        ''' <param name="values">The input symbols to add to the input queue.</param>
        ''' <param name="clearFirst">Clears the queue before enqueuing the value</param>
        ''' <param name="ifEmpty">If true, the signal is queued only if the queue is empty.</param>
        Public Sub EnqueueInput(ByVal values As TSymbol(), ByVal clearFirst As Boolean, ByVal ifEmpty As Boolean) Implements IMachine(Of TSymbol).EnqueueInput
            If values IsNot Nothing AndAlso values.Length > 0 Then
                SyncLock Me.inputSymbolQueue
                    If clearFirst AndAlso Me.inputSymbolQueue.Count > 0 Then
                        Me.inputSymbolQueue.Clear()
                    End If
                    If Not (ifEmpty AndAlso Me.inputSymbolQueue.Count > 0) Then
                        For Each value As TSymbol In values
                            Me.inputSymbolQueue.Enqueue(value)
                        Next
                    End If
                End SyncLock
            End If
        End Sub

        ''' <summary>
        ''' Returns true if the state machine received an input.
        ''' </summary>
        Public Function HasInput() As Boolean Implements IMachine(Of TSymbol).HasInput
            SyncLock Me.inputSymbolQueue
                Return Me.inputSymbolQueue.Count > 0
            End SyncLock
        End Function

        ''' <summary>
        ''' Gets the input without removing it from the queue.
        ''' </summary>
        Public Function PeekInput() As TSymbol Implements IMachine(Of TSymbol).PeekInput
            SyncLock Me.inputSymbolQueue
                If Me.inputSymbolQueue.Count > 0 Then
                    Return Me.inputSymbolQueue.Peek
                Else
                    Return Nothing
                End If
            End SyncLock
        End Function

#End Region

#Region " EVENTS: MANAGERS "

        ''' <summary>
        ''' Used to lock execution for synchronization.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification:="<Pending>")>
        Private executeLocker As Object

#End Region

#Region " EVENTS: ACKNOWLEDGE "

        ''' <summary> Raises the Acknowledge event. </summary>
        ''' <param name="e"> Specifies the <see cref="System.EventArgs">system event arguments</see> </param>
        Protected Overridable Sub OnAcknowledge(ByVal e As SymbolEventArgs(Of TSymbol)) Implements IMachine(Of TSymbol).OnAcknowledge
            SafeEventHandlers.SafeBeginEndInvoke(Me.AcknowledgeEvent, Me, e)
        End Sub

        ''' <summary>Occurs upon machine acknowledging receipt of a new input.</summary>
        ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
        Public Event Acknowledge As EventHandler(Of SymbolEventArgs(Of TSymbol)) Implements IMachine(Of TSymbol).Acknowledge

#End Region

#Region " EVENTS: COUNT OUT "

        ''' <summary>Raises the CountOut event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnCountOut(ByVal e As System.EventArgs) Implements IMachine(Of TSymbol).OnCountOut
            Me._IsCountOut = True
            SafeEventHandlers.SafeBeginEndInvoke(Me.CountOutEvent, Me, e)
        End Sub

        ''' <summary>Occurs if the current state actions count exceeded the 
        ''' <see cref="istate.CountOutCount">count out count</see>. When detected, the 
        ''' state machine does not execute the state actions after entering the state.
        ''' </summary>
        ''' <remarks>Use this event to process Count out operations.</remarks>
        Public Event CountOut As EventHandler(Of System.EventArgs) Implements IMachine(Of TSymbol).CountOut

#End Region

#Region " EVENTS: CREATED "

        ''' <summary>Raises the Created event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnCreated(ByVal e As System.EventArgs) Implements IMachine(Of TSymbol).OnCreated
            SafeEventHandlers.SafeBeginEndInvoke(Me.CreatedEvent, Me, e)
        End Sub

        ''' <summary>Occurs after the creation of state machine.</summary>
        ''' <remarks>Use this event to notify the container class that the state machine was created.</remarks>
        Public Event Created As EventHandler(Of System.EventArgs) Implements IMachine(Of TSymbol).Created

#End Region

#Region " EVENTS: STARTED "

        ''' <summary>Raises the Started event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnStarted(ByVal e As System.EventArgs) Implements IMachine(Of TSymbol).OnStarted
            SafeEventHandlers.SafeBeginEndInvoke(Me.StartedEvent, Me, e)
        End Sub

        ''' <summary>Occurs upon state activation (entry).</summary>
        ''' <remarks>Use this event to notify the container class that the machine started.</remarks>
        Public Event Started As EventHandler(Of System.EventArgs) Implements IMachine(Of TSymbol).Started

#End Region

#Region " EVENTS: STATE CHANGED "

        ''' <summary>Raises the StateChanged event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnStateChanged(ByVal e As System.EventArgs) Implements IMachine(Of TSymbol).OnStateChanged
            SafeEventHandlers.SafeBeginEndInvoke(Me.StateChangedEvent, Me, e)
        End Sub

        ''' <summary>Occurs upon state deactivation (exit).</summary>
        ''' <remarks>Use this event to notify the container class that the state was deactivated.</remarks>
        Public Event StateChanged As EventHandler(Of System.EventArgs) Implements IMachine(Of TSymbol).StateChanged

#End Region

#Region " EVENTS: STOPPED "

        ''' <summary>Raises the Stopped event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnStopped(ByVal e As System.EventArgs) Implements IMachine(Of TSymbol).OnStopped
            SafeEventHandlers.SafeBeginEndInvoke(Me.StoppedEvent, Me, e)
        End Sub

        ''' <summary>Occurs upon state deactivation (exit).</summary>
        ''' <remarks>Use this event to notify the container class that the machine stopped.</remarks>
        Public Event Stopped As EventHandler(Of System.EventArgs) Implements IMachine(Of TSymbol).Stopped

#End Region

#Region " EXECUTION "

        ''' <summary>
        ''' Starts the state machine by <see cref="Initiate">initiating</see> the start state and 
        ''' <see cref="Sequence">repeating the sequence</see> until receiving a <see cref="RequestStop">stop request.</see>
        ''' </summary>
        Public Overridable Sub Activate() Implements IMachine(Of TSymbol).Activate

            Me.Initiate()
            Me.Sequence()

        End Sub

        ''' <summary>
        ''' Specifies the state machine period.
        ''' </summary>
        Public Overridable Property Interval() As Integer Implements IMachine(Of TSymbol).Interval

        ''' <summary>
        ''' Moves the state machine to the Start State. 
        ''' This action clears the input QUEUE. It leaves the machine stopped with the 
        ''' <see cref="_isStopRequested">stop requested sentinel</see> turned on, which is required for the 
        ''' machine to resume its actions.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        Public Overridable Sub Initiate() Implements IMachine(Of TSymbol).Initiate

            SyncLock executeLocker

                Me._IsCountOut = False

                ' set the current state.
                Me._currentState = Me.startState

                ' turn on stop requested.
                Me.RequestStop()

                ' clear the input queue
                Me.inputSymbolQueue.Clear()

                If Me.OnsetDelay > 0 Then
                    Thread.Sleep(Me.OnsetDelay)
                End If

                ' start the ball rolling.
                Me.OnStarted(System.EventArgs.Empty)

                ' notify of the state change after the machine started.
                Me.OnStateChanged(System.EventArgs.Empty)

            End SyncLock

            ' this starts the state operations.
            Me.CurrentState.OnEnter(System.EventArgs.Empty)

        End Sub

        ''' <summary>
        ''' Returns true if the number of consecutive re-entries exceeded the count out interval. 
        ''' </summary>
        Public Property IsCountOut() As Boolean Implements IMachine(Of TSymbol).IsCountOut

        ''' <summary>
        ''' Gets the condition indicating that a stop was requested.
        ''' </summary>
        Public Property IsStopRequested() As Boolean Implements IMachine(Of TSymbol).IsStopRequested

        ''' <summary>
        ''' Gets or sets the time in milliseconds to wait before starting the machine cycle.
        ''' </summary>
        Public Property OnsetDelay() As Integer Implements IMachine(Of TSymbol).OnsetDelay

        ''' <summary>
        ''' Process a single input if any.  This allows running the state machine 
        ''' based on signals alone.
        ''' </summary>
        Public Function ProcessInput() As Boolean Implements IMachine(Of TSymbol).ProcessInput

            If Me.HasInput() Then
                Return ProcessInput(Me.DequeueInput)
            Else
                Return False
            End If

        End Function

        ''' <summary>
        ''' Immediately processes a single input ignoring the input queue. 
        ''' This allows running the state machine based on signals alone.
        ''' </summary>
        ''' <param name="symbol">Specifies the input to process.</param>
        ''' <returns>True if state change was implement or false if the transition is invalid of the 
        ''' change was Canceled.</returns>
        ''' <remarks> David, 04/26/09, 2.3.3403.x">
        ''' Allows makings a transition into the same state in which case the 
        ''' <see cref="IState.Leave">Leave</see> and <see cref="IState.Enter">Enter</see> events 
        ''' do not occur.
        ''' Use the <see cref="IState.Act">act</see> event to process state actions.
        ''' </para></remarks>
        Private Function _ProcessInput(ByVal symbol As TSymbol) As Boolean

            Me._LastActionMessage = String.Empty
            Dim e As New System.ComponentModel.CancelEventArgs

            If Me.CurrentState.IsCountOut Then
                Me.BuildActionMessage("Input '{0}' processing aborted because the '{1}' state had a count-out.",
                                      symbol, Me.CurrentStateCaption)
                Me.OnCountOut(System.EventArgs.Empty)
                Return False
            End If

            Me.OnAcknowledge(New SymbolEventArgs(Of TSymbol)(symbol))

            Dim newTranstion As ITransition(Of TSymbol) = Me.transitionTable.SelectTransition(Me.CurrentState, symbol)

            If newTranstion.NextState Is Nothing Then

                Me._currentState = Me._currentState
                Me.BuildActionMessage("Transition not found from the '{0}' state for the '{1}' input.",
                                      Me.CurrentStateCaption, symbol)
                Return False

            Else

                newTranstion.OnStarting(e)

                If e.Cancel Then
                    Me.BuildActionMessage("Transition Canceled from the '{0}' state for the '{1}' input.",
                                          Me.CurrentStateCaption, symbol)
                Else

                    If Not Me.CurrentState.Equals(newTranstion.NextState) Then

                        Me.CurrentState.OnLeave(e)

                        If e.Cancel Then

                            Me.BuildActionMessage("Exit from the '{0}' state canceled for the '{1}' input.",
                                                  Me.CurrentStateCaption, symbol)

                        Else

                            Me._previousState = Me.States.Item(Me.CurrentState.Name)

                            Me._currentState = newTranstion.NextState

                            Me.OnStateChanged(System.EventArgs.Empty)

                            Me.CurrentState.OnEnter(System.EventArgs.Empty)

                        End If

                    End If

                    If Not e.Cancel Then

                        newTranstion.OnCompleted(System.EventArgs.Empty)

                    End If

                End If

            End If

            If Not e.Cancel Then
                Me.CurrentState.OnAct(System.EventArgs.Empty)
            End If

            Return Not e.Failed

        End Function

        ''' <summary>
        ''' Immediately processes a single input ignoring the input queue. 
        ''' This allows running the state machine based on signals alone.
        ''' </summary>
        ''' <param name="value">Specifies the input to process.</param>
        ''' <returns>True if state change was implement or false if the transition is invalid of the 
        ''' change was Canceled.</returns>
        ''' <remarks> David, 04/26/09, 2.3.3403.x">
        ''' Allows makings a transition into the same state in which case the 
        ''' <see cref="IState.Leave">Leave</see> and <see cref="IState.Enter">Enter</see> events 
        ''' do not occur.
        ''' Use the <see cref="IState.Act">act</see> event to process state actions.
        ''' </para></remarks>
        Public Function ProcessInput(ByVal value As TSymbol) As Boolean Implements IMachine(Of TSymbol).ProcessInput

            SyncLock executeLocker
                Return Me._processInput(value)
            End SyncLock

        End Function

        Private isFailed As Boolean
        ''' <summary>
        ''' Immediately processes a series of inputs input ignoring the input queue. 
        ''' This allows running the state machine based on signals alone.
        ''' </summary>
        ''' <param name="values">Specifies the inputs to process.</param>
        ''' <returns>True if state change was implement or false if the transition is invalid of the 
        ''' change was Canceled.</returns>
        ''' <remarks> David, 04/26/09, 2.3.3403.x">
        ''' Allows makings a transition into the same state in which case the 
        ''' <see cref="IState.Leave">Leave</see> and <see cref="IState.Enter">Enter</see> events 
        ''' do not occur.
        ''' Use the <see cref="IState.Act">act</see> event to process state actions.
        ''' </para></remarks>
        Public Function ProcessInput(ByVal values As TSymbol()) As Boolean Implements IMachine(Of TSymbol).ProcessInput
            If values IsNot Nothing AndAlso values.Length > 0 Then
                SyncLock executeLocker
                    For Each value As TSymbol In values
                        isFailed = Not Me._processInput(value) OrElse Me.CurrentState.IsCountOut OrElse Me.CurrentState.IsTimeout
                        If isFailed Then
                            Exit For
                        End If
                    Next
                End SyncLock
            End If
            Return Not isFailed
        End Function

        ''' <summary>
        ''' Requests the state machine to stop running after completion of the last action.
        ''' </summary>
        Public Overridable Sub RequestStop() Implements IMachine(Of TSymbol).RequestStop
            Me._IsStopRequested = True
        End Sub

        ''' <summary>
        ''' Runs the state machine sequence until receiving <see cref="RequestStop">stop request.</see>
        ''' The sequence will run only after a previous stop requested that leaves 
        ''' <see cref="_isStopRequested">stop requested sentinel</see> turned on.
        ''' </summary>
        Public Overridable Sub Sequence() Implements IMachine(Of TSymbol).Sequence

            If Not Me._IsStopRequested Then
                Return
            End If

            SyncLock executeLocker

                Me._IsCountOut = False

                Me._IsStopRequested = False

                Do Until Me._IsStopRequested

                    If Me.HasInput() Then
                        Me._processInput(Me.DequeueInput)
                    End If

                    ' it takes the system 10 to 15 ms to get back to this thread.
                    ' Use timer for faster systems.
                    Thread.Sleep(Me.Interval)

                Loop

                Me.OnStopped(System.EventArgs.Empty)

            End SyncLock

        End Sub

#End Region

#Region " MACHINE CONSTRUCTION AND ANALYSIS "

        Private _AnalysisReport As System.Text.StringBuilder
        ''' <summary>
        ''' Returns the analysis report.
        ''' </summary>
        Public ReadOnly Property AnalysisReport() As String Implements IMachine(Of TSymbol).AnalysisReport
            Get
                If Me._analysisReport Is Nothing Then
                    Return "Analysis not run"
                Else
                    Return Me._analysisReport.ToString
                End If
            End Get
        End Property

        Private _IsFullyDefined As Boolean?
        ''' <summary>
        ''' Returns true of the state machine is fully defined.
        ''' The machine is fully defined if all states have an associated transition.
        ''' </summary>
        Public ReadOnly Property IsFullyDefined() As Boolean? Implements IMachine(Of TSymbol).IsFullyDefined
            Get
                Return Me._isFullyDefined
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the state machine is deterministic, i.e., if each state and symbol have a unique
        ''' next state. Namely, if all transitions are unique.
        ''' </summary>
        Public ReadOnly Property IsDeterministic() As Boolean Implements IMachine(Of TSymbol).IsDeterministic
            Get
                Return Me.transitionTable.IsDeterministic
            End Get
        End Property

        ''' <summary>
        ''' Analyzes the state machine to determine if it is fully specified.
        ''' </summary>
        ''' <returns>
        ''' True if the machine is fully specified and all signals and states are unique.
        ''' 
        ''' </returns>
        Public Function Analyze() As Boolean Implements IMachine(Of TSymbol).Analyze


            Me._analysisReport = New System.Text.StringBuilder
            Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                            "Analysis report {1:DD-MM-yyyy HH:mm:ss}{0}", Environment.NewLine, DateTime.Now)

            Me._isFullyDefined = True
            Dim transitions As LinkedList(Of ITransition(Of TSymbol)) = New LinkedList(Of ITransition(Of TSymbol))()

            Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                            "Analysis of Transitions:{0}", Environment.NewLine)
            For Each existingState As State In Me.States.Values

                For Each symbol As TSymbol In Me._symbols.Values

                    Dim outcome As String = String.Empty
                    Dim transtion As ITransition(Of TSymbol) = Me.transitionTable.SelectTransition(existingState, symbol)
                    If transtion.NextState Is Nothing Then

                        Me._isFullyDefined = False

                        outcome = "not found"

                        transitions.AddLast(transtion)

                    Else

                        outcome = "found"

                    End If

                    Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                    "Transition from State '{1}' with symbol '{2}' {3}{0}", Environment.NewLine,
                                                    existingState, symbol, outcome)

                Next symbol

            Next existingState

            If Not Me._isFullyDefined Then

                Me._analysisReport.AppendLine("Following transitions are missing:")

                Dim node As LinkedListNode(Of ITransition(Of TSymbol)) = transitions.First

                Do While node IsNot Nothing

                    Dim existingTransition As ITransition(Of TSymbol) = node.Value

                    Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                    "Transition from State '{1}' with symbol '{2}' -> undefined{0}", Environment.NewLine,
                                                    existingTransition.State.Name, existingTransition.Symbol.ToString)

                    node = node.Next

                Loop

                If Me.IsDeterministic Then
                    Me._analysisReport.AppendLine("This Moore-Machine is a partially-specified deterministic finite state machine.")
                Else
                    Me._analysisReport.Append(Me.transitionTable.AnalysisReport)
                    Me._analysisReport.AppendLine("This Moore-Machine is a partially-specified non-deterministic finite state machine.")
                End If

            Else

                If Me.IsDeterministic Then
                    Me._analysisReport.AppendLine("This Moore-Machine is a fully-specified deterministic finite state machine.")
                Else
                    Me._analysisReport.Append(Me.transitionTable.AnalysisReport)
                    Me._analysisReport.AppendLine("This Moore-Machine is a fully-specified non-deterministic finite state machine.")
                End If

            End If

            Return Not Me._isFullyDefined.Value

        End Function

#End Region

#Region " STATES "

        Private _CurrentState As IState
        ''' <summary>
        ''' Gets reference to the active <see cref="IState">state</see>.
        ''' </summary>
        Public ReadOnly Property CurrentState() As IState Implements IMachine(Of TSymbol).CurrentState
            Get
                Return Me._currentState
            End Get
        End Property

        ''' <summary>
        ''' Gets the caption of the active state.
        ''' </summary>
        Public ReadOnly Property CurrentStateCaption() As String Implements IMachine(Of TSymbol).CurrentStateCaption
            Get
                If Me.CurrentState Is Nothing Then
                    Return "N/A"
                Else
                    Return Me.CurrentState.Caption
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns the unique ID of the current state.
        ''' </summary>
        Public ReadOnly Property CurrentStateUniqueId() As Integer Implements IMachine(Of TSymbol).CurrentStateUniqueId
            Get
                If Me.CurrentState Is Nothing Then
                    Return 0
                Else
                    Return Me.CurrentState.UniqueId
                End If
            End Get
        End Property

        Private _PreviousState As IState
        ''' <summary>
        ''' Gets reference to the previous active <see cref="IState">state</see>.
        ''' </summary>
        Public ReadOnly Property PreviousState() As IState Implements IMachine(Of TSymbol).PreviousState
            Get
                Return Me._previousState
            End Get
        End Property

        ''' <summary>
        ''' A collection of states with a unique names.
        ''' </summary>
        Private _States As Collections.Generic.Dictionary(Of String, IState)
        ''' <summary>
        ''' Returns the collection of states with a unique names.
        ''' </summary>
        Protected Function States() As Collections.Generic.Dictionary(Of String, IState)
            Return Me._states
        End Function

        Private _StartState As IState
        ''' <summary>
        ''' Gets the start state--the state at which the automaton is when no input has been processed yet.
        ''' </summary>
        Private ReadOnly Property StartState() As IState
            Get
                Return Me._startState
            End Get
        End Property

        ''' <summary>
        ''' Sets the start state to a state other than the first state that was added.
        ''' This also sets the current state.
        ''' </summary>
        Public Sub StartStateSetter(ByVal value As IState) Implements IMachine(Of TSymbol).StartStateSetter
            Me._startState = value
            Me._currentState = value
        End Sub

        ''' <summary>
        ''' Adds a <see cref="State">state</see> to the <see cref="Machine(Of TSymbol)">Moore state Machine</see>.
        ''' </summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
   Justification:="Object is returned")>
        Public Function AddState(ByVal uniqueName As String, ByVal timeoutInterval As Integer) As IState Implements IMachine(Of TSymbol).AddState
            Dim candidate As New State(uniqueName, timeoutInterval)
            Me.States.Add(uniqueName, candidate)
            ' initialize the unique ID.
            candidate.UniqueId = Me.States.Count
            If Me.States.Count = 1 Then
                StartStateSetter(candidate)
            End If
            Return candidate
        End Function

        ''' <summary>
        ''' Adds a <see cref="State">state</see> to the <see cref="Machine(Of TSymbol)">Moore state Machine</see>.
        ''' </summary>
        Public Function AddState(ByVal uniqueName As String) As IState Implements IMachine(Of TSymbol).AddState
            Return Me.AddState(uniqueName, Threading.Timeout.Infinite)
        End Function

#End Region

#Region " SYMBOLS "

        ''' <summary>
        ''' A finite set of symbols. Also called alphabet of the language the automaton accepts
        ''' Symbols are sometimes just called "letters" or "atoms".
        ''' </summary>
        Private _Symbols As Collections.Generic.Dictionary(Of Integer, TSymbol)

        ''' <summary>
        ''' Adds an input symbol.
        ''' The symbol must have a unique <see cref="GetHashCode">hash code</see>
        ''' </summary>
        ''' <param name="value">Specifies the symbol value</param>
        Public Function AddSymbol(ByVal value As TSymbol) As TSymbol Implements IMachine(Of TSymbol).AddSymbol
            Me._symbols.Add(value.GetHashCode, value)
            Return value
        End Function

#End Region

#Region " ACTION MESSAGE "

        ''' <summary>
        ''' Gets the last action message.
        ''' </summary>
        Public Property LastActionMessage() As String Implements IMachine(Of TSymbol).LastActionMessage

        ''' <summary>
        ''' Builds the <see cref="LastActionMessage">last action message</see>
        ''' </summary>
        Protected Sub BuildActionMessage(ByVal format As String, ByVal ParamArray args() As Object) Implements IMachine(Of TSymbol).BuildActionMessage
            Me.LastActionMessage = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        End Sub

#End Region

#Region " TRANSITION FUNCTION "

        ''' <summary>
        ''' Gets or sets the transition function. Defines the next state given the current state and current input.
        ''' </summary>
        Private Property TransitionTable() As TransitionFunction(Of TSymbol)

        ''' <summary>
        ''' Adds a transition to the state machine.
        ''' </summary>
        ''' <param name="nextState"></param>
        Public Function AddTransition(ByVal state As IState, ByVal symbol As TSymbol, ByVal nextState As IState) As ITransition(Of TSymbol) Implements IMachine(Of TSymbol).AddTransition

            Dim transition As ITransition(Of TSymbol) = Me.transitionTable.AddTransition(state, symbol, nextState)
            Return transition

        End Function

        ''' <summary>
        ''' Checks if the specified <typeparamref name="TSymbol">symbol</typeparamref>input symbol signals a valid 
        ''' transition from the current state. 
        ''' Sets the <see cref="LastActionMessage">last action message</see> if an invalid implied transition is found.
        ''' </summary>
        ''' <param name="value">The inputs <typeparamref name="TSymbol">symbol</typeparamref> which requires validation</param>
        ''' <returns>True if the symbol leads to a valid transition or False if no transition 
        ''' from the current state is defined for the current input symbol.</returns>
        Public Function ValidateInput(ByVal value As TSymbol) As Boolean Implements IMachine(Of TSymbol).ValidateInput
            If Me.transitionTable.SelectTransition(Me.CurrentState, value).NextState Is Nothing Then
                Me.BuildActionMessage("Transition from the '{0}' state not found for the '{1}' input.", Me.CurrentStateCaption, value)
                Return False
            Else
                Me._LastActionMessage = String.Empty
                Return True
            End If
        End Function

        ''' <summary>
        ''' Checks for the validity of all the transitions implied by the specified symbols.
        ''' Sets the <see cref="LastActionMessage">last action message</see> if an invalid implied transition is found.
        ''' </summary>
        ''' <param name="values">The inputs <typeparamref name="TSymbol">symbol</typeparamref> which requires validation</param>
        ''' <returns>Returns True if all implied transitions are valid. Returns false after detecting the first invalid implied transition.</returns>
        Public Function ValidateAllInputs(ByVal values As TSymbol()) As Boolean Implements IMachine(Of TSymbol).ValidateAllInputs
            If values IsNot Nothing AndAlso values.Length > 0 Then
                For Each symbol As TSymbol In values
                    If Not Me.ValidateInput(symbol) Then
                        Return False
                    End If
                Next
            End If
            Return True
        End Function

        ''' <summary>
        ''' Checks for the validity of one of the transitions implied by the specified symbols.
        ''' Sets the <see cref="LastActionMessage">last action message</see> if an invalid implied transition is found.
        ''' </summary>
        ''' <param name="values">The inputs <typeparamref name="TSymbol">symbol</typeparamref> which requires validation</param>
        ''' <returns>Returns True if all implied transitions are valid. Returns false after detecting the first invalid implied transition.</returns>
        Public Function ValidateInput(ByVal values() As TSymbol) As Boolean Implements IMachine(Of TSymbol).ValidateInput
            If values IsNot Nothing AndAlso values.Length > 0 Then
                For Each symbol As TSymbol In values
                    If Me.ValidateInput(symbol) Then
                        Return True
                    End If
                Next
                Return False
            End If
            Return True
        End Function

#End Region

    End Class

End Namespace

