Imports System.ComponentModel
Namespace Moore

    ''' <summary>
    ''' Implements a state in a finite automata sequencing.
    ''' </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/26/09, 2.3.3403.x">
    ''' based on Moore machine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreMachine.aspx
    ''' Applies to expansions from the original.
    ''' </para></remarks>
    <Description("Moore Finite State Machine State"), DefaultEvent("Act")>
    Public Class State
        Implements IState

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary>
        ''' Constructs the state with a specific callback function.
        ''' </summary>
        ''' <param name="uniqueName">Specifies a unique state name</param>
        ''' <param name="timeoutInterval">Specifies the state timeout interval.</param>
        Public Sub New(ByVal uniqueName As String, ByVal timeoutInterval As Integer)
            MyBase.New()
            If String.IsNullOrWhiteSpace(uniqueName) Then uniqueName = String.Empty
            Me._name = uniqueName
            Me._Caption = uniqueName.ToUpperInvariant
            If timeoutInterval <= 0 Then
                timeoutInterval = Threading.Timeout.Infinite
            End If
            Me._timeoutInterval = State.TimeoutIntervalSetter(timeoutInterval)
            eventLocker = New Object
            timeoutLocker = New Object
        End Sub

        ''' <summary>
        ''' Constructs the state with a specific callback function.
        ''' </summary>
        ''' <param name="uniqueName">Specifies a unique state name</param>
        Public Sub New(ByVal uniqueName As String)
            Me.New(uniqueName, Threading.Timeout.Infinite)
        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method Overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    ' Free managed resources when explicitly called
                    If Me.ActEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.ActEvent?.GetInvocationList
                            RemoveHandler Me.Act, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If
                    If Me.EnterEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.EnterEvent?.GetInvocationList
                            RemoveHandler Me.Enter, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If
                    If Me.LeaveEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.LeaveEvent?.GetInvocationList
                            RemoveHandler Me.Leave, CType(d, Global.System.EventHandler(Of Global.System.ComponentModel.CancelEventArgs))
                        Next
                    End If
                    If Me.TimeoutEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.TimeoutEvent?.GetInvocationList
                            RemoveHandler Me.Timeout, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If
                    SyncLock eventLocker
                        If Me._timeoutTimer IsNot Nothing Then Me._timeoutTimer.Dispose() : Me._timeoutTimer = Nothing
                    End SyncLock
                End If
            Finally
                Me.IsDisposed = True
            End Try
        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        End Sub

#End Region

#Region " EVENTS "

        Private eventLocker As Object
        ''' <summary>Raises the Act event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnAct(ByVal e As System.EventArgs) Implements IState.OnAct
            SyncLock eventLocker
                If Me._CountOutCount > 0 AndAlso Me._countOutCountDown >= 0 Then
                    Me._countOutCountDown -= 1
                End If
                If Not Me.IsCountOut Then
                    SafeEventHandlers.SafeBeginEndInvoke(Me.ActEvent, Me, e)
                End If
            End SyncLock
        End Sub

        ''' <summary>Occurs upon state actions.</summary>
        ''' <remarks>Use this event to notify the container class that the state actions are taking place.</remarks>
        Public Event Act As EventHandler(Of System.EventArgs) Implements IState.Act

        ''' <summary>Raises the Enter event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnEnter(ByVal e As System.EventArgs) Implements IState.OnEnter
            SyncLock eventLocker
                If Me._CountOutCount > 0 Then
                    Me._countOutCountDown = Me._CountOutCount
                End If
                ' start the timeout timer. Timeout interval is infinite (-1) if timeout is non-positive.
                Me._isTimeout = False
                Me._timeoutTimer = New System.Threading.Timer(New System.Threading.TimerCallback(AddressOf OnTimer),
                                                              New TimerState, Me._timeoutInterval, System.Threading.Timeout.Infinite)
                SafeEventHandlers.SafeBeginEndInvoke(Me.EnterEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs upon state activation (entry).</summary>
        ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
        Public Event Enter As EventHandler(Of System.EventArgs) Implements IState.Enter

        ''' <summary>Raises the Leave event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnLeave(ByVal e As System.ComponentModel.CancelEventArgs) Implements IState.OnLeave
            SyncLock eventLocker
                ' stop the timeout timer.
                Me._timeoutTimer.Change(Threading.Timeout.Infinite, Threading.Timeout.Infinite)
                Me._timeoutTimer.Dispose()
                Me._timeoutTimer = Nothing
                SafeEventHandlers.SafeBeginEndInvoke(Me.LeaveEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs upon state deactivation (exit).</summary>
        ''' <remarks>Use this event to notify the container class that the state was deactivated.</remarks>
        Public Event Leave As EventHandler(Of System.ComponentModel.CancelEventArgs) Implements IState.Leave

        ''' <summary>Raises the Timeout event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnTimeout(ByVal e As System.EventArgs) Implements IState.OnTimeout
            Me._isTimeout = True
            SafeEventHandlers.SafeBeginEndInvoke(Me.TimeoutEvent, Me, e)
        End Sub

        ''' <summary>Occurs upon state deactivation (exit).</summary>
        ''' <remarks>Use this event to notify the container class that the state was deactivated.</remarks>
        Public Event Timeout As EventHandler(Of System.EventArgs) Implements IState.Timeout

#End Region

#Region " EQUALS "

        ''' <summary>
        ''' Returns true if the instance equals another instance.
        ''' </summary>
        ''' <param name="left">Specifies a left hand value for comparison</param>
        ''' <param name="right">Specifies a right hand value for comparison</param>
        Public Overloads Shared Function Equals(ByVal left As IState, ByVal right As IState) As Boolean
            If left Is Nothing Then
                Return right Is Nothing
            ElseIf right Is Nothing Then
                Return False
            Else
                Return left.UniqueId.Equals(right.UniqueId)
            End If
        End Function

        ''' <summary>
        ''' Implements comparison with a generic object.
        ''' </summary>
        Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean Implements IState.Equals
            Return State.Equals(Me, TryCast(obj, IState))
        End Function

        ''' <summary>
        ''' Returns true if the instance equals another instance.
        ''' </summary>
        Public Overloads Function Equals(ByVal value As IState) As Boolean Implements IState.Equals
            Return State.Equals(Me, value)
        End Function

        ''' <summary>
        ''' Returns the hash code for the state.
        ''' </summary>
        Public Overrides Function GetHashCode() As Integer Implements IState.GetHashCode
            Return Me.UniqueId.GetHashCode()
        End Function

#End Region

#Region " MEMBERS "

        ''' <summary>
        ''' Gets or sets the state caption to use when displaying the state.
        ''' Initialized to the state name.
        ''' </summary>
        Public Property Caption() As String Implements IState.Caption

        ''' <summary>
        ''' Gets or sets the number of times the state can be re-entered consecutively. 
        ''' </summary>
        Property CountOutCount() As Integer Implements IState.CountOutCount

        Private _CountOutCountDown As Integer
        ''' <summary>
        ''' Returns true if the number of consecutive re-entries exceeded the count out interval. 
        ''' </summary>
        Function IsCountOut() As Boolean Implements IState.IsCountOut
            Return Me._countOutCountDown < 0
        End Function

        Private _IsTimeout As Boolean
        ''' <summary>
        ''' Returns true if state hit timeout. 
        ''' </summary>
        Public ReadOnly Property IsTimeout() As Boolean Implements IState.IsTimeout
            Get
                Return Me._isTimeout
            End Get
        End Property

        Private _Name As String
        ''' <summary>
        ''' Gets the state name.
        ''' The state name must be unique for the state machine.
        ''' </summary>
        Public ReadOnly Property Name() As String Implements IState.Name
            Get
                Return Me._name
            End Get
        End Property

        ''' <summary>
        ''' Returns a value timeout value.
        ''' </summary>
        Public Shared Function TimeoutIntervalSetter(ByVal value As Integer) As Integer
            If value <= 0 Then
                value = Threading.Timeout.Infinite
            End If
            Return value
        End Function

        Private _TimeoutInterval As Integer
        ''' <summary>
        ''' Gets or sets the timeout interval.
        ''' Non-positive values is converted to <see cref="System.Threading.Timeout.Infinite">infinite</see>
        ''' </summary>
        Public Property TimeoutInterval() As Integer Implements IState.TimeoutInterval
            Get
                Return Me._timeoutInterval
            End Get
            Set(ByVal value As Integer)
                Me._timeoutInterval = TimeoutIntervalSetter(value)
            End Set
        End Property

        ''' <summary>
        ''' Gets or sets the state unique ID.
        ''' </summary>
        Public Property UniqueId() As Integer Implements IState.UniqueId

#End Region

#Region " TIMEOUT TIMER "

        ''' <summary>Gets or sets reference to the thread timer, which events run the sequencing engine.</summary>
        Private _TimeoutTimer As System.Threading.Timer

        Private timeoutLocker As Object
        ''' <summary>
        ''' Handles timer events. These depend on the reader state.
        ''' </summary>
        Protected Overridable Sub OnTimer(ByVal state As Object)

            ' cast the timer state so that we can access the timer as necessary.
            Dim currentTimerState As TimerState = CType(state, TimerState)

            ' check if already active
            If currentTimerState.IsBusy Then
                ' if already active, increment the count of missed ticks.
                currentTimerState.TicksMissed += 1
                ' and get out
                Return
            End If

            SyncLock timeoutLocker

                ' if the event disposed the timer than exit.
                If Me._timeoutTimer Is Nothing Then
                    Return
                End If

                ' tag the counter as busy.
                currentTimerState.IsBusy = True

                ' increment to total count of missed ticks
                currentTimerState.TicksMissedTotal += currentTimerState.TicksMissed

                ' and clear the count since last timer event
                currentTimerState.TicksMissed = 0

                ' throw the timeout event.
                Me.OnTimeout(System.EventArgs.Empty)

                ' tag the counter as no longer busy.
                If currentTimerState IsNot Nothing Then
                    currentTimerState.IsBusy = False
                End If

            End SyncLock


        End Sub

        ''' <summary>
        ''' Provides a timer state to the timer event.</summary>
        ''' <remarks> David, 07/10/04, 2.0.1652.x">
        ''' (c) 2010 Integrated Scientific Resources, Inc.
        ''' From <see cref="Automata">automata library.</see>
        ''' </para></remarks>
        Protected Class TimerState

            ''' <summary>
            ''' Gets or sets a value indicating whether this instance is busy.
            ''' </summary>
            ''' <value><c>True</c> if this instance is busy; otherwise, <c>False</c>.</value>
            Public Property IsBusy() As Boolean

            ''' <summary>
            ''' Gets or sets the ticks missed.
            ''' </summary>
            ''' <value>The ticks missed.</value>
            Public Property TicksMissed() As Integer

            ''' <summary>
            ''' Gets or sets the ticks missed total.
            ''' </summary>
            ''' <value>The ticks missed total.</value>
            Public Property TicksMissedTotal() As Integer

        End Class

#End Region

    End Class

End Namespace
