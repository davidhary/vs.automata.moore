Friend Module SafeEventHandlers

    ''' <summary> Safely triggers an event. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Public Sub SafeBeginEndInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                              ByVal sender As Object, ByVal e As TEventArgs)
        If handler IsNot Nothing Then
            For Each d As [Delegate] In handler?.GetInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, e})
                Else
                    Dim target As System.ComponentModel.ISynchronizeInvoke = TryCast(d.Target, System.ComponentModel.ISynchronizeInvoke)
                    If target IsNot Nothing AndAlso target.InvokeRequired Then
                        Dim result As IAsyncResult = target.BeginInvoke(d, New Object() {sender, e})
                        If result IsNot Nothing Then
                            target.EndInvoke(result)
                        End If
                    Else
                        d.DynamicInvoke(New Object() {sender, e})
                    End If
                End If
            Next
        End If
    End Sub

End Module
