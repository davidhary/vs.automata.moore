﻿Imports System.ComponentModel
Namespace Moore

    ''' <summary>
    ''' Defines an interface for handling a query, failures, count outs and time outs for the
    ''' <see cref="Moore.QueryAutomaton">Query automation</see>
    ''' </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 09/06/10, 3.0.3901.x">
    ''' created
    ''' </para></remarks>
    Public Interface IQueryAutomaton
        Inherits IMachine(Of QueryAutomatonSymbol)

#Region " EXECUTION OVERLOADS "

        Overloads Sub Activate()
        Overloads Sub Initiate()
        Overloads Sub Sequence()

#End Region

#Region " ACTION EVENTS: QUERY "

        ''' <summary>Raises the Query event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="HandledEventArgs">event handled arguments</see>
        ''' </param>
        Sub OnQuery(ByVal e As HandledEventArgs)

        ''' <summary>Occurs upon state deactivation (exit).</summary>
        ''' <remarks>Use this event to notify the container class that the state was deactivated.</remarks>
        Event Query As EventHandler(Of HandledEventArgs)

#End Region

#Region " ACTION EVENTS: COUNT OUT "

        Overloads Sub OnCountOut(ByVal e As System.EventArgs)

#End Region

#Region " ACTION EVENTS: TIMEOUT "

        ''' <summary>
        ''' Gets the sentinel indicating that a state timed out.
        ''' </summary>
        ReadOnly Property IsTimeout() As Boolean

#End Region

    End Interface

End Namespace

