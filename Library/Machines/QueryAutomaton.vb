Imports System.ComponentModel

Namespace Moore

    ''' <summary>
    ''' An automaton class for implementing a finite automata for querying.
    ''' The automaton can be set to automatically stop after the query is done, failed, or timed out.
    ''' This automaton can be used to implement an automation for waiting for a service request. 
    ''' The machine has the following states:<para>
    ''' READY</para><para>
    ''' FAILURE</para><para>
    ''' QUERYING</para><para>
    ''' STOP - Requests a stop</para><para>
    ''' And the following transitions:</para><para>
    ''' READY: any state -> READY</para><para>
    ''' FAILURE: any state -> FAILURE</para><para>
    ''' QUERY: READY, FAILURE -> QUERYING</para><para>
    ''' STOP: any state -> STOP</para>
    ''' </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 09/06/10, 3.0.3901.x">
    ''' created
    ''' </para></remarks>
    Public Class QueryAutomaton
        Inherits Machine(Of QueryAutomatonSymbol)
        Implements IQueryAutomaton

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="queryTimeout">Specifies the query timeout interval in milliseconds</param>
        Public Sub New(ByVal queryTimeout As Integer)

            MyBase.New()
            MyBase.OnsetDelay = 0
            MyBase.Interval = 1

            ' define the states.
            Me._readyState = MyBase.AddState("ready")
            Me._failureState = MyBase.AddState("failure")
            Me._failureState.CountOutCount = 3
            Me._queryingState = MyBase.AddState("querying")
            Me._queryingState.TimeoutInterval = queryTimeout
            Me._stopState = MyBase.AddState("stop")

            ' set the start state
            Me.StartStateSetter(Me._readyState)

            ' define the signals.
            MyBase.AddSymbol(QueryAutomatonSymbol.Ready)
            MyBase.AddSymbol(QueryAutomatonSymbol.Failure)
            MyBase.AddSymbol(QueryAutomatonSymbol.Query)
            MyBase.AddSymbol(QueryAutomatonSymbol.Stop)

            ' define the transitions

            ' READY TRANSITION
            For Each state As IState In MyBase.States.Values
                MyBase.AddTransition(state, QueryAutomatonSymbol.Ready, Me._readyState)
            Next

            ' FAILURE TRANSITION
            For Each state As IState In MyBase.States.Values
                MyBase.AddTransition(state, QueryAutomatonSymbol.Failure, Me._failureState)
            Next

            ' STOP TRANSITION
            For Each state As IState In MyBase.States.Values
                MyBase.AddTransition(state, QueryAutomatonSymbol.Stop, Me._stopState)
            Next

            MyBase.AddTransition(Me._readyState, QueryAutomatonSymbol.Query, Me._queryingState)
            MyBase.AddTransition(Me._queryingState, QueryAutomatonSymbol.Query, Me._queryingState)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    ' Free managed resources when explicitly called
                    If Me.QueryEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.QueryEvent?.GetInvocationList
                            RemoveHandler Me.Query, CType(d, Global.System.EventHandler(Of HandledEventArgs))
                        Next
                    End If
                    If Me._failureState IsNot Nothing Then Me._failureState = Nothing
                    If Me._readyState IsNot Nothing Then Me._readyState = Nothing
                    If Me._queryingState IsNot Nothing Then Me._queryingState = Nothing
                    If Me._stopState IsNot Nothing Then Me._stopState = Nothing
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#Region " STATES "

        Private WithEvents _ReadyState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="ReadyState">Ready State</see>.
        ''' </summary>
        Protected ReadOnly Property ReadyState() As IState
            Get
                Return Me._readyState
            End Get
        End Property

        Private WithEvents _QueryingState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="QueryingState">Querying State</see>.
        ''' </summary>
        Protected ReadOnly Property QueryingState() As IState
            Get
                Return Me._queryingState
            End Get
        End Property

        Private WithEvents _FailureState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="FailureState">Failure State</see>.
        ''' </summary>
        Protected ReadOnly Property FailureState() As IState
            Get
                Return Me._failureState
            End Get
        End Property

        Private WithEvents _StopState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="StopState">Stop State</see>.
        ''' </summary>
        Protected ReadOnly Property StopState() As IState
            Get
                Return Me._stopState
            End Get
        End Property

#End Region

#Region " STATE EVENTS "

        ''' <summary>
        ''' Handles failure state action. Sends the machine to the stop state if the failure was handled. If 
        ''' handling failed, sends the machine to another failure. 
        ''' </summary>
        Private Sub FailureState_Act(ByVal sender As Object, ByVal e As System.EventArgs) Handles _failureState.Act
            If String.IsNullOrWhiteSpace(MyBase.LastActionMessage) Then
                MyBase.BuildActionMessage("Timeout occurred during the {0} state", Me.CurrentStateCaption)
            End If
            Me.EnqueueInput(QueryAutomatonSymbol.Stop, False, False)
        End Sub

        Private Sub TimeoutThis(ByVal sender As Object, ByVal e As System.EventArgs) Handles _failureState.Timeout, _queryingState.Timeout
            Me._isTimeout = True
            MyBase.BuildActionMessage("Timeout occurred during the {0} state", Me.CurrentStateCaption)
            EnqueueInput(QueryAutomatonSymbol.Failure, False, False)
        End Sub

        ''' <summary>
        ''' Issues the query request. The query request should turn on the handled flag when done. Otherwise, the 
        ''' query will repeat until satisfied or timed out.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub QueryingState_Act(ByVal sender As Object, ByVal e As System.EventArgs) Handles _queryingState.Act
            Try
                Dim eventArgs As New HandledEventArgs
                Me.OnQuery(eventArgs)
                If eventArgs.Handled Then
                    EnqueueInput(QueryAutomatonSymbol.Stop, False, False)
                Else
                    EnqueueInput(QueryAutomatonSymbol.Query, False, False)
                End If
            Catch ex As Exception
                MyBase.BuildActionMessage("Exception occurred querying. {0}", ex.ToFullBlownString)
                EnqueueInput(QueryAutomatonSymbol.Failure, False, False)
            End Try
        End Sub

        Private Sub StopState_Act(ByVal sender As Object, ByVal e As System.EventArgs) Handles _stopState.Act
            MyBase.RequestStop()
        End Sub

#End Region

#Region " EXECUTION "

        ''' <summary>
        ''' Starts the state machine by <see cref="Initiate">initiating</see> the start state, enqueuing 
        ''' the <see cref="QueryAutomatonSymbol.Query">query</see> signal and then 
        ''' <see cref="Sequence">repeating the sequence</see> until receiving a <see cref="RequestStop">stop request.</see>
        ''' </summary>
        Public Overrides Sub Activate() Implements IQueryAutomaton.Activate

            Me.Initiate()
            Me.EnqueueInput(QueryAutomatonSymbol.Query, False, False)
            Me.Sequence()

        End Sub

        Public Overrides Sub Initiate() Implements IQueryAutomaton.Initiate
            Me._isTimeout = False
            MyBase.Initiate()
        End Sub

        Public Overrides Sub Sequence() Implements IQueryAutomaton.Sequence
            Me._isTimeout = False
            MyBase.Sequence()
        End Sub

#End Region

#Region " ACTION EVENTS: QUERY "

        ''' <summary>Raises the Query event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="HandledEventArgs">event handled arguments</see>
        ''' </param>
        Protected Overridable Sub OnQuery(ByVal e As HandledEventArgs) Implements IQueryAutomaton.OnQuery
            SafeEventHandlers.SafeBeginEndInvoke(Me.QueryEvent, Me, e)
        End Sub

        ''' <summary>Occurs upon state deactivation (exit).</summary>
        ''' <remarks>Use this event to notify the container class that the state was deactivated.</remarks>
        Public Event Query As EventHandler(Of HandledEventArgs) Implements IQueryAutomaton.Query

#End Region

#Region " ACTION EVENTS: COUNT OUT "

        Protected Overrides Sub OnCountOut(ByVal e As System.EventArgs) Implements IQueryAutomaton.OnCountOut
            MyBase.OnCountOut(e)
            MyBase.BuildActionMessage("Count out occurred during the {0} state", Me.CurrentStateCaption)
            EnqueueInput(QueryAutomatonSymbol.Failure, False, False)
        End Sub

#End Region

#Region " ACTION EVENTS: TIMEOUT "

        Private _IsTimeout As Boolean
        ''' <summary>
        ''' Gets the sentinel indicating that a state timed out.
        ''' </summary>
        Public ReadOnly Property IsTimeout() As Boolean Implements IQueryAutomaton.IsTimeout
            Get
                Return Me._isTimeout
            End Get
        End Property

#End Region

    End Class

    ''' <summary>
    ''' Enumerates the state transition symbols for the 
    ''' <see cref="QueryAutomaton">Ready-Query</see> automaton.
    ''' </summary>
    Public Enum QueryAutomatonSymbol
        <Description("Not Defined")> None
        <Description("Query: Ready, Querying -> Querying")> Query
        <Description("Failure: Any State -> Failure")> Failure
        <Description("Ready: Any state -> Ready")> Ready
        <Description("Stop: Any state -> Stop")> [Stop]
    End Enum

End Namespace

