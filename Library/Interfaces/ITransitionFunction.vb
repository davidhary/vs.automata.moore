''' <summary>
''' Interface to a finite state machine transition function.
''' </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/26/09, 2.3.3403.x">
''' created
''' </para></remarks>
Public Interface ITransitionFunction(Of TSymbol)

    Inherits IDisposable

#Region " TRANSITION TABLE CONSTRUCTION AND ANALYSIS "

    ''' <summary>
    ''' Returns the analysis report.
    ''' </summary>
    ReadOnly Property AnalysisReport() As String

    ''' <summary>
    ''' Returns true if the state machine is deterministic, i.e., if each state and symbol have a unique
    ''' next state. Namely, if all transitions are unique.
    ''' </summary>
    ReadOnly Property IsDeterministic() As Boolean

    ''' <summary>
    ''' Adds a translation to the transition table.
    ''' </summary>
    ''' <param name="state">Specifies the current state.</param>
    ''' <param name="symbol">Specifies the input symbol that signals the transition.</param>
    ''' <param name="nextState">Specifies the next state.</param>
    Function AddTransition(ByVal state As IState, ByVal symbol As TSymbol, ByVal nextState As IState) As ITransition(Of TSymbol)

#End Region

#Region " INPUT SYMBOL PROCESSING "

    ''' <summary>
    ''' Traverses the transition function and transition specified for the current state and the 
    ''' input symbol.
    ''' </summary>
    ''' <param name="state">Specifies the current state.</param>
    ''' <param name="symbol">Specifies the input symbol that signals the transition.</param>
    Function SelectTransition(ByVal state As IState, ByVal symbol As TSymbol) As ITransition(Of TSymbol)

#End Region

End Interface
