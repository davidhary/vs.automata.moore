''' <summary>
''' Interface to a finite state machine.
''' </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/26/09, 2.3.3403.x">
''' created
''' </para></remarks>
Public Interface IMachine(Of TSymbol)

    Inherits IDisposable

#Region " INPUT MANAGEMENT "

    ''' <summary>
    ''' Clears the input queue. This is required in machines where both internal and external
    ''' might feed inputs.
    ''' </summary>
    Sub ClearInputs()

    ''' <summary>
    ''' Enqueues an input symbol.
    ''' </summary>
    ''' <param name="value">The input symbol to add to the input queue.</param>
    Sub EnqueueInput(ByVal value As TSymbol)

    ''' <summary>
    ''' Enqueues an input symbol.
    ''' </summary>
    ''' <param name="value">The input symbol to add to the input queue.</param>
    ''' <param name="clearFirst">Clears the queue before enqueuing the value</param>
    ''' <param name="ifEmpty">If true, the signal is queued only if the queue is empty.</param>
    Sub EnqueueInput(ByVal value As TSymbol, ByVal clearFirst As Boolean, ByVal ifEmpty As Boolean)

    ''' <summary>
    ''' Enqueues an input symbol.
    ''' </summary>
    ''' <param name="values">The input symbols to add to the input queue.</param>
    Sub EnqueueInput(ByVal values As TSymbol())

    ''' <summary>
    ''' Enqueues an input symbol.
    ''' </summary>
    ''' <param name="values">The input symbols to add to the input queue.</param>
    ''' <param name="clearFirst">Clears the queue before enqueuing the value</param>
    ''' <param name="ifEmpty">If true, the signal is queued only if the queue is empty.</param>
    Sub EnqueueInput(ByVal values As TSymbol(), ByVal clearFirst As Boolean, ByVal ifEmpty As Boolean)

    ''' <summary>
    ''' Returns true if the state machine received an input.
    ''' </summary>
    Function HasInput() As Boolean

    ''' <summary>
    ''' Gets the input without removing it from the queue.
    ''' </summary>
    Function PeekInput() As TSymbol

#End Region

#Region " EVENTS "

    ''' <summary>Occurs upon machine acknowledging receipt of a new input symbol.</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Event Acknowledge As EventHandler(Of SymbolEventArgs(Of TSymbol))

    ''' <summary>Raises the <see cref="Acknowledge">Acknowledge</see> event.</summary>
    Sub OnAcknowledge(ByVal e As SymbolEventArgs(Of TSymbol))

    ''' <summary>Occurs if the current state actions count exceeded the 
    ''' <see cref="istate.CountOutCount">count out count</see>. When detected, the 
    ''' state machine does not execute the state actions after entering the state.
    ''' </summary>
    ''' <remarks>Use this event to process count out operations.</remarks>
    Event CountOut As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the CountOut event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Sub OnCountOut(ByVal e As System.EventArgs)

    ''' <summary>Occurs after the creation of state machine.</summary>
    ''' <remarks>Use this event to notify the container class that the state machine was created.</remarks>
    Event Created As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the <see cref="Created">Created</see> event.</summary>
    Sub OnCreated(ByVal e As System.EventArgs)

    ''' <summary>
    ''' Gets or sets the condition indicating that a stop was requested.
    ''' </summary>
    Property IsStopRequested() As Boolean

    ''' <summary>Occurs upon state deactivation (exit).</summary>
    ''' <remarks>Use this event to notify the container class that a state changed.</remarks>
    Event StateChanged As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the StateChanged event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Sub OnStateChanged(ByVal e As System.EventArgs)

    ''' <summary>Occurs upon start of state machine.</summary>
    ''' <remarks>Use this event to notify the container class that the machine started.</remarks>
    Event Started As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the <see cref="Started">Started</see> event.</summary>
    Sub OnStarted(ByVal e As System.EventArgs)

    ''' <summary>Occurs upon stop of state execution.</summary>
    ''' <remarks>Use this event to notify the container class that the machine stopped.</remarks>
    Event Stopped As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the <see cref="Stopped">Stopped</see> event.</summary>
    Sub OnStopped(ByVal e As System.EventArgs)

#End Region

#Region " EXECUTION "

    ''' <summary>
    ''' Starts the state machine by <see cref="Initiate">initiating</see> the start state and 
    ''' <see cref="Sequence">repeating the sequence</see> until receiving a <see cref="RequestStop">stop request.</see>
    ''' </summary>
    Sub Activate()

    ''' <summary>
    ''' Gets or sets the time in milliseconds to wait before starting the machine cycle.
    ''' </summary>
    Property OnsetDelay() As Integer

    ''' <summary>
    ''' Specifies the state machine period.
    ''' </summary>
    Property Interval() As Integer

    ''' <summary>
    ''' Moves the state machine to the Start State. 
    ''' This action clears the input QUEUE. It leaves the machine stopped with the 
    ''' <see cref="RequestStop">stop requested sentinel</see> turned on, which is required for the 
    ''' machine to resume its actions.
    ''' </summary>
    Sub Initiate()

    ''' <summary>
    ''' Gets or sets the sentinel indicating that the number of consecutive re-entries exceeded the count out interval. 
    ''' </summary>
    Property IsCountOut() As Boolean

    ''' <summary>
    ''' Process a single input if any.  This allows running the state machine 
    ''' based on signals alone.
    ''' </summary>
    ''' <returns>True if a new state</returns>
    Function ProcessInput() As Boolean

    ''' <summary>
    ''' Immediately processes a single input ignoring the input queue. 
    ''' This allows running the state machine based on signals alone.
    ''' </summary>
    ''' <param name="value">Specifies the input to process.</param>
    ''' <returns>True if state change was implement or false if the transition is invalid of the 
    ''' change was Canceled.</returns>
    Function ProcessInput(ByVal value As TSymbol) As Boolean

    ''' <summary>
    ''' Immediately processes a series of inputs input ignoring the input queue. 
    ''' This allows running the state machine based on signals alone.
    ''' </summary>
    ''' <param name="values">Specifies the inputs to process.</param>
    ''' <returns>True if state change was implement or false if the transition is invalid of the 
    ''' change was Canceled.</returns>
    ''' <remarks> David, 04/26/09, 2.3.3403.x">
    ''' Allows makings a transition into the same state in which case the 
    ''' <see cref="IState.Leave">Leave</see> and <see cref="IState.Enter">Enter</see> events 
    ''' do not occur.
    ''' Use the <see cref="IState.Act">act</see> event to process state actions.
    ''' </para></remarks>
    Function ProcessInput(ByVal values As TSymbol()) As Boolean

    ''' <summary>
    ''' Requests the state machine to stop running after completion of the last action.
    ''' </summary>
    Sub RequestStop()

    ''' <summary>
    ''' Runs the state machine sequence until receiving <see cref="RequestStop">stop request.</see>
    ''' The sequence will run only after a previous stop requested that leaves 
    ''' <see cref="RequestStop">stop requested sentinel</see> turned on.
    ''' </summary>
    Sub Sequence()

#End Region

#Region " MACHINE CONSTRUCTION AND ANALYSIS "

    ''' <summary>
    ''' Returns the analysis report.
    ''' </summary>
    ReadOnly Property AnalysisReport() As String

    ''' <summary>
    ''' Returns true of the state machine is fully defined.
    ''' The machine is fully defined if all states have an associated transition.
    ''' </summary>
    ReadOnly Property IsFullyDefined() As Boolean?

    ''' <summary>
    ''' Returns true if the state machine is deterministic, i.e., if each state and symbol have a unique
    ''' next state. Namely, if all transitions are unique.
    ''' </summary>
    ReadOnly Property IsDeterministic() As Boolean

    ''' <summary>
    ''' Analyzes the state machine to determine if it is fully specified.
    ''' </summary>
    ''' <returns>
    ''' True if the machine is fully specified and all signals and states are unique.
    ''' </returns>
    Function Analyze() As Boolean

#End Region

#Region " STATES "

    ''' <summary>
    ''' Gets reference to the active <see cref="IState">state</see>.
    ''' </summary>
    ReadOnly Property CurrentState() As IState

    ''' <summary>
    ''' Gets the <see cref="IState.Caption">caption</see> of the active state.
    ''' </summary>
    ReadOnly Property CurrentStateCaption() As String

    ''' <summary>
    ''' Gets the <see cref="IState.UniqueId">unique id</see> of the active state.
    ''' </summary>
    ReadOnly Property CurrentStateUniqueId() As Integer

    ''' <summary>
    ''' Gets reference to the previous active <see cref="IState">state</see>.
    ''' </summary>
    ReadOnly Property PreviousState() As IState

    ''' <summary>
    ''' Sets the start state to a state other than the first state that was added.
    ''' </summary>
    Sub StartStateSetter(ByVal value As IState)

    ''' <summary>
    ''' Adds a <see cref="IState">state</see> to the <see cref="IMachine(Of TSymbol)">Moore state Machine</see>.
    ''' </summary>
    ''' <param name="uniqueName">Specifies the state name. Must be unique among all states defined
    ''' for this state machine.</param>
    ''' <param name="timeoutInterval">Specifies the timeout interval for all state operations to complete.
    ''' Specify a <see cref="System.Threading.Timeout.Infinite">infinite (-1)</see> or zero for 
    ''' an infinite timeout.</param>
    Function AddState(ByVal uniqueName As String, ByVal timeoutInterval As Integer) As IState

    ''' <summary>
    ''' Adds a <see cref="IState">state</see> to the <see cref="IMachine(Of TSymbol)">Moore state Machine</see>.
    ''' </summary>
    ''' <param name="uniqueName">Specifies the state name. Must be unique among all states defined
    ''' for this state machine.</param>
    Function AddState(ByVal uniqueName As String) As IState

#End Region

#Region " SYMBOLS "

    ''' <summary>
    ''' Adds an input symbol.
    ''' The symbol must have a unique hash code.
    ''' </summary>
    ''' <param name="value">Specifies the symbol value</param>
    Function AddSymbol(ByVal value As TSymbol) As TSymbol

#End Region

#Region " ACTION MESSAGE "

    ''' <summary>
    ''' Builds the <see cref="LastActionMessage">last action message</see>
    ''' </summary>
    Sub BuildActionMessage(ByVal format As String, ByVal ParamArray args() As Object)

    ''' <summary>
    ''' Gets the last action message.
    ''' </summary>
    Property LastActionMessage() As String

#End Region

#Region " TRANSITION FUNCTION "

    ''' <summary>
    ''' Adds a transition to the state machine.
    ''' </summary>
    Function AddTransition(ByVal state As IState, ByVal symbol As TSymbol, ByVal nextState As IState) As ITransition(Of TSymbol)

    ''' <summary>
    ''' Checks if the specified <typeparamref name="TSymbol">symbol</typeparamref> signals a valid 
    ''' transition from the current state. 
    ''' Sets the <see cref="LastActionMessage">last action message</see> if an invalid implied transition is found.
    ''' </summary>
    ''' <param name="value">The inputs <typeparamref name="TSymbol">symbol</typeparamref> which requires validation</param>
    ''' <returns>True if the symbol leads to a valid transition or False if no transition 
    ''' from the current state is defined for the current input symbol.</returns>
    Function ValidateInput(ByVal value As TSymbol) As Boolean

    ''' <summary>
    ''' Checks for the validity of all the transitions implied by the specified symbols.
    ''' Sets the <see cref="LastActionMessage">last action message</see> if an invalid implied transition is found.
    ''' </summary>
    ''' <param name="values">The inputs <typeparamref name="TSymbol">symbols</typeparamref> which requires validation</param>
    ''' <returns>Returns True if all implied transitions are valid. Returns false after detecting the first invalid implied transition.</returns>
    Function ValidateAllInputs(ByVal values As TSymbol()) As Boolean

    ''' <summary>
    ''' Checks for the validity of one of the transitions implied by the specified symbols.
    ''' Sets the <see cref="LastActionMessage">last action message</see> if an invalid implied transition is found.
    ''' </summary>
    ''' <param name="values">The inputs <typeparamref name="TSymbol">symbols</typeparamref> which requires validation</param>
    ''' <returns>Returns True if all implied transitions are valid. Returns false after detecting the first invalid implied transition.</returns>
    Function ValidateInput(ByVal values As TSymbol()) As Boolean

#End Region

End Interface
