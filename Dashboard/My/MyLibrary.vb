﻿Imports isr.Core
Imports isr.Core.ExceptionExtensions
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Automata + &HB

        Public Const AssemblyTitle As String = "Finite Automata Dashboard Library"
        Public Const AssemblyDescription As String = "Finite Automata Dashboard Library"
        Public Const AssemblyProduct As String = "Finite.Automata.Dashboard"

    End Class

End Namespace


