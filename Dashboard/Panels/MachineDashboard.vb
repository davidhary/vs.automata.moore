Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.IO
Imports isr.Automata.Finite.Dashboard.ExceptionExtensions
''' <summary> A machine dashboard. </summary>
''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-09-14 </para></remarks>
Public Class MachineDashboard
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTORES "

    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        Me._DefaultSenderButton = Me._MachineInfoButton

        Me.Enabled = False
        ' https://stackoverflow.com/questions/2646606/how-do-i-reclaim-the-space-from-the-grip
        ' The padding is broken. MS won't fix this.
        With Me._StateStatusStrip
            .SizingGrip = False
            .Padding = New Padding(.Padding.Left, .Padding.Top, .Padding.Left, .Padding.Bottom)
        End With
        With Me._SignalStatusStrip
            .SizingGrip = False
            .Padding = New Padding(.Padding.Left, .Padding.Top, .Padding.Left, .Padding.Bottom)
        End With
        With Me._NameStatusStrip
            .SizingGrip = False
            .Padding = New Padding(.Padding.Left, .Padding.Top, .Padding.Left, .Padding.Bottom)
        End With
    End Sub
    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                                                   release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me._Machine = Nothing
                If Me.components IsNot Nothing Then Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " MACHINE LISTENER "

    ''' <summary> Gets or sets the percent progress tool tip text. </summary>
    ''' <value> The percent progress tool tip text. </value>
    <Category("Appearance"), Description("Sets the tool tip on the percent progress label."), DefaultValue("Percent progress")>
    Public Property PercentProgressToolTipText As String
        Get
            Return Me._PercentProgressLabel.ToolTipText
        End Get
        Set(value As String)
            Me._PercentProgressLabel.ToolTipText = value
        End Set
    End Property

    Private WithEvents _Machine As IMachine
    ''' <summary> Gets or sets the machine. </summary>
    ''' <value> The machine. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)>
    Public Property Machine As IMachine
        Get
            Return Me._Machine
        End Get
        Set(value As IMachine)
            Me.Enabled = value IsNot Nothing
            Me._Machine = value
            Me.RefreshDisplay(value)
        End Set
    End Property

    ''' <summary> Gets or sets the default sender button. </summary>
    ''' <value> The default sender button. </value>
    Private ReadOnly Property DefaultSenderButton As System.Windows.Forms.ToolStripItem
    Private Sub OnInitiated(sender As IMachine)
        If sender Is Nothing Then Return
        Me._StopButton.Enabled = True
        Me._StopMenuItem.Enabled = True
        Me._CancelMenuItem.Enabled = True
        Me._NextButton.Enabled = sender.HasInput
        Me._ImmediateStopOptionsButton.Enabled = True
        Me.UpdateStartPauseButton(sender)
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Machine_Initiated(sender As Object, e As EventArgs) Handles _Machine.Initiated
        Dim automaton As IMachine = TryCast(sender, IMachine)
        If automaton Is Nothing OrElse e Is Nothing Then Return
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, EventArgs)(AddressOf Me.Machine_Initiated), New Object() {sender, e})
            Else
                Me.OnInitiated(automaton)
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(Me.DefaultSenderButton, Core.Forma.InfoProviderLevel.Error, $"Exception handling the Initiated event;. {ex.ToFullBlownString}")

        End Try
    End Sub

    Private Sub OnStopped(sender As IMachine)
        If sender Is Nothing Then Return
        Me._StopButton.Enabled = False
        Me._StopMenuItem.Enabled = False
        Me._CancelMenuItem.Enabled = False
        Me._NextButton.Enabled = False
        Me._ImmediateStopOptionsButton.Enabled = False
        Me.UpdateStartPauseButton(sender)
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Machine_Stopped(sender As Object, e As EventArgs) Handles _Machine.Stopped
        Dim automaton As IMachine = TryCast(sender, IMachine)
        If automaton Is Nothing OrElse e Is Nothing Then Return
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, EventArgs)(AddressOf Me.Machine_Stopped), New Object() {sender, e})
            Else
                Me.OnStopped(automaton)
            End If
        Catch ex As Exception

            Me.InfoProvider.Annunciate(Me.DefaultSenderButton, Core.Forma.InfoProviderLevel.Error, $"Exception handling the Stopped event;. {ex.ToFullBlownString}")
        End Try
    End Sub

    Private Sub OnFailed(sender As IMachine, e As ErrorEventArgs)
        If sender Is Nothing Then Return
        Me._FailureCountTextBox.Text = sender.ConsecutiveFailureCount.ToString
        Me.InfoProvider.Annunciate(Me.DefaultSenderButton, Core.Forma.InfoProviderLevel.Error, $"Failure reported;. {e.GetException.ToFullBlownString}")
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Machine_Failed(sender As Object, e As ErrorEventArgs) Handles _Machine.Failed
        Dim automaton As IMachine = TryCast(sender, IMachine)

        If automaton Is Nothing OrElse e Is Nothing Then Return
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, ErrorEventArgs)(AddressOf Me.Machine_Failed), New Object() {sender, e})
            Else
                Me.OnFailed(automaton, e)

            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(Me.DefaultSenderButton, Core.Forma.InfoProviderLevel.Error,
                                       $"Exception handling the Failed event;. {ex.ToFullBlownString}")
        End Try
    End Sub

    Private Sub RefreshDisplay(ByVal sender As IMachine)
        If sender IsNot Nothing Then
            If sender.IsRunning Then
                Me.OnInitiated(sender)
            Else
                Me.OnStopped(sender)
            End If
        End If
        If sender Is Nothing Then
            Me._MachineNameLabel.Text = "<name>"
            Me._FailureCountTextBox.Text = "0"
            Me._PercentProgressLabel.UpdateProgress(0)
        Else
            Me._MachineNameLabel.Text = sender?.Name
            Me._FailureCountTextBox.Text = sender.ConsecutiveFailureCount.ToString
            Me._PercentProgressLabel.UpdateProgress(sender.PercentProgress)
        End If
        Me.UpdateStartPauseButton(sender)
        Me.UpdateSignal(sender)
        Me.UpdateStateProgress(sender)
    End Sub

    Private Sub UpdateStateProgress(ByVal sender As IEngine)
        If sender Is Nothing Then
            Me._PreviousStateTextBox.Text = "<previous state>"
            Me._StateProgressLabel.UpdateProgress(0, "<state>")
        Else
            Me._PreviousStateTextBox.Text = sender.PreviousStateName
            Me._StateProgressLabel.UpdateProgress(sender.ProgressIndicatorPercent, sender.CurrentStateName)
        End If
    End Sub

    Private Sub UpdateSignal(ByVal sender As IMachine)
        If sender Is Nothing Then
            Me._NextButton.Enabled = False
            Me._NextButton.Text = "|"
            Me._NextInputSymbolLabel.Text = "<symbol>"
        Else
            Dim input As String = String.Empty
            Dim hasInput As Boolean = False
            If Me.Machine.HasInput Then
                hasInput = True
                input = sender.NextSymbolName
            End If
            hasInput = hasInput AndAlso Not String.IsNullOrWhiteSpace(input)
            Me._NextButton.Enabled = hasInput AndAlso sender.IsPauseRequested
            Me._NextInputSymbolLabel.Text = input
            Me._NextButton.Text = If(Me._NextButton.Enabled, ">", "|")
        End If
    End Sub

    Private Sub UpdateStartPauseButton(ByVal sender As IMachine)
        If sender Is Nothing Then
        ElseIf sender.IsRunning Then
        Else
        End If
        If sender Is Nothing OrElse Not sender.IsRunning Then
            Me._StartPauseButton.Enabled = False
            Me._StartPauseButton.Image = My.Resources.Media_playback_start_8
        ElseIf sender.IsPauseRequested Then
            Me._StartPauseButton.Enabled = sender.IsRunning
            Me._StartPauseButton.Image = My.Resources.Media_playback_start_8
            Me._StartPauseButton.ToolTipText = "Resume"
        Else
            Me._StartPauseButton.Enabled = sender.IsRunning
            Me._StartPauseButton.Image = My.Resources.Media_playback_pause_8
            Me._StartPauseButton.ToolTipText = "Pause"
        End If
    End Sub

    Private Sub OnPropertyChanged(ByVal sender As IMachine, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(IMachine.FailureStatus)
                Me.InfoProvider.Clear()
                Select Case sender.FailureStatus
                    Case FailureStatus.SequenceError
                        Me._SequenceStatusAnnunciatorButton.Image = My.Resources.Dialog_warning_panel
                        Me._SequenceStatusAnnunciatorButton.ToolTipText = "Sequence Error"
                    Case FailureStatus.SequenceException
                        Me._SequenceStatusAnnunciatorButton.Image = My.Resources.Exclamation
                        Me._SequenceStatusAnnunciatorButton.ToolTipText = "Sequence Exception"
                    Case Else
                        If String.IsNullOrWhiteSpace(sender.FailureDetails) OrElse
                            String.Equals(sender.FailureDetails, Me.AnnunciatedMessage) Then
                            Me._SequenceStatusAnnunciatorButton.Image = My.Resources.Dialog_clean
                            Me._SequenceStatusAnnunciatorButton.ToolTipText = "Sequence Okay"
                        Else
                            Me._SequenceStatusAnnunciatorButton.Image = My.Resources.Dialog_information
                            Me._SequenceStatusAnnunciatorButton.ToolTipText = "Message available"
                        End If
                End Select
            Case NameOf(IMachine.HasInput)
                Me.UpdateSignal(sender)
            Case NameOf(IMachine.IsPauseRequested)
                Me.UpdateStartPauseButton(sender)
                If sender.IsPauseRequested Then Me.UpdateSignal(sender)
            Case NameOf(IMachine.ConsecutiveFailureCount)
                Me._FailureCountTextBox.Text = sender.ConsecutiveFailureCount.ToString
            Case NameOf(IMachine.PercentProgress)
                Me._PercentProgressLabel.UpdateProgress(sender.PercentProgress)
            Case NameOf(IMachine.ProgressIndicatorPercent)
                Me.UpdateStateProgress(sender)
                Me.UpdateSignal(sender)
        End Select
    End Sub


    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Machine_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _Machine.PropertyChanged
        Dim automaton As IMachine = TryCast(sender, IMachine)
        If automaton Is Nothing OrElse e Is Nothing Then Return
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.Machine_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(automaton, e.PropertyName)

            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(Me._DefaultSenderButton, Core.Forma.InfoProviderLevel.Error, $"Exception handling the Failed event;. {ex.ToFullBlownString}")
        End Try
    End Sub

    Private Const _NothingToReport As String = "Nothing to report"
    Private _AnnunciatedMessage As String
    Private Property AnnunciatedMessage As String
        Get
            Return Me._AnnunciatedMessage
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.AnnunciatedMessage) Then
                Me._AnnunciatedMessage = value
                Me.InfoProvider.Annunciate(Me._SequenceStatusAnnunciatorButton, Core.Forma.InfoProviderLevel.Info, value)
                Me._SequenceStatusAnnunciatorButton.Image = My.Resources.Dialog_clean
                Me._SequenceStatusAnnunciatorButton.ToolTipText = "Observed"
                If Not String.Equals(value, MachineDashboard._NothingToReport) Then
                    Clipboard.SetText(value)
                End If
            End If
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FailureAnnunciatorButton_Click(sender As Object, e As EventArgs) Handles _SequenceStatusAnnunciatorButton.Click
        Try
            Me.InfoProvider.Clear()
            Me.AnnunciatedMessage = If(String.IsNullOrWhiteSpace(Me.Machine.FailureDetails), MachineDashboard._NothingToReport, Me.Machine.FailureDetails)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(Me._DefaultSenderButton, Core.Forma.InfoProviderLevel.Error,
                                       $"Exception handling the Failure annunciator click event;. {ex.ToFullBlownString}")
        End Try
    End Sub

#End Region

#Region " MACHINE CONTROL "

    Private Sub NextButton_Click(sender As Object, e As EventArgs) Handles _NextButton.Click
        Me.Machine.TryProcessInput()
    End Sub

    Private Sub StartPauseButton_Click(sender As Object, e As EventArgs) Handles _StartPauseButton.Click
        If Me.Machine.IsPauseRequested Then
            Me.Machine.ResumePause()
        Else
            Me.Machine.RequestPause()
        End If

    End Sub

    Private Sub StopButton_Click(sender As Object, e As EventArgs) Handles _StopButton.Click
        Me.Machine.EnqueueStopSymbol()
    End Sub

    Private Sub StopMenuItem_Click(sender As Object, e As EventArgs) Handles _StopMenuItem.Click
        Me.Machine.RequestStop()
    End Sub

    Private Sub CancelMenuItem_Click(sender As Object, e As EventArgs) Handles _CancelMenuItem.Click

        Me.Machine.RequestCancellation()
    End Sub


#End Region

End Class
