﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MachineDashboard

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._StateStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._StateProgressLabel = New isr.Core.Controls.ToolStripProgressLabel()
        Me._SignalStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._NextInputSymbolLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._PercentProgressLabel = New isr.Core.Controls.ToolStripProgressLabel()
        Me._MachineToolStrip = New System.Windows.Forms.ToolStrip()
        Me._MachineInfoButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._FailureCountTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._PreviousStateTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._NextButton = New System.Windows.Forms.ToolStripButton()
        Me._StartPauseButton = New System.Windows.Forms.ToolStripButton()
        Me._StopButton = New System.Windows.Forms.ToolStripButton()
        Me._ImmediateStopOptionsButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._CancelMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._StopMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SequenceStatusAnnunciatorButton = New System.Windows.Forms.ToolStripButton()
        Me._NameStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._MachineNameLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._StateStatusStrip.SuspendLayout()
        Me._SignalStatusStrip.SuspendLayout()
        Me._MachineToolStrip.SuspendLayout()
        Me._NameStatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_StateStatusStrip
        '
        Me._StateStatusStrip.AutoSize = False
        Me._StateStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StateProgressLabel})
        Me._StateStatusStrip.Location = New System.Drawing.Point(0, 52)
        Me._StateStatusStrip.Name = "_StateStatusStrip"
        Me._StateStatusStrip.ShowItemToolTips = True
        Me._StateStatusStrip.Size = New System.Drawing.Size(163, 16)
        Me._StateStatusStrip.SizingGrip = False
        Me._StateStatusStrip.TabIndex = 0
        Me._StateStatusStrip.Text = "State Status"
        '
        '_StateProgressLabel
        '
        Me._StateProgressLabel.BarColor = System.Drawing.Color.Aquamarine
        Me._StateProgressLabel.Margin = New System.Windows.Forms.Padding(1)
        Me._StateProgressLabel.Name = "_StateProgressLabel"
        Me._StateProgressLabel.Size = New System.Drawing.Size(146, 14)
        Me._StateProgressLabel.Spring = True
        Me._StateProgressLabel.Text = "<state>"
        Me._StateProgressLabel.ToolTipText = "Current state"
        '
        '_SignalStatusStrip
        '
        Me._SignalStatusStrip.AutoSize = False
        Me._SignalStatusStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me._SignalStatusStrip.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SignalStatusStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SignalStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._NextInputSymbolLabel, Me._PercentProgressLabel})
        Me._SignalStatusStrip.Location = New System.Drawing.Point(0, 16)
        Me._SignalStatusStrip.Name = "_SignalStatusStrip"
        Me._SignalStatusStrip.ShowItemToolTips = True
        Me._SignalStatusStrip.Size = New System.Drawing.Size(163, 16)
        Me._SignalStatusStrip.SizingGrip = False
        Me._SignalStatusStrip.TabIndex = 1
        Me._SignalStatusStrip.Text = "Signal Status Strip"
        '
        '_NextInputSymbolLabel
        '
        Me._NextInputSymbolLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._NextInputSymbolLabel.Name = "_NextInputSymbolLabel"
        Me._NextInputSymbolLabel.Size = New System.Drawing.Size(96, 16)
        Me._NextInputSymbolLabel.Spring = True
        Me._NextInputSymbolLabel.Text = "<input>"
        Me._NextInputSymbolLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._NextInputSymbolLabel.ToolTipText = "Next input symbol"
        '
        '_PercentProgressLabel
        '
        Me._PercentProgressLabel.AutoSize = False
        Me._PercentProgressLabel.BackColor = System.Drawing.Color.Transparent
        Me._PercentProgressLabel.BarColor = System.Drawing.SystemColors.Highlight
        Me._PercentProgressLabel.Margin = New System.Windows.Forms.Padding(1)
        Me._PercentProgressLabel.Name = "_PercentProgressLabel"
        Me._PercentProgressLabel.Size = New System.Drawing.Size(50, 14)
        Me._PercentProgressLabel.Text = "<%>"
        '
        '_MachineToolStrip
        '
        Me._MachineToolStrip.AutoSize = False
        Me._MachineToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._MachineToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._MachineInfoButton, Me._NextButton, Me._StartPauseButton, Me._StopButton, Me._ImmediateStopOptionsButton, Me._SequenceStatusAnnunciatorButton})
        Me._MachineToolStrip.Location = New System.Drawing.Point(0, 32)
        Me._MachineToolStrip.Name = "_MachineToolStrip"
        Me._MachineToolStrip.Padding = New System.Windows.Forms.Padding(0)
        Me._MachineToolStrip.Size = New System.Drawing.Size(163, 20)
        Me._MachineToolStrip.TabIndex = 2
        Me._MachineToolStrip.Text = "ToolStrip1"
        '
        '_MachineInfoButton
        '
        Me._MachineInfoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._MachineInfoButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FailureCountTextBox, Me._PreviousStateTextBox})
        Me._MachineInfoButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MachineInfoButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._MachineInfoButton.Margin = New System.Windows.Forms.Padding(1)
        Me._MachineInfoButton.Name = "_MachineInfoButton"
        Me._MachineInfoButton.Size = New System.Drawing.Size(26, 18)
        Me._MachineInfoButton.Text = "i"
        Me._MachineInfoButton.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me._MachineInfoButton.ToolTipText = "Additional info"
        '
        '_FailureCountTextBox
        '
        Me._FailureCountTextBox.Name = "_FailureCountTextBox"
        Me._FailureCountTextBox.Size = New System.Drawing.Size(100, 23)
        Me._FailureCountTextBox.ToolTipText = "Failure count"
        '
        '_PreviousStateTextBox
        '
        Me._PreviousStateTextBox.Name = "_PreviousStateTextBox"
        Me._PreviousStateTextBox.Size = New System.Drawing.Size(100, 23)
        '
        '_NextButton
        '
        Me._NextButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._NextButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._NextButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._NextButton.Margin = New System.Windows.Forms.Padding(1)
        Me._NextButton.Name = "_NextButton"
        Me._NextButton.Size = New System.Drawing.Size(23, 18)
        Me._NextButton.Text = ">"
        Me._NextButton.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me._NextButton.ToolTipText = "Enqueue next symbol"
        '
        '_StartPauseButton
        '
        Me._StartPauseButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._StartPauseButton.Image = Global.isr.Automata.Finite.Dashboard.My.Resources.Resources.media_playback_start_8
        Me._StartPauseButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._StartPauseButton.Margin = New System.Windows.Forms.Padding(1)
        Me._StartPauseButton.Name = "_StartPauseButton"
        Me._StartPauseButton.Size = New System.Drawing.Size(23, 18)
        Me._StartPauseButton.Text = "Start"
        '
        '_StopButton
        '
        Me._StopButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._StopButton.Image = Global.isr.Automata.Finite.Dashboard.My.Resources.Resources.media_playback_stop_8
        Me._StopButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._StopButton.Margin = New System.Windows.Forms.Padding(1)
        Me._StopButton.Name = "_StopButton"
        Me._StopButton.Size = New System.Drawing.Size(23, 18)
        Me._StopButton.Text = "Stop"
        '
        '_ImmediateStopOptionsButton
        '
        Me._ImmediateStopOptionsButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._ImmediateStopOptionsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ImmediateStopOptionsButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._CancelMenuItem, Me._StopMenuItem})
        Me._ImmediateStopOptionsButton.Image = Global.isr.Automata.Finite.Dashboard.My.Resources.Resources._stop
        Me._ImmediateStopOptionsButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ImmediateStopOptionsButton.Margin = New System.Windows.Forms.Padding(1)
        Me._ImmediateStopOptionsButton.Name = "_ImmediateStopOptionsButton"
        Me._ImmediateStopOptionsButton.Size = New System.Drawing.Size(32, 18)
        Me._ImmediateStopOptionsButton.Text = "Immediate Stop Options"
        Me._ImmediateStopOptionsButton.ToolTipText = "Select immediate stops"
        '
        '_CancelMenuItem
        '
        Me._CancelMenuItem.Name = "_CancelMenuItem"
        Me._CancelMenuItem.Size = New System.Drawing.Size(152, 22)
        Me._CancelMenuItem.Text = "Cancel"
        Me._CancelMenuItem.ToolTipText = "Request an immediate cancel"
        '
        '_StopMenuItem
        '
        Me._StopMenuItem.Name = "_StopMenuItem"
        Me._StopMenuItem.Size = New System.Drawing.Size(152, 22)
        Me._StopMenuItem.Text = "Stop"
        Me._StopMenuItem.ToolTipText = "Requests an immediate stop"
        '
        '_SequenceStatusAnnunciatorButton
        '
        Me._SequenceStatusAnnunciatorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SequenceStatusAnnunciatorButton.Image = Global.isr.Automata.Finite.Dashboard.My.Resources.Resources.dialog_clean
        Me._SequenceStatusAnnunciatorButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SequenceStatusAnnunciatorButton.Name = "_SequenceStatusAnnunciatorButton"
        Me._SequenceStatusAnnunciatorButton.Size = New System.Drawing.Size(23, 20)
        Me._SequenceStatusAnnunciatorButton.Text = "Sequence Status"
        '
        '_NameStatusStrip
        '
        Me._NameStatusStrip.AutoSize = False
        Me._NameStatusStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me._NameStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._MachineNameLabel})
        Me._NameStatusStrip.Location = New System.Drawing.Point(0, 0)
        Me._NameStatusStrip.Name = "_NameStatusStrip"
        Me._NameStatusStrip.ShowItemToolTips = True
        Me._NameStatusStrip.Size = New System.Drawing.Size(163, 16)
        Me._NameStatusStrip.SizingGrip = False
        Me._NameStatusStrip.TabIndex = 3
        Me._NameStatusStrip.Text = "Name Status Strip"
        '
        '_MachineNameLabel
        '
        Me._MachineNameLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._MachineNameLabel.Name = "_MachineNameLabel"
        Me._MachineNameLabel.Size = New System.Drawing.Size(53, 16)
        Me._MachineNameLabel.Text = "<name>"
        Me._MachineNameLabel.ToolTipText = "Automaton name"
        '
        'MachineDashboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._MachineToolStrip)
        Me.Controls.Add(Me._SignalStatusStrip)
        Me.Controls.Add(Me._StateStatusStrip)
        Me.Controls.Add(Me._NameStatusStrip)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "MachineDashboard"
        Me.Size = New System.Drawing.Size(163, 68)
        Me._StateStatusStrip.ResumeLayout(False)
        Me._StateStatusStrip.PerformLayout()
        Me._SignalStatusStrip.ResumeLayout(False)
        Me._SignalStatusStrip.PerformLayout()
        Me._MachineToolStrip.ResumeLayout(False)
        Me._MachineToolStrip.PerformLayout()
        Me._NameStatusStrip.ResumeLayout(False)
        Me._NameStatusStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _StateStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _SignalStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _MachineToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _MachineInfoButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _NextInputSymbolLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _FailureCountTextBox As Windows.Forms.ToolStripTextBox
    Private WithEvents _PreviousStateTextBox As Windows.Forms.ToolStripTextBox
    Private WithEvents _NextButton As Windows.Forms.ToolStripButton
    Private WithEvents _StartPauseButton As Windows.Forms.ToolStripButton
    Private WithEvents _StopButton As Windows.Forms.ToolStripButton
    Private WithEvents _ImmediateStopOptionsButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _CancelMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _StopMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _NameStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _MachineNameLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _PercentProgressLabel As Core.Controls.ToolStripProgressLabel
    Private WithEvents _StateProgressLabel As Core.Controls.ToolStripProgressLabel
    Private WithEvents _SequenceStatusAnnunciatorButton As Windows.Forms.ToolStripButton
End Class
