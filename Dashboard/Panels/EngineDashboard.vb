Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.IO
Imports isr.Automata.Finite.Dashboard.ExceptionExtensions
''' <summary> A Engine dashboard. </summary>
''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-09-14 </para></remarks>
Public Class EngineDashboard
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTORES "

    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        Me._DefaultSenderButton = Me._EngineInfoButton

        Me.Enabled = False
        ' https://stackoverflow.com/questions/2646606/how-do-i-reclaim-the-space-from-the-grip
        ' The padding is broken. MS won't fix this.
        With Me._StateStatusStrip
            .SizingGrip = False
            .Padding = New Padding(.Padding.Left, .Padding.Top, .Padding.Left, .Padding.Bottom)
        End With
        With Me._SignalStatusStrip
            .SizingGrip = False
            .Padding = New Padding(.Padding.Left, .Padding.Top, .Padding.Left, .Padding.Bottom)
        End With
        With Me._NameStatusStrip
            .SizingGrip = False
            .Padding = New Padding(.Padding.Left, .Padding.Top, .Padding.Left, .Padding.Bottom)
        End With
    End Sub
    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                                                   release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me._Engine = Nothing
                If Me.components IsNot Nothing Then Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " ENGINE LISTENER "

    ''' <summary> Gets or sets the percent progress tool tip text. </summary>
    ''' <value> The percent progress tool tip text. </value>
    <Category("Appearance"), Description("Sets the tool tip on the percent progress label."), DefaultValue("Percent progress")>
    Public Property PercentProgressToolTipText As String
        Get
            Return Me._PercentProgressLabel.ToolTipText
        End Get
        Set(value As String)
            Me._PercentProgressLabel.ToolTipText = value
        End Set
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Engine As IEngine
#Enable Warning IDE1006 ' Naming Styles
    ''' <summary> Gets or sets the Engine. </summary>
    ''' <value> The Engine. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)>
    Public Property Engine As IEngine
        Get
            Return Me._Engine
        End Get
        Set(value As IEngine)
            Me.Enabled = value IsNot Nothing
            Me._Engine = value
            Me.RefreshDisplay(value)
        End Set
    End Property

    ''' <summary> Gets or sets the default sender button. </summary>
    ''' <value> The default sender button. </value>
    Private ReadOnly Property DefaultSenderButton As System.Windows.Forms.ToolStripItem

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Engine_Initiated(sender As Object, e As EventArgs) Handles _Engine.Initiated
        Dim automaton As IEngine = TryCast(sender, IEngine)
        If automaton Is Nothing OrElse e Is Nothing Then Return
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, EventArgs)(AddressOf Me.Engine_Initiated), New Object() {sender, e})
            Else
                Me.RefreshDisplay(automaton)
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(Me.DefaultSenderButton, Core.Forma.InfoProviderLevel.Error, $"Exception handling the Initiated event;. {ex.ToFullBlownString}")

        End Try
    End Sub

    Private Sub OnFailed(sender As IEngine, e As ErrorEventArgs)
        If sender Is Nothing Then Return
        Me._FailureCountTextBox.Text = sender.ConsecutiveFailureCount.ToString
        Me.InfoProvider.Annunciate(Me.DefaultSenderButton, Core.Forma.InfoProviderLevel.Error, $"Failure reported;. {e.GetException.ToFullBlownString}")
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Engine_Failed(sender As Object, e As ErrorEventArgs) Handles _Engine.Failed
        Dim automaton As IEngine = TryCast(sender, IEngine)

        If automaton Is Nothing OrElse e Is Nothing Then Return
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, ErrorEventArgs)(AddressOf Me.Engine_Failed), New Object() {sender, e})
            Else
                Me.OnFailed(automaton, e)
            End If

        Catch ex As Exception
            Me.InfoProvider.Annunciate(Me.DefaultSenderButton, Core.Forma.InfoProviderLevel.Error, $"Exception handling the Failed event;. {ex.ToFullBlownString}")
        End Try
    End Sub

    Private Sub RefreshDisplay(ByVal sender As IEngine)
        If sender Is Nothing Then
            Me._EngineNameLabel.Text = "<name>"
            Me._FailureCountTextBox.Text = "0"
            Me._PercentProgressLabel.UpdateProgress(-1)
        Else
            Me._EngineNameLabel.Text = sender.Name
            Me._FailureCountTextBox.Text = sender.ConsecutiveFailureCount.ToString
            Me._PercentProgressLabel.UpdateProgress(sender.PercentProgress)
        End If
        Me.UpdateStateProgress(sender)
        Me.UpdateInput(sender)
    End Sub

    Private Sub UpdateInput(ByVal sender As IEngine)
        Me._LastInputSymbolLabel.Text = If(sender Is Nothing, "<input>", sender.LastInputSymbolName)
    End Sub

    Private Sub UpdateStateProgress(ByVal sender As IEngine)
        If sender Is Nothing Then
            Me._PreviousStateTextBox.Text = "<previous state>"
            Me._StateProgressLabel.UpdateProgress(0, "<state>")
        Else
            Me._PreviousStateTextBox.Text = sender.PreviousStateName
            Me._StateProgressLabel.UpdateProgress(sender.ProgressIndicatorPercent, sender.CurrentStateName)
        End If
    End Sub

    Private Sub OnPropertyChanged(ByVal sender As IEngine, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(IEngine.FailureStatus)
                Me.InfoProvider.Clear()
                Select Case sender.FailureStatus
                    Case FailureStatus.SequenceError
                        Me._SequenceStatusAnnunciatorButton.Image = My.Resources.Dialog_warning_panel
                        Me._SequenceStatusAnnunciatorButton.ToolTipText = "Sequence Error"

                    Case FailureStatus.SequenceException
                        Me._SequenceStatusAnnunciatorButton.Image = My.Resources.Exclamation
                        Me._SequenceStatusAnnunciatorButton.ToolTipText = "Sequence Exception"
                    Case Else
                        If String.IsNullOrWhiteSpace(sender.FailureDetails) OrElse
                            String.Equals(sender.FailureDetails, Me.AnnunciatedMessage) Then
                            Me._SequenceStatusAnnunciatorButton.Image = My.Resources.Dialog_clean
                            Me._SequenceStatusAnnunciatorButton.ToolTipText = "Sequence Okay"
                        Else
                            Me._SequenceStatusAnnunciatorButton.Image = My.Resources.Dialog_information
                            Me._SequenceStatusAnnunciatorButton.ToolTipText = "Message available"
                        End If
                End Select
            Case NameOf(IEngine.ConsecutiveFailureCount)
                Me._FailureCountTextBox.Text = sender.ConsecutiveFailureCount.ToString
            Case NameOf(IEngine.LastInputSymbolName)
                Me.UpdateInput(sender)
            Case NameOf(IEngine.PercentProgress)
                Me._PercentProgressLabel.UpdateProgress(sender.PercentProgress)
            Case NameOf(IEngine.ProgressIndicatorPercent)
                Me.UpdateStateProgress(sender)
        End Select
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Engine_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _Engine.PropertyChanged
        Dim automaton As IEngine = TryCast(sender, IEngine)
        If automaton Is Nothing OrElse e Is Nothing Then Return
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.Engine_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(automaton, e.PropertyName)
            End If

        Catch ex As Exception
            Me.InfoProvider.Annunciate(Me._DefaultSenderButton, Core.Forma.InfoProviderLevel.Error,
                                       $"Exception handling the Failed event;. {ex.ToFullBlownString}")
        End Try
    End Sub

    Private Const _NothingToReport As String = "Nothing to report"
    Private _AnnunciatedMessage As String
    Private Property AnnunciatedMessage As String
        Get
            Return Me._AnnunciatedMessage
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.AnnunciatedMessage) Then
                Me._AnnunciatedMessage = value
                Me.InfoProvider.Annunciate(Me._SequenceStatusAnnunciatorButton, Core.Forma.InfoProviderLevel.Info, value)
                Me._SequenceStatusAnnunciatorButton.Image = My.Resources.Dialog_clean
                Me._SequenceStatusAnnunciatorButton.ToolTipText = "Observed"
                If Not String.Equals(value, EngineDashboard._NothingToReport) Then
                    Clipboard.SetText(value)
                End If
            End If
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FailureAnnunciatorButton_Click(sender As Object, e As EventArgs) Handles _SequenceStatusAnnunciatorButton.Click
        Try
            Me.InfoProvider.Clear()
            Me.AnnunciatedMessage = If(String.IsNullOrWhiteSpace(Me.Engine.FailureDetails), EngineDashboard._NothingToReport, Me.Engine.FailureDetails)

        Catch ex As Exception
            Me.InfoProvider.Annunciate(Me._DefaultSenderButton, Core.Forma.InfoProviderLevel.Error,
                                       $"Exception handling the Failure annunciator click event;. {ex.ToFullBlownString}")
        End Try
    End Sub

#End Region

End Class
