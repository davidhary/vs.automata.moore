﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class EngineMonitor

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._StateStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._StateProgressLabel = New isr.Core.Controls.ToolStripProgressLabel()
        Me._SignalStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._LastInputSymbolLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._PercentProgressLabel = New isr.Core.Controls.ToolStripProgressLabel()
        Me._EngineToolStrip = New System.Windows.Forms.ToolStrip()
        Me._EngineInfoButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._FailureCountTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._PreviousStateTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._SequenceStatusAnnunciatorButton = New System.Windows.Forms.ToolStripButton()
        Me._NameStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._EngineNameLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._StateStatusStrip.SuspendLayout()
        Me._SignalStatusStrip.SuspendLayout()
        Me._EngineToolStrip.SuspendLayout()
        Me._NameStatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_StateStatusStrip
        '
        Me._StateStatusStrip.AutoSize = False
        Me._StateStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StateProgressLabel})
        Me._StateStatusStrip.Location = New System.Drawing.Point(0, 52)
        Me._StateStatusStrip.Name = "_StateStatusStrip"
        Me._StateStatusStrip.ShowItemToolTips = True
        Me._StateStatusStrip.Size = New System.Drawing.Size(163, 16)
        Me._StateStatusStrip.SizingGrip = False
        Me._StateStatusStrip.TabIndex = 0
        Me._StateStatusStrip.Text = "State status"
        '
        '_StateProgressLabel
        '
        Me._StateProgressLabel.BarColor = System.Drawing.Color.Aquamarine
        Me._StateProgressLabel.Margin = New System.Windows.Forms.Padding(1)
        Me._StateProgressLabel.Name = "_StateProgressLabel"
        Me._StateProgressLabel.Size = New System.Drawing.Size(146, 14)
        Me._StateProgressLabel.Spring = True
        Me._StateProgressLabel.Text = "<state>"
        Me._StateProgressLabel.ToolTipText = "Current state"
        '
        '_SignalStatusStrip
        '
        Me._SignalStatusStrip.AutoSize = False
        Me._SignalStatusStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me._SignalStatusStrip.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SignalStatusStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SignalStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LastInputSymbolLabel, Me._PercentProgressLabel})
        Me._SignalStatusStrip.Location = New System.Drawing.Point(0, 16)
        Me._SignalStatusStrip.Name = "_SignalStatusStrip"
        Me._SignalStatusStrip.ShowItemToolTips = True
        Me._SignalStatusStrip.Size = New System.Drawing.Size(163, 16)
        Me._SignalStatusStrip.SizingGrip = False
        Me._SignalStatusStrip.TabIndex = 1
        Me._SignalStatusStrip.Text = "Signal Status Strip"
        '
        '_LastInputSymbolLabel
        '
        Me._LastInputSymbolLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._LastInputSymbolLabel.Name = "_LastInputSymbolLabel"
        Me._LastInputSymbolLabel.Size = New System.Drawing.Size(96, 16)
        Me._LastInputSymbolLabel.Spring = True
        Me._LastInputSymbolLabel.Text = "<input>"
        Me._LastInputSymbolLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._LastInputSymbolLabel.ToolTipText = "Last input symbol"
        '
        '_PercentProgressLabel
        '
        Me._PercentProgressLabel.AutoSize = False
        Me._PercentProgressLabel.BackColor = System.Drawing.Color.Transparent
        Me._PercentProgressLabel.BarColor = System.Drawing.SystemColors.Highlight
        Me._PercentProgressLabel.Margin = New System.Windows.Forms.Padding(1)
        Me._PercentProgressLabel.Name = "_PercentProgressLabel"
        Me._PercentProgressLabel.Size = New System.Drawing.Size(50, 14)
        Me._PercentProgressLabel.Text = "<%>"
        Me._PercentProgressLabel.ToolTipText = "Percent progress"
        '
        '_EngineToolStrip
        '
        Me._EngineToolStrip.AutoSize = False
        Me._EngineToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._EngineToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._EngineInfoButton, Me._SequenceStatusAnnunciatorButton})
        Me._EngineToolStrip.Location = New System.Drawing.Point(0, 32)
        Me._EngineToolStrip.Name = "_EngineToolStrip"
        Me._EngineToolStrip.Padding = New System.Windows.Forms.Padding(0)
        Me._EngineToolStrip.Size = New System.Drawing.Size(163, 20)
        Me._EngineToolStrip.TabIndex = 2
        Me._EngineToolStrip.Text = "ToolStrip1"
        '
        '_EngineInfoButton
        '
        Me._EngineInfoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._EngineInfoButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FailureCountTextBox, Me._PreviousStateTextBox})
        Me._EngineInfoButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EngineInfoButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._EngineInfoButton.Margin = New System.Windows.Forms.Padding(1)
        Me._EngineInfoButton.Name = "_EngineInfoButton"
        Me._EngineInfoButton.Size = New System.Drawing.Size(26, 18)
        Me._EngineInfoButton.Text = "i"
        Me._EngineInfoButton.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me._EngineInfoButton.ToolTipText = "Additional info"
        '
        '_FailureCountTextBox
        '
        Me._FailureCountTextBox.Name = "_FailureCountTextBox"
        Me._FailureCountTextBox.Size = New System.Drawing.Size(100, 23)
        Me._FailureCountTextBox.ToolTipText = "Failure count"
        '
        '_PreviousStateTextBox
        '
        Me._PreviousStateTextBox.Name = "_PreviousStateTextBox"
        Me._PreviousStateTextBox.Size = New System.Drawing.Size(100, 23)
        '
        '_SequenceStatusAnnunciatorButton
        '
        Me._SequenceStatusAnnunciatorButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._SequenceStatusAnnunciatorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SequenceStatusAnnunciatorButton.Image = Global.isr.Automata.Finite.Dashboard.My.Resources.Resources.dialog_clean
        Me._SequenceStatusAnnunciatorButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SequenceStatusAnnunciatorButton.Name = "_SequenceStatusAnnunciatorButton"
        Me._SequenceStatusAnnunciatorButton.Size = New System.Drawing.Size(23, 17)
        Me._SequenceStatusAnnunciatorButton.Text = "Status Annunciator"
        Me._SequenceStatusAnnunciatorButton.ToolTipText = "Status Annunciator"
        '
        '_NameStatusStrip
        '
        Me._NameStatusStrip.AutoSize = False
        Me._NameStatusStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me._NameStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._EngineNameLabel})
        Me._NameStatusStrip.Location = New System.Drawing.Point(0, 0)
        Me._NameStatusStrip.Name = "_NameStatusStrip"
        Me._NameStatusStrip.ShowItemToolTips = True
        Me._NameStatusStrip.Size = New System.Drawing.Size(163, 16)
        Me._NameStatusStrip.SizingGrip = False
        Me._NameStatusStrip.TabIndex = 3
        Me._NameStatusStrip.Text = "Name Status Strip"
        '
        '_EngineNameLabel
        '
        Me._EngineNameLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._EngineNameLabel.Name = "_EngineNameLabel"
        Me._EngineNameLabel.Size = New System.Drawing.Size(53, 16)
        Me._EngineNameLabel.Text = "<name>"
        Me._EngineNameLabel.ToolTipText = "Automaton name"
        '
        'EngineMonitor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._EngineToolStrip)
        Me.Controls.Add(Me._SignalStatusStrip)
        Me.Controls.Add(Me._StateStatusStrip)
        Me.Controls.Add(Me._NameStatusStrip)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "EngineMonitor"
        Me.Size = New System.Drawing.Size(163, 68)
        Me._StateStatusStrip.ResumeLayout(False)
        Me._StateStatusStrip.PerformLayout()
        Me._SignalStatusStrip.ResumeLayout(False)
        Me._SignalStatusStrip.PerformLayout()
        Me._EngineToolStrip.ResumeLayout(False)
        Me._EngineToolStrip.PerformLayout()
        Me._NameStatusStrip.ResumeLayout(False)
        Me._NameStatusStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _StateStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _SignalStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _EngineToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _EngineInfoButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _LastInputSymbolLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _FailureCountTextBox As Windows.Forms.ToolStripTextBox
    Private WithEvents _PreviousStateTextBox As Windows.Forms.ToolStripTextBox
    Private WithEvents _NameStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _EngineNameLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _PercentProgressLabel As Core.Controls.ToolStripProgressLabel
    Private WithEvents _StateProgressLabel As Core.Controls.ToolStripProgressLabel
    Private WithEvents _SequenceStatusAnnunciatorButton As Windows.Forms.ToolStripButton
End Class
