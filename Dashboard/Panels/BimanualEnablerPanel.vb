Imports System.ComponentModel
Imports System.Drawing
Imports isr.Automata.Finite.Dashboard.ExceptionExtensions
Imports isr.Automata.Finite.machines
''' <summary>
''' Displays the Bi-Manual Enabler state machine
''' </summary>
''' <remarks> (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 1998-05-02, 1.15.0000. </para></remarks>
Public Class BimanualEnablerPanel
    Inherits isr.Core.Forma.FormBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructor for this class.
    ''' </summary>
    Public Sub New()
        MyBase.New()

        Me.InitializingComponents = True
        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not Me.components Is Nothing Then
                Me.components.Dispose()
                If Me._BimanualEnabler IsNot Nothing Then Me._BimanualEnabler.Dispose() : Me._BimanualEnabler = Nothing
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

#Region " FORM EVENTS HANDLERS "

    Private Sub Form_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Dim keyAscii As Integer = Asc(eventArgs.KeyChar)

        Select Case keyAscii

            Case System.Windows.Forms.Keys.A, System.Windows.Forms.Keys.A + 32

                System.Windows.Forms.SendKeys.Send("%A")

            Case System.Windows.Forms.Keys.E, System.Windows.Forms.Keys.E + 32

                System.Windows.Forms.SendKeys.Send("%E")

            Case System.Windows.Forms.Keys.L, System.Windows.Forms.Keys.L + 32

                System.Windows.Forms.SendKeys.Send("%L")

            Case System.Windows.Forms.Keys.R, System.Windows.Forms.Keys.R + 32

                System.Windows.Forms.SendKeys.Send("%R")

            Case System.Windows.Forms.Keys.S, System.Windows.Forms.Keys.S + 32

                System.Windows.Forms.SendKeys.Send("%S")

            Case System.Windows.Forms.Keys.S, System.Windows.Forms.Keys.O + 32

                System.Windows.Forms.SendKeys.Send("%O")

            Case System.Windows.Forms.Keys.Y, System.Windows.Forms.Keys.Y + 32

                System.Windows.Forms.SendKeys.Send("%Y")

        End Select

        eventArgs.KeyChar = Chr(keyAscii)
        If keyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' set the form caption
            Me.Text = Core.ApplicationInfo.BuildApplicationDescriptionCaption("Bi-Manual Enabler")

            ' default to center screen.
            Me.CenterToScreen()

        Catch ex As Exception
            If Windows.Forms.DialogResult.Abort = Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Exception loading", Windows.Forms.MessageBoxButtons.AbortRetryIgnore, Windows.Forms.MessageBoxIcon.Error, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly) Then
                Windows.Forms.Application.Exit()
            End If
        Finally
            MyBase.OnLoad(e)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()
        End Try

    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As System.EventArgs)

        Try

            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' allow form rendering time to complete: process all messages currently in the queue.
            Windows.Forms.Application.DoEvents()

            Me.OnFormShown()

        Catch ex As Exception
            If Windows.Forms.DialogResult.Abort = Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Exception showing", Windows.Forms.MessageBoxButtons.AbortRetryIgnore, Windows.Forms.MessageBoxIcon.Error, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly) Then
                Windows.Forms.Application.Exit()
            End If
        Finally
            MyBase.OnShown(e)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()
            Me.OnStateEnter(Me.BimanualEnabler, Me.BimanualEnabler.OriginState)
            Me.OnPropertyChanged(Me.BimanualEnabler, NameOf(BimanualEnabler.CurrentStateIdentity))
        End Try

    End Sub

    Private Sub OnFormShown()
        Try
            ' Create a new state machine
            Me.BimanualEnabler = New isr.Automata.Finite.Machines.BimanualEnabler With {.ResponseTimeout = TimeSpan.FromSeconds(0.25)}
            Me._ResponseTimeLabel.Text = Me.BimanualEnabler.ResponseTimeout.TotalSeconds.ToString("#.00#\s", Globalization.CultureInfo.CurrentCulture)
            Windows.Forms.Application.DoEvents()

            ' Set the caption to the Application title
            Me.Text = Core.ApplicationInfo.BuildApplicationDescriptionCaption(Me.BimanualEnabler.Name)

            ' set initial values
            'Me.LeftPushButton_CheckStateChanged(_LeftPushButton, New System.EventArgs())
            'Me.RightPushButton_CheckStateChanged(_RightPushButton, New System.EventArgs())
            'Me.StartToggleButton_CheckStateChanged(_StartToggleButton, New System.EventArgs())
            'Me.ReadyToggleButton_CheckStateChanged(_ReadyToggleButton, New System.EventArgs())

            ' allow form rendering time to complete: process all messages currently in the queue.
            Windows.Forms.Application.DoEvents()
        Catch
            If Me.BimanualEnabler IsNot Nothing Then
                Me.BimanualEnabler.Dispose()
            End If
            Me._BimanualEnabler = Nothing
            Throw
        Finally
        End Try
    End Sub


#End Region

#Region " MACHINE BEHAVIOR "

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary>
    ''' Instance of the State Machine class.
    ''' </summary>
    Private WithEvents _BimanualEnabler As BimanualEnabler
#Enable Warning IDE1006 ' Naming Styles
    Private Property BimanualEnabler As BimanualEnabler
        Get
            Return Me._BimanualEnabler
        End Get
        Set(value As BimanualEnabler)
            If Me._BimanualEnabler IsNot Nothing Then
                With Me._BimanualEnabler
                    RemoveHandler .EnabledState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    RemoveHandler .EstopState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    RemoveHandler .FailureState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    RemoveHandler .GoState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    RemoveHandler .OriginState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    RemoveHandler .ReadyState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    RemoveHandler .SetState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    RemoveHandler .PropertyChanged, AddressOf Me.BimanualEnabler_PropertyChanged
                End With
            End If
            Me._BimanualEnabler = value
            If value Is Nothing Then
            Else
                With Me._BimanualEnabler
                    AddHandler .EnabledState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    AddHandler .EstopState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    AddHandler .FailureState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    AddHandler .GoState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    AddHandler .OriginState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    AddHandler .ReadyState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    AddHandler .SetState.Enter, AddressOf Me.BimanualEnablerState_Enter
                    AddHandler .PropertyChanged, AddressOf Me.BimanualEnabler_PropertyChanged
                End With
            End If
        End Set
    End Property

    Private Sub OnPropertyChanged(ByVal sender As BimanualEnabler, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(BimanualEnabler.ProgressIndicatorPercent)
                ' Me._PlatformStateLabel.Text = sender.CurrentStateIdentity.Description
                Me._CurrentStateLabel.Text = sender.CurrentStateName
            Case NameOf(BimanualEnabler.CurrentStateName)
                Me._CurrentStateLabel.Text = sender.CurrentStateName
            Case NameOf(BimanualEnabler.CurrentStateIdentity)
                Me._CurrentStateLabel.Text = sender.CurrentStateName
        End Select
    End Sub

    ''' <summary> Platform property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BimanualEnabler_PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.BimanualEnabler_PropertyChanged), New Object() {sender, e})
        Else
            Dim automaton As BimanualEnabler = TryCast(sender, BimanualEnabler)
            If automaton Is Nothing OrElse e Is Nothing Then Return
            Dim activity As String = $"handling {automaton.Name}.{e.PropertyName} change"
            Try
                Me.OnPropertyChanged(automaton, e.PropertyName)
            Catch ex As Exception
                Me._ErrorLable.Text = $"{Me.Name} exception {activity} at {automaton.Name}.{automaton.CurrentStateIdentity} state;. {ex.ToFullBlownString}"
            End Try
        End If
    End Sub

    ''' <summary> Executes the state enter action. </summary>
    ''' <param name="sender"> Source of the event. </param>
    Private Sub OnStateEnter(ByVal automaton As BimanualEnabler, ByVal sender As IState(Of BimanualEnablerState))
        If automaton Is Nothing OrElse sender Is Nothing Then Return
        Me.StateColor(automaton.PreviousStateIdentity) = SystemColors.InactiveBorder
        Me.StateColor(automaton.CurrentStateIdentity) = SystemColors.ActiveBorder
        Select Case sender.Identity
            Case BimanualEnablerState.Enabled
            Case BimanualEnablerState.Estop
            Case BimanualEnablerState.Failure
                Me._InfoLabel.Text = $"{sender.Name} failure #{automaton.ConsecutiveFailureCount} occurred"
            Case BimanualEnablerState.Go
            Case BimanualEnablerState.Ready
                Me._LeftPushButton.CheckState = System.Windows.Forms.CheckState.Unchecked
                Me._RightPushButton.CheckState = System.Windows.Forms.CheckState.Unchecked
            Case BimanualEnablerState.Set
            Case BimanualEnablerState.Origin
                Me._LeftPushButton.CheckState = System.Windows.Forms.CheckState.Unchecked
                Me._RightPushButton.CheckState = System.Windows.Forms.CheckState.Unchecked
        End Select
    End Sub

    ''' <summary> Handles Platform automaton state Enter events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BimanualEnablerState_Enter(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim state As IState(Of BimanualEnablerState) = TryCast(sender, IState(Of BimanualEnablerState))
        Dim automaton As BimanualEnabler = Me.BimanualEnabler
        If automaton Is Nothing OrElse state Is Nothing Then Return
        Dim activity As String = $"handling {automaton.Name}{automaton.CurrentStateIdentity}.Enter event"
        Try
            Me.OnStateEnter(automaton, state)
        Catch ex As Exception
            Me._ErrorLable.Text = $"{Me.Name} exception handling {activity};. {ex.ToFullBlownString}"
        End Try
    End Sub

#End Region

#Region " MACHINE APPERARANCE "

    ''' <summary>
    ''' Sets or gets the state color.
    ''' </summary>
    ''' <param name="stateId"></param>
    Private Property StateColor(ByVal stateId As BimanualEnablerState) As Color
        Get
            Return Me.StateShape(stateId).BackColor
        End Get
        Set(ByVal value As Color)
            Me.StateShape(stateId).BackColor = value
            Me.StateLabel(stateId).BackColor = value
        End Set
    End Property

    ''' <summary>
    ''' Selects the state shape.
    ''' </summary>
    ''' <param name="stateId"></param>
    Private ReadOnly Property StateLabel(ByVal stateId As BimanualEnablerState) As Windows.Forms.Label
        Get
            Select Case stateId
                Case BimanualEnablerState.Enabled
                    Return Me._EnabledStateLabelLabel
                Case BimanualEnablerState.Estop
                    Return Me._EStopStateLabelLabel
                Case BimanualEnablerState.Ready
                    Return Me._ReadyStateLabelLabel
                Case BimanualEnablerState.Set
                    Return Me._SetStateLabelLabel
                Case BimanualEnablerState.Go
                    Return Me._GoStateLabeLabel
                Case BimanualEnablerState.Origin
                    Return Me._OriginStateLabelLabel
                Case BimanualEnablerState.None
                    Return Me._OriginStateLabelLabel
                Case Else
                    Debug.Assert(False)
                    Return Me._OriginStateLabelLabel
            End Select

        End Get
    End Property

    ''' <summary>
    ''' Selects the state shape.
    ''' </summary>
    ''' <param name="stateId"></param>
    Private ReadOnly Property StateShape(ByVal stateId As BimanualEnablerState) As Windows.Forms.Label
        Get

            Select Case stateId
                Case BimanualEnablerState.Enabled
                    Return Me._EnabledStateLabel
                Case BimanualEnablerState.Estop
                    Return Me._EStopStateLabel
                Case BimanualEnablerState.Ready
                    Return Me._ReadyStateLabel
                Case BimanualEnablerState.Set
                    Return Me._SetStateLabel
                Case BimanualEnablerState.Go
                    Return Me._GoStateLabel
                Case BimanualEnablerState.Origin
                    Return Me._OriginStateShape
                Case BimanualEnablerState.None
                    Return Me._OriginStateShape
                Case Else
                    Debug.Assert(False)
                    Return Me._OriginStateShape
            End Select

        End Get
    End Property

#End Region

#Region " CONTROL EVENTS HANDLERS "

    ''' <summary>
    ''' Sets the acknowledge input.
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    Private Sub AcknowledgeButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _AcknowledgeButton.Click

        If Me._AcknowledgeButton.Enabled AndAlso Me.BimanualEnabler IsNot Nothing Then
            ' Acknowledge receipt of the go signal.
            Me.BimanualEnabler.Acknowledge()
        End If

    End Sub

    ''' <summary>
    ''' Sends an EStop signal.
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    Private Sub EStopButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _EStopButton.Click
        If Me._EStopButton.Enabled AndAlso Me.BimanualEnabler IsNot Nothing Then
            ' Send the state machine to its idle state
            Me.BimanualEnabler.EstopPressed()
        End If
    End Sub

    ''' <summary>
    ''' Inputs the left button state to the state machine.
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    Private Sub LeftPushButton_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _LeftPushButton.CheckStateChanged
        If Me.InitializingComponents Then Return
        If Me._LeftPushButton.Checked Then
            ' calls on a thread are required otherwise the buttons are not released in timeout because the ready enter occurs within the check state change event.
            Dim thread As New Threading.Thread(New Threading.ThreadStart(AddressOf Me.BimanualEnabler.ButtonPressed))
            thread.Start()
            Me._LeftPushButton.BackColor = SystemColors.GradientActiveCaption
        Else
            Dim thread As New Threading.Thread(New Threading.ThreadStart(AddressOf Me.BimanualEnabler.ButtonReleased))
            thread.Start()
            Me._LeftPushButton.BackColor = SystemColors.GradientInactiveCaption
        End If
    End Sub

    ''' <summary>
    ''' Sends the ready signal to the state machine.
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    Private Sub ReadyButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _ReadyButton.Click
        If Me.InitializingComponents Then Return
        Me.BimanualEnabler.Ready()
    End Sub

    ''' <summary>
    ''' Inputs the right button state to the state machine.
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    Private Sub RightPushButton_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _RightPushButton.CheckStateChanged
        If Me.InitializingComponents Then Return
        If Me._RightPushButton.Checked Then
            ' calls on a thread are required otherwise the buttons are not released in timeout because the ready enter occurs within the check state change event.
            Dim thread As New Threading.Thread(New Threading.ThreadStart(AddressOf Me.BimanualEnabler.ButtonPressed))
            thread.Start()
            Me._RightPushButton.BackColor = SystemColors.GradientActiveCaption
        Else
            Dim thread As New Threading.Thread(New Threading.ThreadStart(AddressOf Me.BimanualEnabler.ButtonReleased))
            thread.Start()
            Me._RightPushButton.BackColor = SystemColors.GradientInactiveCaption
        End If
    End Sub

    ''' <summary>
    ''' Sends the start or stop transition signal.
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    Private Sub OriginButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _OriginButton.Click
        If Me.InitializingComponents Then Return
        Me.BimanualEnabler.ProcessInput(BimanualEnablerSymbol.Origin)
    End Sub

#End Region

End Class
