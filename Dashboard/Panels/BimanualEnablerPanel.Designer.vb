<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class BimanualEnablerPanel
#Region "Windows Form Designer generated code "
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _AcknowledgeButton As System.Windows.Forms.Button
    Private WithEvents _RightPushButton As System.Windows.Forms.CheckBox
    Private WithEvents _LeftPushButton As System.Windows.Forms.CheckBox
    Private WithEvents _EStopButton As System.Windows.Forms.Button
    Private WithEvents _ResponseTimeLabel As System.Windows.Forms.Label
    Private WithEvents _ResponseTimeLabelLabel As System.Windows.Forms.Label
    Private WithEvents _EnabledStateLabel As System.Windows.Forms.Label
    Private WithEvents _OriginStateLabelLabel As System.Windows.Forms.Label
    Private WithEvents _GoStateLabel As System.Windows.Forms.Label
    Private WithEvents _SetStateLabel As System.Windows.Forms.Label
    Private WithEvents _ReadyStateLabel As System.Windows.Forms.Label
    Private WithEvents _EStopStateLabel As System.Windows.Forms.Label
    Private WithEvents _UserInterfacePanel As System.Windows.Forms.Panel
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BimanualEnablerPanel))
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._AcknowledgeButton = New System.Windows.Forms.Button()
        Me._RightPushButton = New System.Windows.Forms.CheckBox()
        Me._LeftPushButton = New System.Windows.Forms.CheckBox()
        Me._EStopButton = New System.Windows.Forms.Button()
        Me._CurrentStateLabel = New System.Windows.Forms.Label()
        Me._OriginButton = New System.Windows.Forms.Button()
        Me._ReadyButton = New System.Windows.Forms.Button()
        Me._UserInterfacePanel = New System.Windows.Forms.Panel()
        Me._EnabledStateLabel = New System.Windows.Forms.Label()
        Me._GoStateLabel = New System.Windows.Forms.Label()
        Me._SetStateLabel = New System.Windows.Forms.Label()
        Me._ReadyStateLabel = New System.Windows.Forms.Label()
        Me._EStopStateLabel = New System.Windows.Forms.Label()
        Me._OriginStateShape = New System.Windows.Forms.Label()
        Me._ResponseTimeLabel = New System.Windows.Forms.Label()
        Me._ResponseTimeLabelLabel = New System.Windows.Forms.Label()
        Me._InfoLabel = New System.Windows.Forms.Label()
        Me._EnabledStateLabelLabel = New System.Windows.Forms.Label()
        Me._OriginStateLabelLabel = New System.Windows.Forms.Label()
        Me._GoStateLabeLabel = New System.Windows.Forms.Label()
        Me._SetStateLabelLabel = New System.Windows.Forms.Label()
        Me._ReadyStateLabelLabel = New System.Windows.Forms.Label()
        Me._EStopStateLabelLabel = New System.Windows.Forms.Label()
        Me._ErrorLable = New System.Windows.Forms.Label()
        Me._UserInterfacePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_AcknowledgeButton
        '
        Me._AcknowledgeButton.BackColor = System.Drawing.SystemColors.Control
        Me._AcknowledgeButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._AcknowledgeButton.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._AcknowledgeButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AcknowledgeButton.Location = New System.Drawing.Point(4, 209)
        Me._AcknowledgeButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._AcknowledgeButton.Name = "_AcknowledgeButton"
        Me._AcknowledgeButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AcknowledgeButton.Size = New System.Drawing.Size(117, 30)
        Me._AcknowledgeButton.TabIndex = 16
        Me._AcknowledgeButton.Text = "&ACKNOWLEDGE"
        Me._ToolTip.SetToolTip(Me._AcknowledgeButton, "Must acknowledge (handshake) to move on.")
        Me._AcknowledgeButton.UseVisualStyleBackColor = False
        '
        '_RightPushButton
        '
        Me._RightPushButton.Appearance = System.Windows.Forms.Appearance.Button
        Me._RightPushButton.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me._RightPushButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._RightPushButton.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._RightPushButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._RightPushButton.Location = New System.Drawing.Point(267, 272)
        Me._RightPushButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._RightPushButton.Name = "_RightPushButton"
        Me._RightPushButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RightPushButton.Size = New System.Drawing.Size(93, 40)
        Me._RightPushButton.TabIndex = 13
        Me._RightPushButton.Text = "&RIGHT"
        Me._RightPushButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._ToolTip.SetToolTip(Me._RightPushButton, "Check left and right buttons quickly to enable.")
        Me._RightPushButton.UseVisualStyleBackColor = False
        '
        '_LeftPushButton
        '
        Me._LeftPushButton.Appearance = System.Windows.Forms.Appearance.Button
        Me._LeftPushButton.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me._LeftPushButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._LeftPushButton.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._LeftPushButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._LeftPushButton.Location = New System.Drawing.Point(163, 272)
        Me._LeftPushButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._LeftPushButton.Name = "_LeftPushButton"
        Me._LeftPushButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._LeftPushButton.Size = New System.Drawing.Size(93, 40)
        Me._LeftPushButton.TabIndex = 12
        Me._LeftPushButton.Text = "&LEFT"
        Me._LeftPushButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._ToolTip.SetToolTip(Me._LeftPushButton, "Check left and right buttons quickly to enable.")
        Me._LeftPushButton.UseVisualStyleBackColor = False
        '
        '_EStopButton
        '
        Me._EStopButton.BackColor = System.Drawing.Color.Red
        Me._EStopButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._EStopButton.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EStopButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._EStopButton.Location = New System.Drawing.Point(16, 253)
        Me._EStopButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._EStopButton.Name = "_EStopButton"
        Me._EStopButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._EStopButton.Size = New System.Drawing.Size(93, 45)
        Me._EStopButton.TabIndex = 9
        Me._EStopButton.Text = "&E-STOP"
        Me._ToolTip.SetToolTip(Me._EStopButton, "Click to go to the EStop state")
        Me._EStopButton.UseVisualStyleBackColor = False
        '
        '_CurrentStateLabel
        '
        Me._CurrentStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._CurrentStateLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._CurrentStateLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CurrentStateLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._CurrentStateLabel.Location = New System.Drawing.Point(3, 4)
        Me._CurrentStateLabel.Name = "_CurrentStateLabel"
        Me._CurrentStateLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CurrentStateLabel.Size = New System.Drawing.Size(111, 21)
        Me._CurrentStateLabel.TabIndex = 14
        Me._CurrentStateLabel.Text = "<state>"
        Me._ToolTip.SetToolTip(Me._CurrentStateLabel, "Current State")
        '
        '_OriginButton
        '
        Me._OriginButton.Location = New System.Drawing.Point(7, 107)
        Me._OriginButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._OriginButton.Name = "_OriginButton"
        Me._OriginButton.Size = New System.Drawing.Size(75, 28)
        Me._OriginButton.TabIndex = 15
        Me._OriginButton.Text = "&ORIGIN"
        Me._ToolTip.SetToolTip(Me._OriginButton, "Forces a move to ORIGIN")
        Me._OriginButton.UseVisualStyleBackColor = True
        '
        '_ReadyButton
        '
        Me._ReadyButton.Location = New System.Drawing.Point(8, 151)
        Me._ReadyButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ReadyButton.Name = "_ReadyButton"
        Me._ReadyButton.Size = New System.Drawing.Size(75, 28)
        Me._ReadyButton.TabIndex = 16
        Me._ReadyButton.Text = "READ&Y"
        Me._ToolTip.SetToolTip(Me._ReadyButton, "Signals move to Ready state.")
        Me._ReadyButton.UseVisualStyleBackColor = True
        '
        '_UserInterfacePanel
        '
        Me._UserInterfacePanel.Controls.Add(Me._ReadyButton)
        Me._UserInterfacePanel.Controls.Add(Me._OriginButton)
        Me._UserInterfacePanel.Controls.Add(Me._EnabledStateLabel)
        Me._UserInterfacePanel.Controls.Add(Me._GoStateLabel)
        Me._UserInterfacePanel.Controls.Add(Me._SetStateLabel)
        Me._UserInterfacePanel.Controls.Add(Me._ReadyStateLabel)
        Me._UserInterfacePanel.Controls.Add(Me._EStopStateLabel)
        Me._UserInterfacePanel.Controls.Add(Me._OriginStateShape)
        Me._UserInterfacePanel.Controls.Add(Me._CurrentStateLabel)
        Me._UserInterfacePanel.Location = New System.Drawing.Point(0, 0)
        Me._UserInterfacePanel.Margin = New System.Windows.Forms.Padding(0)
        Me._UserInterfacePanel.Name = "_UserInterfacePanel"
        Me._UserInterfacePanel.Size = New System.Drawing.Size(425, 329)
        Me._UserInterfacePanel.TabIndex = 20
        '
        '_EnabledStateLabel
        '
        Me._EnabledStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._EnabledStateLabel.Location = New System.Drawing.Point(115, 132)
        Me._EnabledStateLabel.Name = "_EnabledStateLabel"
        Me._EnabledStateLabel.Size = New System.Drawing.Size(89, 64)
        Me._EnabledStateLabel.TabIndex = 1
        '
        '_GoStateLabel
        '
        Me._GoStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._GoStateLabel.Location = New System.Drawing.Point(219, 194)
        Me._GoStateLabel.Name = "_GoStateLabel"
        Me._GoStateLabel.Size = New System.Drawing.Size(89, 64)
        Me._GoStateLabel.TabIndex = 2
        '
        '_SetStateLabel
        '
        Me._SetStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._SetStateLabel.Location = New System.Drawing.Point(328, 132)
        Me._SetStateLabel.Name = "_SetStateLabel"
        Me._SetStateLabel.Size = New System.Drawing.Size(89, 64)
        Me._SetStateLabel.TabIndex = 3
        '
        '_ReadyStateLabel
        '
        Me._ReadyStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._ReadyStateLabel.Location = New System.Drawing.Point(219, 68)
        Me._ReadyStateLabel.Name = "_ReadyStateLabel"
        Me._ReadyStateLabel.Size = New System.Drawing.Size(89, 64)
        Me._ReadyStateLabel.TabIndex = 4
        '
        '_EStopStateLabel
        '
        Me._EStopStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._EStopStateLabel.Location = New System.Drawing.Point(328, 11)
        Me._EStopStateLabel.Name = "_EStopStateLabel"
        Me._EStopStateLabel.Size = New System.Drawing.Size(89, 64)
        Me._EStopStateLabel.TabIndex = 5
        '
        '_OriginStateShape
        '
        Me._OriginStateShape.BackColor = System.Drawing.SystemColors.Control
        Me._OriginStateShape.Location = New System.Drawing.Point(136, 24)
        Me._OriginStateShape.Name = "_OriginStateShape"
        Me._OriginStateShape.Size = New System.Drawing.Size(60, 40)
        Me._OriginStateShape.TabIndex = 7
        '
        '_ResponseTimeLabel
        '
        Me._ResponseTimeLabel.BackColor = System.Drawing.SystemColors.Control
        Me._ResponseTimeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._ResponseTimeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ResponseTimeLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ResponseTimeLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ResponseTimeLabel.Location = New System.Drawing.Point(8, 75)
        Me._ResponseTimeLabel.Name = "_ResponseTimeLabel"
        Me._ResponseTimeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ResponseTimeLabel.Size = New System.Drawing.Size(44, 21)
        Me._ResponseTimeLabel.TabIndex = 15
        Me._ResponseTimeLabel.Text = "0.2500"
        '
        '_ResponseTimeLabelLabel
        '
        Me._ResponseTimeLabelLabel.BackColor = System.Drawing.Color.Transparent
        Me._ResponseTimeLabelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ResponseTimeLabelLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ResponseTimeLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ResponseTimeLabelLabel.Location = New System.Drawing.Point(8, 55)
        Me._ResponseTimeLabelLabel.Name = "_ResponseTimeLabelLabel"
        Me._ResponseTimeLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ResponseTimeLabelLabel.Size = New System.Drawing.Size(111, 21)
        Me._ResponseTimeLabelLabel.TabIndex = 14
        Me._ResponseTimeLabelLabel.Text = "Bi-Manual Timeout"
        '
        '_InfoLabel
        '
        Me._InfoLabel.BackColor = System.Drawing.SystemColors.Control
        Me._InfoLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._InfoLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._InfoLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._InfoLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._InfoLabel.Location = New System.Drawing.Point(0, 400)
        Me._InfoLabel.Name = "_InfoLabel"
        Me._InfoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InfoLabel.Size = New System.Drawing.Size(425, 64)
        Me._InfoLabel.TabIndex = 11
        Me._InfoLabel.Text = "An OSHA-compliant Bi-Manual Enabler control with E-Stop.  This control is enabled" &
    " upon receiving an acknowledge signal after both push buttons are pressed within" &
    " a preset time interval."
        Me._InfoLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_EnabledStateLabelLabel
        '
        Me._EnabledStateLabelLabel.AutoSize = True
        Me._EnabledStateLabelLabel.BackColor = System.Drawing.Color.Transparent
        Me._EnabledStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._EnabledStateLabelLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EnabledStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._EnabledStateLabelLabel.Location = New System.Drawing.Point(132, 155)
        Me._EnabledStateLabelLabel.Name = "_EnabledStateLabelLabel"
        Me._EnabledStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._EnabledStateLabelLabel.Size = New System.Drawing.Size(54, 14)
        Me._EnabledStateLabelLabel.TabIndex = 10
        Me._EnabledStateLabelLabel.Text = "ENABLED"
        Me._EnabledStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_OriginStateLabelLabel
        '
        Me._OriginStateLabelLabel.AutoSize = True
        Me._OriginStateLabelLabel.BackColor = System.Drawing.Color.Transparent
        Me._OriginStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._OriginStateLabelLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._OriginStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._OriginStateLabelLabel.Location = New System.Drawing.Point(146, 35)
        Me._OriginStateLabelLabel.Name = "_OriginStateLabelLabel"
        Me._OriginStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._OriginStateLabelLabel.Size = New System.Drawing.Size(41, 14)
        Me._OriginStateLabelLabel.TabIndex = 7
        Me._OriginStateLabelLabel.Text = "ORIGIN"
        Me._OriginStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_GoStateLabeLabel
        '
        Me._GoStateLabeLabel.AutoSize = True
        Me._GoStateLabeLabel.BackColor = System.Drawing.Color.Transparent
        Me._GoStateLabeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._GoStateLabeLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GoStateLabeLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._GoStateLabeLabel.Location = New System.Drawing.Point(252, 220)
        Me._GoStateLabeLabel.Name = "_GoStateLabeLabel"
        Me._GoStateLabeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._GoStateLabeLabel.Size = New System.Drawing.Size(23, 14)
        Me._GoStateLabeLabel.TabIndex = 4
        Me._GoStateLabeLabel.Text = "GO"
        Me._GoStateLabeLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_SetStateLabelLabel
        '
        Me._SetStateLabelLabel.AutoSize = True
        Me._SetStateLabelLabel.BackColor = System.Drawing.Color.Transparent
        Me._SetStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._SetStateLabelLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SetStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SetStateLabelLabel.Location = New System.Drawing.Point(359, 155)
        Me._SetStateLabelLabel.Name = "_SetStateLabelLabel"
        Me._SetStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SetStateLabelLabel.Size = New System.Drawing.Size(26, 14)
        Me._SetStateLabelLabel.TabIndex = 3
        Me._SetStateLabelLabel.Text = "SET"
        Me._SetStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_ReadyStateLabelLabel
        '
        Me._ReadyStateLabelLabel.AutoSize = True
        Me._ReadyStateLabelLabel.BackColor = System.Drawing.Color.Transparent
        Me._ReadyStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ReadyStateLabelLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReadyStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ReadyStateLabelLabel.Location = New System.Drawing.Point(242, 92)
        Me._ReadyStateLabelLabel.Name = "_ReadyStateLabelLabel"
        Me._ReadyStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ReadyStateLabelLabel.Size = New System.Drawing.Size(43, 14)
        Me._ReadyStateLabelLabel.TabIndex = 2
        Me._ReadyStateLabelLabel.Text = "READY"
        Me._ReadyStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_EStopStateLabelLabel
        '
        Me._EStopStateLabelLabel.AutoSize = True
        Me._EStopStateLabelLabel.BackColor = System.Drawing.Color.Transparent
        Me._EStopStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._EStopStateLabelLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EStopStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._EStopStateLabelLabel.Location = New System.Drawing.Point(352, 34)
        Me._EStopStateLabelLabel.Name = "_EStopStateLabelLabel"
        Me._EStopStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._EStopStateLabelLabel.Size = New System.Drawing.Size(40, 14)
        Me._EStopStateLabelLabel.TabIndex = 1
        Me._EStopStateLabelLabel.Text = "ESTOP"
        Me._EStopStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_ErrorLable
        '
        Me._ErrorLable.BackColor = System.Drawing.SystemColors.Control
        Me._ErrorLable.Cursor = System.Windows.Forms.Cursors.Default
        Me._ErrorLable.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ErrorLable.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ErrorLable.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ErrorLable.Location = New System.Drawing.Point(0, 336)
        Me._ErrorLable.Name = "_ErrorLable"
        Me._ErrorLable.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ErrorLable.Size = New System.Drawing.Size(425, 64)
        Me._ErrorLable.TabIndex = 21
        Me._ErrorLable.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'BimanualEnablerPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(425, 464)
        Me.Controls.Add(Me._ErrorLable)
        Me.Controls.Add(Me._AcknowledgeButton)
        Me.Controls.Add(Me._RightPushButton)
        Me.Controls.Add(Me._LeftPushButton)
        Me.Controls.Add(Me._EStopButton)
        Me.Controls.Add(Me._ResponseTimeLabel)
        Me.Controls.Add(Me._ResponseTimeLabelLabel)
        Me.Controls.Add(Me._InfoLabel)
        Me.Controls.Add(Me._EnabledStateLabelLabel)
        Me.Controls.Add(Me._OriginStateLabelLabel)
        Me.Controls.Add(Me._GoStateLabeLabel)
        Me.Controls.Add(Me._SetStateLabelLabel)
        Me.Controls.Add(Me._ReadyStateLabelLabel)
        Me.Controls.Add(Me._EStopStateLabelLabel)
        Me.Controls.Add(Me._UserInterfacePanel)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(449, 188)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "BimanualEnablerPanel"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._UserInterfacePanel.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _ErrorLable As Windows.Forms.Label
    Private WithEvents _InfoLabel As Windows.Forms.Label
    Private WithEvents _EnabledStateLabelLabel As Windows.Forms.Label
    Private WithEvents _GoStateLabeLabel As Windows.Forms.Label
    Private WithEvents _SetStateLabelLabel As Windows.Forms.Label
    Private WithEvents _ReadyStateLabelLabel As Windows.Forms.Label
    Private WithEvents _EStopStateLabelLabel As Windows.Forms.Label
    Private WithEvents _OriginStateShape As Windows.Forms.Label
    Private WithEvents _CurrentStateLabel As Windows.Forms.Label
    Private WithEvents _ReadyButton As Windows.Forms.Button
    Private WithEvents _OriginButton As Windows.Forms.Button
#End Region
End Class
