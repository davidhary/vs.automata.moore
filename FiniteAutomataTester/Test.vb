Imports System.IO
Imports System.Threading
Imports isr.Automata.ExceptionExtensions
Imports isr.Automata.Finite
Imports isr.Automata.Finite.Machines
Public Class Test

    Public Shared Sub Run()
        Dim test As New Test
        'test.RunClassBasedSigma()
        'test.RunEnumBasedSigma()
        'test.RunStringBasedSigma()
        test.RunQueryMachine()
        Console.WriteLine("Exiting.  Enter key to exit:")
        Console.ReadKey()
    End Sub

#Region " THREE STATE MACHINES "

    Private WithEvents State1 As IState(Of SigmaState)
    Private WithEvents State2 As IState(Of SigmaState)
    Private WithEvents State3 As IState(Of SigmaState)

    Private WithEvents StringTransition1 As ITransition(Of String, SigmaState)
    Private WithEvents StringTransition2 As ITransition(Of String, SigmaState)
    Private WithEvents StringTransition3 As ITransition(Of String, SigmaState)
    Private WithEvents StringMachine As IMachine(Of String, SigmaState)

    Private WithEvents SignalTransition1 As ITransition(Of SigmaSignal, SigmaState)
    Private WithEvents SignalTransition2 As ITransition(Of SigmaSignal, SigmaState)
    Private WithEvents SignalTransition3 As ITransition(Of SigmaSignal, SigmaState)
    Private WithEvents SignalMachine As IMachine(Of SigmaSignal, SigmaState)

    Private WithEvents SigmaTransition1 As ITransition(Of Sigma, SigmaState)
    Private WithEvents SigmaTransition2 As ITransition(Of Sigma, SigmaState)
    Private WithEvents SigmaTransition3 As ITransition(Of Sigma, SigmaState)
    Private WithEvents SigmaMachine As IMachine(Of Sigma, SigmaState)

    Private Class StringMachine1
        Inherits Machine(Of String, SigmaState)
        Public Sub New()
            MyBase.New("String Machine")
        End Sub
        Public Overrides Sub EnqueueStopSymbol()
            Throw New NotImplementedException()
        End Sub
        Public Overrides Sub UpdatePercentProgress()
            Throw New NotImplementedException()
        End Sub
        Public Overrides Sub UpdateProgressIndicatorPercent()
            Throw New NotImplementedException()
        End Sub

        Public Overrides ReadOnly Property NullStateIdentity As SigmaState = SigmaState.None

        Public Overrides ReadOnly Property InitialStateIdentity As SigmaState = SigmaState.One

        Public Overrides ReadOnly Property TerminalStateIdentity As SigmaState = SigmaState.Three

        Public Overrides ReadOnly Property FailInputSymbol As String = String.Empty

        Public Overrides ReadOnly Property InitialInputSymbol As String = String.Empty

        Public Overrides ReadOnly Property TerminalInputSymbol As String = String.Empty

        Public Overrides Function InputSymbolNames() As IEnumerable(Of String)
            Return New String() {"1->2", "2->3", "3->1"}
        End Function

    End Class

    ''' <summary>
    ''' Tests a Moore machine with String signals.
    ''' </summary>
    Public Sub RunStringBasedSigma()
        Me.StringMachine = New StringMachine1 With {
            .OnsetDelay = TimeSpan.FromMilliseconds(500),
            .Interval = TimeSpan.FromMilliseconds(1700)
        }
        Me.State1 = Me.StringMachine.Add(SigmaState.One)
        Me.State2 = Me.StringMachine.Add(SigmaState.Two)
        Me.State3 = Me.StringMachine.Add(SigmaState.Three)

        Dim sigma As String() = New String() {"", "", ""}
        sigma(0) = Me.StringMachine.Add("1->2")
        sigma(1) = Me.StringMachine.Add("2->3")
        sigma(2) = Me.StringMachine.Add("3->1")

        Me.StringTransition1 = Me.StringMachine.Add(Me.State1, sigma(0), Me.State2) ' state1 --1->2--> state2
        Me.StringTransition2 = Me.StringMachine.Add(Me.State2, sigma(1), Me.State3) ' state2 --2->3--> state3
        Me.StringTransition3 = Me.StringMachine.Add(Me.State3, sigma(2), Me.State1) ' state3 --3->1--> state1

        Me.StringMachine.Analyze()
        Console.WriteLine(Me.StringMachine.AnalysisReport)

        Dim machineThread As Thread = New Thread(New ThreadStart(AddressOf Me.StringMachine.StartSequence))
        machineThread.Start()

        Dim i As Integer
        Do Until Me.StringMachine.IsStopRequested
            Me.StringMachine.EnqueueInput(sigma(i))
            If (DateTimeOffset.Now.Second Mod 15 = 0) Then
                Me.StringMachine.RequestStop()
            Else
                Thread.Sleep(600)
            End If
            i = (i + 1) Mod 3
        Loop
        ' wait till thread completes all actions.
        If machineThread.IsAlive Then
            Console.WriteLine("thread still alive")
        Else
            Console.WriteLine("thread not alive")
        End If
        machineThread.Join()
        Console.WriteLine("machine thread joined with main thread")
    End Sub

    Private Class SignalMachine1
        Inherits Machine(Of SigmaSignal, SigmaState)

        Public Sub New()
            MyBase.New("Signal Machine")
        End Sub
        Public Overrides Sub EnqueueStopSymbol()
            Throw New NotImplementedException()
        End Sub

        Public Overrides Sub UpdatePercentProgress()
            Throw New NotImplementedException()
        End Sub

        Public Overrides Sub UpdateProgressIndicatorPercent()
            Throw New NotImplementedException()
        End Sub

        Public Overrides ReadOnly Property NullStateIdentity As SigmaState = SigmaState.None

        Public Overrides ReadOnly Property InitialStateIdentity As SigmaState = SigmaState.One

        Public Overrides ReadOnly Property TerminalStateIdentity As SigmaState = SigmaState.Three

        Public Overrides ReadOnly Property FailInputSymbol As SigmaSignal = SigmaSignal.None

        Public Overrides ReadOnly Property InitialInputSymbol As SigmaSignal = SigmaSignal.One

        Public Overrides ReadOnly Property TerminalInputSymbol As SigmaSignal = SigmaSignal.Three

        Public Overrides Function InputSymbolNames() As IEnumerable(Of String)
            Return New String() {SigmaSignal.One.ToString, SigmaSignal.Two.ToString, SigmaSignal.Three.ToString}
        End Function

    End Class
    ''' <summary>
    ''' Tests a state machine using enumerated signals.
    ''' </summary>
    Public Sub RunEnumBasedSigma()
        Me.SignalMachine = New SignalMachine1 With {
            .OnsetDelay = TimeSpan.FromMilliseconds(500),
            .Interval = TimeSpan.FromMilliseconds(1700)
        }
        Me.State1 = Me.SignalMachine.Add(SigmaState.One)
        Me.State2 = Me.SignalMachine.Add(SigmaState.Two)
        Me.State3 = Me.SignalMachine.Add(SigmaState.Three)

        Dim sigma As SigmaSignal() = New SigmaSignal(2) {}
        sigma(0) = Me.SignalMachine.Add(SigmaSignal.One)
        sigma(1) = Me.SignalMachine.Add(SigmaSignal.Two)
        sigma(2) = Me.SignalMachine.Add(SigmaSignal.Three)

        Me.SignalTransition1 = Me.SignalMachine.Add(Me.State1, sigma(0), Me.State2) ' state1 --one--> state2
        Me.SignalTransition2 = Me.SignalMachine.Add(Me.State2, sigma(1), Me.State3) ' state2 --two--> state3
        Me.SignalTransition3 = Me.SignalMachine.Add(Me.State3, sigma(2), Me.State1) ' state3 --three--> state1

        Me.SignalMachine.Analyze()
        Console.WriteLine(Me.SignalMachine.AnalysisReport)

        Dim machineThread As Thread = New Thread(New ThreadStart(AddressOf Me.SignalMachine.StartSequence))
        machineThread.Start()

        Dim i As Integer
        Do Until Me.SignalMachine.IsStopRequested
            Me.SignalMachine.EnqueueInput(sigma(i))
            If (DateTimeOffset.Now.Second Mod 15 = 0) Then
                Me.SignalMachine.RequestStop()
            End If
            Thread.Sleep(600)
            i = (i + 1) Mod 3
        Loop
        ' wait till thread completes all actions.
        machineThread.Join()
    End Sub

    Private Class SigmaMachine1
        Inherits Machine(Of Sigma, SigmaState)
        Public Sub New()
            MyBase.New("Sigma Machine")
        End Sub

        Public Overrides Sub EnqueueStopSymbol()
            Throw New NotImplementedException()
        End Sub

        Public Overrides Sub UpdatePercentProgress()
            Throw New NotImplementedException()
        End Sub

        Public Overrides Sub UpdateProgressIndicatorPercent()
            Throw New NotImplementedException()
        End Sub

        Public Overrides ReadOnly Property NullStateIdentity As SigmaState = SigmaState.None

        Public Overrides ReadOnly Property InitialStateIdentity As SigmaState = SigmaState.One

        Public Overrides ReadOnly Property TerminalStateIdentity As SigmaState = SigmaState.Three

        Public Overrides ReadOnly Property FailInputSymbol As Sigma = New Sigma(SigmaSignal.None)

        Public Overrides ReadOnly Property InitialInputSymbol As Sigma = New Sigma(SigmaSignal.One)

        Public Overrides ReadOnly Property TerminalInputSymbol As Sigma = New Sigma(SigmaSignal.Three)

        Public Overrides Function InputSymbolNames() As IEnumerable(Of String)
            Return New String() {SigmaSignal.One.ToString, SigmaSignal.Two.ToString, SigmaSignal.Three.ToString}
        End Function


    End Class
    ''' <summary>
    ''' Tests a state machine with signals based on a class. 
    ''' </summary>
    Public Sub RunClassBasedSigma()
        Me.SigmaMachine = New SigmaMachine1 With {
            .OnsetDelay = TimeSpan.FromMilliseconds(500),
            .Interval = TimeSpan.FromMilliseconds(1700)
        }
        Me.State1 = Me.SignalMachine.Add(SigmaState.One)
        Me.State2 = Me.SignalMachine.Add(SigmaState.Two)
        Me.State3 = Me.SignalMachine.Add(SigmaState.Three)

        Dim sigma As Sigma() = New Sigma(2) {}
        sigma(0) = Me.SigmaMachine.Add(New Sigma(SigmaSignal.One))
        sigma(1) = Me.SigmaMachine.Add(New Sigma(SigmaSignal.Two))
        sigma(2) = Me.SigmaMachine.Add(New Sigma(SigmaSignal.Three))

        Me.SigmaTransition1 = Me.SigmaMachine.Add(Me.State1, sigma(0), Me.State2) ' state1 --one--> state2
        Me.SigmaTransition2 = Me.SigmaMachine.Add(Me.State2, sigma(1), Me.State3) ' state2 --two--> state3
        Me.SigmaTransition3 = Me.SigmaMachine.Add(Me.State3, sigma(2), Me.State1) ' state3 --three--> state1
        ' delta.AddTransition(state3, sigma(2), state2) ' Not deterministic

        Me.SigmaMachine.Analyze()
        Console.WriteLine(Me.SigmaMachine.AnalysisReport)

        Dim machineThread As Thread = New Thread(New ThreadStart(AddressOf Me.SigmaMachine.StartSequence))
        machineThread.Start()

        Dim i As Integer
        Do Until Me.SigmaMachine.IsStopRequested
            Me.SigmaMachine.EnqueueInput(sigma(i))
            If (DateTimeOffset.Now.Second Mod 31 = 0) Then
                Me.SigmaMachine.RequestStop()
            End If
            Thread.Sleep(600)
            i = (i + 1) Mod 3
        Loop
        ' wait till thread completes all actions.
        machineThread.Join()
    End Sub

    Private Sub ActThis(ByVal sender As Object, ByVal e As System.EventArgs) Handles State1.Reenter, State2.Reenter, State3.Reenter
        Dim state As IState(Of SigmaState) = CType(sender, IState(Of SigmaState))
        Console.WriteLine("{0} Is running...", state.Identity)
    End Sub

    Private Sub EnterThis(ByVal sender As Object, ByVal e As System.EventArgs) Handles State1.Enter, State2.Enter, State3.Enter
        Dim state As IState(Of SigmaState) = CType(sender, IState(Of SigmaState))
        Console.WriteLine("{0} entered.", state.Identity)
    End Sub

    Private Sub LeaveThis(ByVal sender As Object, ByVal e As System.EventArgs) Handles State1.Leave, State2.Leave, State3.Leave
        Dim state As IState(Of SigmaState) = CType(sender, IState(Of SigmaState))
        Console.WriteLine("{0} left.", state.Identity)
    End Sub

    Private Sub SigmaMachine_Acknowledge(ByVal sender As Object, ByVal e As System.EventArgs) Handles SigmaMachine.SymbolAcknowledged
        Console.WriteLine("Got input:  {0}", Me.SigmaMachine.PeekInput())
    End Sub

    Private Sub SignalMachine_Acknowledge(ByVal sender As Object, ByVal e As System.EventArgs) Handles SignalMachine.SymbolAcknowledged
        Console.WriteLine("Got input: {0}", Me.SignalMachine.PeekInput())
    End Sub

    Private Sub StringMachine_Acknowledge(ByVal sender As Object, ByVal e As System.EventArgs) Handles StringMachine.SymbolAcknowledged
        Console.WriteLine("Got input: {0}", Me.StringMachine.PeekInput())
    End Sub

    Private Sub Machine_Started(ByVal sender As Object, ByVal e As System.EventArgs) Handles SigmaMachine.Initiated, SignalMachine.Initiated, StringMachine.Initiated
        Console.WriteLine("Machine Started")
    End Sub

    Private Sub Machine_Stopped(ByVal sender As Object, ByVal e As System.EventArgs) Handles SigmaMachine.Stopped, SignalMachine.Stopped, StringMachine.Stopped
        Console.WriteLine("Machine Stopped")
    End Sub

    Private Sub Transition1_Completed(ByVal sender As Object, ByVal e As System.EventArgs) Handles SigmaTransition1.Completed, SigmaTransition2.Completed,
                                                                        SigmaTransition3.Completed, SignalTransition1.Completed, SignalTransition2.Completed,
                                                                        SignalTransition3.Completed, StringTransition1.Completed, StringTransition2.Completed, StringTransition3.Completed
        Console.WriteLine("transition completed.")

    End Sub

    Private Sub Transition1_Starting(ByVal sender As Object, ByVal e As System.EventArgs) Handles SigmaTransition1.Starting, SigmaTransition2.Starting, SigmaTransition3.Starting,
                                                                        SignalTransition1.Starting, SignalTransition2.Starting, SignalTransition3.Starting,
                                                                        StringTransition1.Starting, StringTransition2.Starting, StringTransition3.Starting
        Console.WriteLine("transition Starting.")

    End Sub

#End Region

#Region " QUERY MACHINE "

    Private WithEvents _QueryMachine As QueryAutomaton

    Private ReadOnly _QueryMachineThread As Thread

    Private Sub JoinThreads(ByVal machineThread As Thread)
        Do
            Thread.Sleep(75)
            Windows.Forms.Application.DoEvents()
        Loop While Not Me._QueryMachine.IsStopRequested
        ' wait till thread completes all actions.
        If machineThread.IsAlive Then
            Console.WriteLine("thread still alive")
        Else
            Console.WriteLine("thread not alive")
            machineThread.Join()
            Console.WriteLine("machine thread joined with main thread")
        End If
        Console.WriteLine("Done.  Enter key to exit:")
        Console.ReadKey()
    End Sub

    Public Sub RunQueryMachine()
        Me._QueryMachine = New QueryAutomaton(TimeSpan.FromSeconds(1))
        Me._QueryMachine.ActivateTask(TimeSpan.Zero, TimeSpan.FromMilliseconds(100))
        'Me._queryMachine.Interval = TimeSpan.FromMilliseconds(100)
        'Me._QueryMachineThread = New Thread(New ThreadStart(AddressOf Me.queryMachine.StartSequence))
        ' Me._QueryMachineThread.Start()
    End Sub

    Public Sub RunQueryMachine1()
        Me._QueryMachine = New QueryAutomaton(TimeSpan.FromSeconds(1)) With {
            .Interval = TimeSpan.FromMilliseconds(100)
        }
        Me._QueryMachine.ActivateTask(TimeSpan.Zero, TimeSpan.FromMilliseconds(100))
        'Dim machineThread As Thread = New Thread(New ThreadStart(AddressOf Me.queryMachine.StartSequence))
        'machineThread.Start()
        'Me.JoinThreads(machineThread)
    End Sub

    Private Sub QueryMachine_Query(ByVal sender As Object, ByVal e As System.ComponentModel.HandledEventArgs) Handles _QueryMachine.Query
        Dim value As Double = New Random(Now.Millisecond).NextDouble
        If value < 1 / 3 Then
            Console.WriteLine("handling query.")
            e.Handled = True
        ElseIf value < 2 / 3 Then
            Console.WriteLine("not handling query.")
            e.Handled = False
        Else
            Me._QueryMachine.EnqueueInput(QueryAutomatonSymbol.Failure)
            ' Throw New Exception("Query failed")
        End If
    End Sub

    Private Sub QueryMachine_Started(ByVal sender As Object, ByVal e As System.EventArgs) Handles _QueryMachine.Initiated
        Console.WriteLine("query machine started.")
    End Sub

#If False Then
    Private Sub OnPropertychanging(ByVal sender As QueryAutomaton, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(QueryAutomaton.CurrentState)
                Console.WriteLine("changing state: {0}.", sender.CurrentState.Identity)
            Case NameOf(QueryAutomaton.IsTimeout)
                Console.WriteLine("changing timeout: {0}.", sender.IsTimeout)
            Case NameOf(QueryAutomaton.IsStopRequested)
                Console.WriteLine("changing stop requested: {0}.", sender.IsStopRequested)
        End Select
    End Sub

    Private Sub QueryMachine_Propertychanging(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangingEventArgs) Handles _queryMachine.PropertyChanging
        Try
            Me.OnPropertychanging(TryCast(sender, QueryAutomaton), e?.PropertyName)
        Catch ex As Exception
            Console.WriteLine("Exception occurred handling the property {0} changing event at the {1} state.",
                              e?.PropertyName, Me._queryMachine?.CurrentState?.Identity)
        End Try
    End Sub
#End If


    Private Sub OnPropertyChanged(ByVal sender As QueryAutomaton, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(QueryAutomaton.CurrentState)
                Console.WriteLine("state: {0}.", sender.CurrentState.Identity)
            Case NameOf(QueryAutomaton.IsTimeout)
                Console.WriteLine("timeout: {0}.", sender.IsTimeout)
            Case NameOf(QueryAutomaton.IsStopRequested)
                Console.WriteLine("stop requested: {0}.", sender.IsStopRequested)
        End Select
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub QueryMachine_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _QueryMachine.PropertyChanged
        Try
            Me.OnPropertyChanged(TryCast(sender, QueryAutomaton), e?.PropertyName)
        Catch ex As Exception
            Console.WriteLine("Exception occurred handling the property {0} changed event at the {1} state.",
                              e?.PropertyName, Me._QueryMachine?.CurrentState?.Identity)
        End Try
    End Sub

    Private Sub QueryMachine_Stopped(ByVal sender As Object, ByVal e As System.EventArgs) Handles _QueryMachine.Stopped
        If Me._QueryMachine.IsTimeout Then
            Console.WriteLine("query machine timeout.")
        Else
            Console.WriteLine("query machine stopped.")
        End If
        If Me._QueryMachineThread IsNot Nothing Then
            Me.JoinThreads(Me._QueryMachineThread)
        End If
    End Sub

    Private Sub QueryMachine_Failed(sender As Object, e As ErrorEventArgs) Handles _QueryMachine.Failed
        Dim machine As QueryAutomaton = TryCast(sender, QueryAutomaton)
        If machine IsNot Nothing Then
            Console.WriteLine("Exception occurred at '{0}' state after '{1}' state;. {2}",
                              machine.CurrentState?.Identity, machine.PreviousState?.Identity, e?.GetException.ToFullBlownString)
            ' requests a stop
            machine.EnqueueInput(QueryAutomatonSymbol.Stop, True, False)
        End If
    End Sub

#End Region

End Class

Public Enum SigmaState
    None
    One
    Two
    Three
End Enum


Public Enum SigmaSignal
    None
    One
    Two
    Three
End Enum

Public Class Sigma
    Public Sub New(ByVal value As SigmaSignal)
        Me._Id = value
    End Sub
    Public Property Id As SigmaSignal
    Public Overrides Function ToString() As String
        Return String.Format("Sigma={0}", Id)
    End Function
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, Sigma))
    End Function
    Public Overloads Function Equals(ByVal value As isr.Automata.Sigma) As Boolean
        Return Me.Id.Equals(value.Id)
    End Function
End Class
